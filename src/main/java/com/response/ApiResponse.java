/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.response;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import org.springframework.http.HttpStatus;

/**
 *
 * @author Welcome
 */
public class ApiResponse implements Serializable {

    private String timestamp;
    private int status;
    private String message;
    private String token;
    private Object result;

    public ApiResponse() {
    }

    public ApiResponse(HttpStatus status, String message, Object result) {
        this.timestamp = getLocalDateTime();
        this.status = status.value();
        this.message = message;
        this.result = result;
    }

    public ApiResponse(HttpStatus status, String message, Object result, String token) {
        this.timestamp = getLocalDateTime();
        this.status = status.value();
        this.message = message;
        this.result = result;
        this.token = token;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getResult() {
        return result;
    }

    public void setResult(Object result) {
        this.result = result;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Override
    public String toString() {
        return "ApiResponse [statusCode=" + status + ", message=" + message + "]";
    }

    private String getLocalDateTime() {
        LocalDateTime localdatetime = LocalDateTime.now();
        DateTimeFormatter format = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
        String formatDateTime = localdatetime.format(format);
        System.out.println(formatDateTime);
        return formatDateTime;
    }

}
