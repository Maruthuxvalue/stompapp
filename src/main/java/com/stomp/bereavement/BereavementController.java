/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stomp.bereavement;

import com.stomp.user.Users;
import com.stomp.user.UserRepository;
import com.response.ApiResponse;
import java.util.List;
import java.util.NoSuchElementException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Welcome
 */
//@RestController
//@RequestMapping(value = "/bb/bereavement")
//@RequestMapping(value = "/bully-buddy/bereavement")
@Service
public class BereavementController {

    @Autowired
    BereavementRepository bereavementRepository;

    @Autowired
    UserRepository userRepository;

    @Transactional
    @PostMapping("/bereavement_save")
    public ApiResponse saveBereavement(Bereavement obj) {
        String langSave = "Record Saved Successfully";
        try {
            Bereavement saveObj = bereavementRepository.save(obj);
            Users lang = userRepository.findById(obj.getUserId()).get();
            if (lang.getUserLang().contains("Spanish")) {
                langSave = "Registro guardado con éxito";
            }
            return new ApiResponse(HttpStatus.OK, langSave, saveObj);
        } catch (Exception e) {
            return new ApiResponse(HttpStatus.BAD_REQUEST, "Exception", e.getMessage());

        }
    }

    @PostMapping("/bereavement_update")
    @Transactional
    public ApiResponse updateBereavement(Bereavement obj) {
        String langUpdate = "Record Updated Successfully";
        String langNotFound = "Record not Found";
        String message = "";
        HttpStatus status = HttpStatus.OK;
        try {
            Bereavement bereavementDetail = bereavementRepository.findById(obj.getId()).get();
            Users lang = userRepository.findById(bereavementDetail.getUserId()).get();
            if (lang.getUserLang().contains("Spanish")) {
                langUpdate = "Registro actualizado con éxito";
                langNotFound = "Registro no encontrado";
            }
            if (bereavementDetail != null) {
                bereavementDetail.setName(obj.getName());
                bereavementDetail.setAge(obj.getAge());
                bereavementDetail.setStateName(obj.getStateName());
                bereavementDetail.setStory(obj.getStory());
                bereavementDetail.setOrganization(obj.getOrganization());
                bereavementDetail.setImageURL(obj.getImageURL());
                bereavementRepository.save(bereavementDetail);
                message = langUpdate;
            } else {
                message = langNotFound;
                status = HttpStatus.NOT_FOUND;
            }
            return new ApiResponse(status, message, bereavementDetail);
        } catch (NoSuchElementException e) {
            return new ApiResponse(HttpStatus.NOT_FOUND, langNotFound, langNotFound);
        } catch (NullPointerException e) {
            return new ApiResponse(HttpStatus.BAD_REQUEST, "Exception", "Null");
        } catch (Exception e) {
            return new ApiResponse(HttpStatus.BAD_REQUEST, "Exception", e.getMessage());
        }
    }

    @PostMapping("/get_all_bereavement")
    @Transactional
    public ApiResponse getList(Bereavement obj) {
        String langNotFound = "Record not Found";
        String langRetrieved = "Record retrieved successfully";
        String message = "";
//        List<Bereavement> bereavementDetail = bereavementRepository.findAllByUserId(obj.getId());
        List<Bereavement> bereavementDetail = bereavementRepository.findAll(Sort.by(Sort.Direction.DESC, "id"));
        HttpStatus status = HttpStatus.OK;
        try {
            if (bereavementDetail != null) {
                message = "Record Retreived Successfully";
            } else {
                bereavementDetail = null;
                message = "Record not Found";
                status = HttpStatus.NOT_FOUND;
            }
            return new ApiResponse(status, message, bereavementDetail);
        } catch (Exception e) {
            return new ApiResponse(HttpStatus.BAD_REQUEST, "Exception", e.getMessage());

        }
    }
}
