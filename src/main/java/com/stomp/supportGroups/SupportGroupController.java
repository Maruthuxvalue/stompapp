/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stomp.supportGroups;

import com.stomp.user.Users;
import com.stomp.user.UserRepository;
import com.response.ApiResponse;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Welcome
 */
//@RestController
//@RequestMapping(value = "/bb/support")
@Service
public class SupportGroupController {

    @Autowired
    SupportGroupRepository supportRepository;

    @PostMapping("/list")
    public ApiResponse getList() {
        List<SupportGroups> list = new ArrayList();
        try {
            list = supportRepository.findAll();
        } catch (Exception e) {
            return new ApiResponse(HttpStatus.BAD_REQUEST, "Error", e.getMessage());
        }
        return new ApiResponse(HttpStatus.OK, "Record retrieved successfully", list);
    }

}
