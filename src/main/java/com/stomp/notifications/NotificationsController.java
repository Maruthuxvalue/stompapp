/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stomp.notifications;

import com.stomp.busRoutes.BusRoutesRepository;
import com.stomp.reporting.Reporting;
import com.stomp.reporting.ReportingRepository;
import com.stomp.user.ParentChild;
import com.stomp.user.ParentChildRepository;
import com.stomp.user.Users;
import com.stomp.user.UserRepository;
import com.response.ApiResponse;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Welcome
 */
//@RestController
//@RequestMapping(value = "/bb/notifications")
@Service
public class NotificationsController {

    @Autowired
    NotificationsRepository notificationsRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    BusRoutesRepository busRepository;

    @Autowired
    ReportingRepository reportingRepository;

    @Autowired
    ParentChildRepository parChildRepository;

    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    @PostMapping("/get_all_notifications_by_id")
    public ApiResponse getNotificationsForStudent(PushNotifications obj) {
        String langRetrieved = "Record retrieved successfully";

        List<PushNotifications> notificationsList = new ArrayList();
        try {
            Users lang = userRepository.findById(obj.getUserId()).get();
            if (lang.getUserLang().contains("Spanish")) {
                langRetrieved = "Registro recuperado con éxito";
            }
            notificationsList = notificationsRepository.findAllByUserIdOrderByIdDesc(obj.getUserId());
        } catch (Exception e) {
            return new ApiResponse(HttpStatus.BAD_REQUEST, "Exception", e.getMessage());

        }
        return new ApiResponse(HttpStatus.OK, langRetrieved, notificationsList);

    }

    @PostMapping("/get_all_notifications_for_parent")
    public ApiResponse getNotificationsForParent(PushNotifications obj) {
        String pushLangEmy = " has reported an Emergency Abuse/Harassment.";
        String langReport = " has reported a Abuse/Harassment Incident.";
        String langChild = "Your Friend/Spouse/Parent ";
        String langTracing = " You can track your Friend's/Spouse's/Parent's latest location using the location tracker. Please contact the admin using the chat on this app or call them immediately.";
        String langPhoneCall = " Please contact the admin using the chat on this app or call them immediately.";
        String langRetrieved = "Record retrieved successfully";
        Map<String, Object> map = new HashMap();
        try {
            Users lang = userRepository.findById(obj.getUserId()).get();
            if (lang.getUserLang().contains("Spanish")) {
                langRetrieved = "Registro recuperado con éxito";
                langChild = "Tu amigo / cónyuge";
                pushLangEmy = " ha reportado una emergencia de acoso.";
                langReport = " ha reportado un incidente de acoso.";
                langTracing = " Puede rastrear la última ubicación de su amigo's / cónyuge's usando el rastreador de ubicación. Por favor comuníquese con el administrador mediante el chat de esta aplicación STOMP o llámelos de inmediato.";
                langPhoneCall = " Por favor comuníquese con el administrador mediante el chat de esta aplicación STOMP o llámelos de inmediato.";

            }
            List<PushNotifications> notificationsList = new ArrayList<PushNotifications>();
            List<PushNotifications> AllNotificationsList = new ArrayList<PushNotifications>();
            List newList = new ArrayList<>();
//            ArrayList<HashMap<String, String>> newList
//                    = new ArrayList<HashMap<String, String>>();
            List<ParentChild> childList = parChildRepository.findAllByFriendId(obj.getUserId());
            List<Users> userList = new ArrayList();
            for (ParentChild item : childList) {
                Users childReports = userRepository.findById(item.getUserId()).get();
                userList.add(childReports);
            }
//            List<User> childList = userRepository.findAllByFriendId(obj.getUserId());
//            int index = 0;
            for (Users childDetials : userList) {
                notificationsList = notificationsRepository.findAllByUserIdOrderByIdDesc(childDetials.getId());
                AllNotificationsList.addAll(notificationsList);
                for (PushNotifications list : notificationsList) {
//                    HashMap<String, String> hm = new HashMap<String, String>();
                    HashMap hm = new HashMap();
                    hm.put("id", list.getId());
                    if (list.getReportId() != null) {
                        hm.put("message", langChild + childDetials.getName() + langReport + langPhoneCall);
                    } else {
                        hm.put("message", langChild + childDetials.getName() + pushLangEmy + langTracing);
                    }
                    hm.put("isRead", list.getIsFriend());
//                hm.put("createdDateTime", formatter.format(list.getCreatedDateTime()));
                    hm.put("reportId", list.getReportId());
                    hm.put("esbId", list.getEsbReportId());
                    hm.put("createdDateTime", list.getCreatedDateTime());
                    hm.put("modifiedDateTime", list.getModifiedDateTime());
//                    newList.add(index,hm);
                    newList.add(hm);
//                    index++;
                }
            }
//            for (int c = 0; c < (newList.size() - 1); c++) {
//                for (int d = 0; d < (newList.size() - c - 1); d++) {
//
//                    if (Integer.parseInt(newList.get(d).get("id")) < Integer
//                            .parseInt(newList.get(d + 1).get("id"))) {
//                        HashMap<String, String> temporary;
//                        temporary = newList.get(d);
//                        newList.set(d, newList.get(d + 1));
//                        newList.set(d + 1, temporary);
//
//                    }
//                }
//            }
            Stream<PushNotifications> result = AllNotificationsList.stream()
                    .filter(x -> x.getIsFriend() == 'N');
            map.put("count", result.count());

            map.put("data", newList);
        } catch (Exception e) {
            return new ApiResponse(HttpStatus.BAD_REQUEST, "Exception", e.getMessage());
        }
        return new ApiResponse(HttpStatus.OK, langRetrieved, map);

    }

    @PostMapping("/get_all_notifications_for_teacher")
    public ApiResponse getNotificationsForTeacher(PushNotifications obj) {
        String langRetrieved = "Record retrieved successfully";

        Map<String, Object> map = new HashMap();
        try {
            List<PushNotifications> itemList = new ArrayList<PushNotifications>();
            List newList = new ArrayList();
            Users userDetails = userRepository.findById(obj.getUserId()).get();
            if (userDetails.getUserLang().contains("Spanish")) {
                langRetrieved = "Registro recuperado con éxito";
            }
            if (userDetails.getUserTypeId() == 2) {
                itemList = userRepository.fetchByManagerIdForNotification(userDetails.getOrgId(), userDetails.getGrade(), obj.getUserId());
                Stream<PushNotifications> result = itemList.stream()
                        .filter(x -> x.getIsManager() == 'N');
                map.put("count", result.count());
                for (PushNotifications list : itemList) {
                    HashMap hm = new HashMap();
                    hm.put("id", list.getId());
                    hm.put("message", list.getMessage());
                    hm.put("isRead", list.getIsManager());
                    hm.put("reportId", list.getReportId());
                    hm.put("esbId", list.getEsbReportId());
                    hm.put("createdDateTime", list.getCreatedDateTime());
                    hm.put("modifiedDateTime", list.getModifiedDateTime());
                    newList.add(hm);
                }
            } else {
                itemList = userRepository.fetchByOrgIdForNotification(userDetails.getOrgId(), obj.getUserId());
                Stream<PushNotifications> result = itemList.stream()
                        .filter(x -> x.getIsAdmin() == 'N');
                map.put("count", result.count());
                for (PushNotifications list : itemList) {
                    HashMap hm = new HashMap();
                    hm.put("id", list.getId());
                    hm.put("message", list.getMessage());
                    hm.put("isRead", list.getIsAdmin());
                    hm.put("reportId", list.getReportId());
                    hm.put("esbId", list.getEsbReportId());
                    hm.put("createdDateTime", list.getCreatedDateTime());
                    hm.put("modifiedDateTime", list.getModifiedDateTime());
                    newList.add(hm);
                }
            }
            map.put("data", newList);
        } catch (Exception e) {
            return new ApiResponse(HttpStatus.BAD_REQUEST, "Exception", e.getMessage());

        }
        return new ApiResponse(HttpStatus.OK, langRetrieved, map);
    }

    @PostMapping("/get_all_notifications_for_driver")
    public ApiResponse getNotificationsForDriver(PushNotifications obj) {
        String langRetrieved = "Record retrieved successfully";
        Map<String, Object> map = new HashMap();
        try {
            List<PushNotifications> itemList = new ArrayList<PushNotifications>();
            List newList = new ArrayList();
            List uniqueChilds = new ArrayList();
            List<Long> parents = new ArrayList();
            Users userDetails = userRepository.findById(obj.getUserId()).get();
            if (userDetails.getUserLang().contains("Spanish")) {
                langRetrieved = "Registro recuperado con éxito";
            }
            itemList = busRepository.fetchByDriverIdForNotification(obj.getUserId());
            for (PushNotifications list : itemList) {
                if (!uniqueChilds.contains(list.getUserId())) {
                    uniqueChilds.add(list.getUserId());
                    List<ParentChild> parentsList = parChildRepository.findAllByUserId(list.getUserId());
                    if (parentsList != null) {
                        for (ParentChild item : parentsList) {
                            if (!parents.contains(item.getFriendId())) {
                                parents.add(item.getFriendId());
                            }
                        }
                    }
                }
            }
            List<PushNotifications> parentsNotifications = notificationsRepository.findAllByUserIdInOrderByIdDesc(parents);
            if (parentsNotifications != null) {
                itemList.addAll(parentsNotifications);
            }
//            Stream<PushNotifications> result = itemList.stream()
//                    .filter(x -> x.getIsDriver() == 'N');
            if (!itemList.isEmpty()) {
                Collections.sort(itemList,
                        Comparator.comparingLong(PushNotifications::getId).reversed());
            }
            int count = 0;
            for (PushNotifications list : itemList) {
                if (list.getReportId() != null) { // to get nofitications for indicidents that happened in bus
                    Reporting report = reportingRepository.findById(list.getReportId()).get();
                    if (report.getIncidentPlace().contains("Bus")) {
                        HashMap hm = new HashMap();
                        hm.put("id", list.getId());
                        hm.put("message", list.getMessage());
                        hm.put("isRead", list.getIsDriver());
                        hm.put("reportId", list.getReportId());
                        hm.put("esbId", list.getEsbReportId());
                        hm.put("createdDateTime", list.getCreatedDateTime());
                        hm.put("modifiedDateTime", list.getModifiedDateTime());
                        if (!newList.contains(hm)) {
                            newList.add(hm);
                            if (list.getIsDriver() == 'N') {
                                count++;
                            }

                        }
                    }
                } else {
                    HashMap hm = new HashMap();
                    hm.put("id", list.getId());
                    hm.put("message", list.getMessage());
                    hm.put("isRead", list.getIsDriver());
                    hm.put("reportId", list.getReportId());
                    hm.put("esbId", list.getEsbReportId());
                    hm.put("createdDateTime", list.getCreatedDateTime());
                    hm.put("modifiedDateTime", list.getModifiedDateTime());
                    if (!newList.contains(hm)) {
                        newList.add(hm);
                        if (list.getIsDriver() == 'N') {
                            count++;
                        }
                    }
                }
            }
            map.put("data", newList);
            map.put("count", count);
        } catch (Exception e) {
            return new ApiResponse(HttpStatus.BAD_REQUEST, "Exception", e.getMessage());
        }
        return new ApiResponse(HttpStatus.OK, langRetrieved, map);
    }

    @PostMapping("/update_read_notifications")
    public ApiResponse updateNotificationsRead(PushNotifications obj) {

        try {
            String[] convertedRankArray = obj.getIds().split(",");
            List<PushNotifications> saveList = new ArrayList();
//            PushNotifications updateFlag = notificationsRepository.findById(obj.getId()).get();
//            Users userDetail = userRepository.findById(obj.getUserId()).get();
            if (obj.getUserTypeId() == 2) {
                for (String number : convertedRankArray) {
                    PushNotifications updateFlag = notificationsRepository.findById(Long.parseLong(number.trim())).get();
                    updateFlag.setIsManager('Y');
                    saveList.add(updateFlag);
                }
            } else if (obj.getUserTypeId() == 3) {
                for (String number : convertedRankArray) {
                    PushNotifications updateFlag = notificationsRepository.findById(Long.parseLong(number.trim())).get();
                    updateFlag.setIsFriend('Y');
                    saveList.add(updateFlag);
                }
            } else if (obj.getUserTypeId() == 4) {
                for (String number : convertedRankArray) {
                    PushNotifications updateFlag = notificationsRepository.findById(Long.parseLong(number.trim())).get();
                    updateFlag.setIsDriver('Y');
                    saveList.add(updateFlag);
                }
            } else if (obj.getUserTypeId() == 5) {

                for (String number : convertedRankArray) {
                    PushNotifications updateFlag = notificationsRepository.findById(Long.parseLong(number.trim())).get();
                    updateFlag.setIsAdmin('Y');
                    saveList.add(updateFlag);
                }
            } else if (obj.getUserTypeId() == 0) {
                for (String number : convertedRankArray) {
                    PushNotifications updateFlag = notificationsRepository.findById(Long.parseLong(number.trim())).get();
                    updateFlag.setIsSuperAdmin('Y');
                    saveList.add(updateFlag);
                }
            }
            notificationsRepository.saveAll(saveList);
        } catch (Exception e) {
            return new ApiResponse(HttpStatus.BAD_REQUEST, "Exception", e.getMessage());

        }
        return new ApiResponse(HttpStatus.OK, "Record Updated Successfully", "Success");

    }

}
