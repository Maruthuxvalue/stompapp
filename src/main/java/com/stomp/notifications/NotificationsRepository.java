/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stomp.notifications;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author Welcome
 */
public interface NotificationsRepository extends JpaRepository<PushNotifications, Long> {

    List<PushNotifications> findAllByUserIdOrderByIdDesc(long id);

    List<PushNotifications> findAllByUserIdInOrderByIdDesc(List<Long> ids);

}
