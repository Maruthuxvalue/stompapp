/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stomp.notifications;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.Transient;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

/**
 *
 * @author Welcome
 */
@Entity
@Table(name = "notifications")
public class PushNotifications implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Id", nullable = false, updatable = false)
    private long id;

    @Column(name = "UserId", nullable = false)
    private long userId;

    @Column(name = "ReportId", nullable = true)
    private Long reportId;

    @Column(name = "EsbReportId", nullable = true)
    private Long esbReportId;

    @Column(name = "Message", nullable = false)
    private String message;

    @Column(name = "IsFriend", nullable = false)
    private char isFriend;

    @Column(name = "IsManager", nullable = false)
    private char isManager;

    @Column(name = "IsDriver", nullable = false)
    private char isDriver;

    @Column(name = "IsAdmin", nullable = false)
    private char isAdmin;

    @Column(name = "IsSuperAdmin", nullable = false)
    private char isSuperAdmin;

    @Transient
    private String ids;

    @Transient
    private Integer userTypeId;

    @Column(name = "CreatedDateTime", updatable = false)
    @CreationTimestamp
//    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date createdDateTime;

    @Column(name = "ModifiedDateTime")
    @UpdateTimestamp
//    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date modifiedDateTime;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public Long getReportId() {
        return reportId;
    }

    public void setReportId(Long reportId) {
        this.reportId = reportId;
    }

    public Long getEsbReportId() {
        return esbReportId;
    }

    public void setEsbReportId(Long esbReportId) {
        this.esbReportId = esbReportId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public char getIsFriend() {
        return isFriend;
    }

    public void setIsFriend(char isFriend) {
        this.isFriend = isFriend;
    }

    public char getIsManager() {
        return isManager;
    }

    public void setIsManager(char isManager) {
        this.isManager = isManager;
    }

    public char getIsDriver() {
        return isDriver;
    }

    public void setIsDriver(char isDriver) {
        this.isDriver = isDriver;
    }

    public char getIsAdmin() {
        return isAdmin;
    }

    public void setIsAdmin(char isAdmin) {
        this.isAdmin = isAdmin;
    }

    public char getIsSuperAdmin() {
        return isSuperAdmin;
    }

    public void setIsSuperAdmin(char isSuperAdmin) {
        this.isSuperAdmin = isSuperAdmin;
    }

    public String getIds() {
        return ids;
    }

    public void setIds(String ids) {
        this.ids = ids;
    }

    public Integer getUserTypeId() {
        return userTypeId;
    }

    public void setUserTypeId(Integer userTypeId) {
        this.userTypeId = userTypeId;
    }

    public Date getCreatedDateTime() {
        return createdDateTime;
    }

    public void setCreatedDateTime(Date createdDateTime) {
        this.createdDateTime = createdDateTime;
    }

    public Date getModifiedDateTime() {
        return modifiedDateTime;
    }

    public void setModifiedDateTime(Date modifiedDateTime) {
        this.modifiedDateTime = modifiedDateTime;
    }

}
