package com.stomp.controller;

import com.response.ApiResponse;
import com.response.ApiResponse1;
import com.stomp.EmergencyReporting.ESBReporting;
import com.stomp.EmergencyReporting.EmergencyReportingController;
import com.stomp.bereavement.Bereavement;
import com.stomp.bereavement.BereavementController;
import com.stomp.busRoutes.BusRoutes;
import com.stomp.busRoutes.BusRoutesController;
import com.stomp.firebase.ChatRequest;
import com.stomp.firebase.FBService;
import com.stomp.groupChat.GroupChat;
import com.stomp.groupChat.GroupChatController;
import com.stomp.language.LanguageController;
import com.stomp.locationAndTypes.IncidentLocation;
import com.stomp.locationAndTypes.LocationAndTypeController;
import com.stomp.model.JwtRequest;
import com.stomp.notifications.NotificationsController;
import com.stomp.notifications.PushNotifications;
import com.stomp.order.Orderdemo;
import com.stomp.order.OrderController;
import com.stomp.org.OrgController;
import com.stomp.org.Organization;
import com.stomp.pushConfiguration.PushConfiguration;
import com.stomp.pushConfiguration.PushConfigurationController;
import com.stomp.reporting.Reporting;
import com.stomp.reporting.ReportingController;
import com.stomp.states.StatesController;
import com.stomp.supportGroups.SupportGroupController;
import com.stomp.twilio.SMSController;
import com.stomp.twilio.TwilioRequest;
import com.stomp.user.ScreenshotCount;
import com.stomp.user.UserContacts;
import com.stomp.user.UserController;
import com.stomp.user.Users;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping(value = "/stomp")
public class HelloWorldController {

    @Autowired
    JwtAuthenticationController authenticateCont;

    @Autowired
    EmergencyReportingController emergencyCont;

    @Autowired
    BereavementController bereavementCont;

    @Autowired
    BusRoutesController busRoutesCont;

    @Autowired
    GroupChatController groupChatCont;

    @Autowired
    LanguageController langCont;

    @Autowired
    NotificationsController notificationCont;

    @Autowired
    PushConfigurationController pushConfigCont;

    @Autowired
    ReportingController reportCont;

    @Autowired
    OrgController schoolCont;

    @Autowired
    StatesController stateCont;

    @Autowired
    SupportGroupController supportCont;

    @Autowired
    UserController userCont;

    @Autowired
    FBService fbCont;

    @Autowired
    SMSController smsCont;

    @Autowired
    LocationAndTypeController locAndTypeCont;
    
    @Autowired
    OrderController orderCont;

    @Transactional
    @PostMapping("/v1/user/authenticate")
    public ResponseEntity<?> createAuthenticationToken(@RequestBody JwtRequest authenticationRequest) throws Exception {
        return authenticateCont.createAuthenticationToken(authenticationRequest);
    }

    @PostMapping("/v1/esb_reporting/get_user_esb_reports")
    public ApiResponse getUsersEsbReports_v1(ESBReporting obj) throws Exception {
        return emergencyCont.getUsersEsbReports(obj);
    }

    @PostMapping("/v1/esb_reporting/esb_save")
    public ApiResponse saveReports_v1(ESBReporting obj) throws Exception {
        return emergencyCont.saveReports(obj, obj.getUserTypeId());
    }

    @PostMapping("/v1/esb_reporting/esb_update")
    public ApiResponse UpdateReport_v1(ESBReporting obj) throws Exception {
        return emergencyCont.UpdateReport(obj);
    }

    @PostMapping("/v1/esb_reporting/get_esb_reports_by_parent_id")
    public ApiResponse getEsbReportsForParent_v1(Users obj) throws Exception {
        return emergencyCont.getReportsForParent(obj);
    }

    @PostMapping("/v1/esb_reporting/get_esb_reports_by_school_id")
    public ApiResponse getEsbReportsForTeachers_v1(Users obj) throws Exception {
        return emergencyCont.getReportsForTeachers(obj);
    }

    @PostMapping("/v1/esb_reporting/get_esb_reports_by_driver_id")
    public ApiResponse getEsbReportsForDrivers_v1(Users obj) throws Exception {
        return emergencyCont.getReportsForDrivers(obj);
    }

    @PostMapping("/v1/bereavement/bereavement_save")
    public ApiResponse saveBereavement_v1(Bereavement obj) throws Exception {
        return bereavementCont.saveBereavement(obj);
    }

    @PostMapping("/v1/bereavement/bereavement_update")
    public ApiResponse updateBereavement_v1(Bereavement obj) throws Exception {
        return bereavementCont.updateBereavement(obj);
    }

    @PostMapping("/v1/bereavement/get_all_bereavement")
    public ApiResponse getList_v1(Bereavement obj) throws Exception {
        return bereavementCont.getList(obj);
    }

    @PostMapping("/v1/bus/get_all_busRoutes")
    public ApiResponse getSchoolBus_v1(BusRoutes obj) throws Exception {
        return busRoutesCont.getSchoolBus(obj);
    }

    @PostMapping("/v1/chat/get_group_chatId")
    public ApiResponse getChatIds_v1(Users obj) throws Exception {
        return groupChatCont.getChatIds(obj.getUserTypeId(), obj.getId());
    }

    @PostMapping("/v1/chat/get_device_tokens_by_chatId")
    public ApiResponse getDeviceTokens_v1(GroupChat obj) throws Exception {
        return groupChatCont.getDeviceTokens(obj);
    }

    @PostMapping("/v1/language/list")
    public ApiResponse getChatIds_v1() throws Exception {
        return langCont.getChatIds();
    }

    @PostMapping("/v1/notifications/get_all_notifications_by_id")
    public ApiResponse getNotificationsForStudent_v1(PushNotifications obj) throws Exception {
        return notificationCont.getNotificationsForStudent(obj);
    }

    @PostMapping("/v1/notifications/get_all_neotifications_for_parent")
    public ApiResponse getNotificationsForParent_v1(PushNotifications obj) throws Exception {
        return notificationCont.getNotificationsForParent(obj);
    }

    @PostMapping("/v1/notifications/get_all_notifications_for_teacher")
    public ApiResponse getNotificationsForTeacher_v1(PushNotifications obj) throws Exception {
        return notificationCont.getNotificationsForTeacher(obj);
    }

    @PostMapping("/v1/notifications/get_all_notifications_for_driver")
    public ApiResponse getNotificationsForDriver_v1(PushNotifications obj) throws Exception {
        return notificationCont.getNotificationsForDriver(obj);
    }

    @PostMapping("/v1/notifications/update_read_notifications")
    public ApiResponse updateNotificationsRead_v1(PushNotifications obj) throws Exception {
        return notificationCont.updateNotificationsRead(obj);
    }

    @PostMapping("/v1/push_config/get_device_token")
    public ApiResponse getUsersToken_v1(PushConfiguration obj) throws Exception {
        return pushConfigCont.getUsersToken(obj);
    }

    @PostMapping("/v1/reporting/get_user_reports")
    public ApiResponse getUsersReports_v1(Reporting obj) throws Exception {
        return reportCont.getUsersReports(obj);
    }

    @PostMapping("/v1/reporting/reporting_save")
    public ApiResponse saveReports_v1(Reporting obj) throws Exception {
        return reportCont.saveReports(obj);
    }
    
    @PostMapping("/v1/order/order_list")
    public ApiResponse getOrders() throws Exception {
        return orderCont.getAllOrderList();
    }

    @PostMapping("/v1/order/order_save") 
    public ApiResponse saveOrder(Orderdemo obj) throws Exception {   
    return orderCont.saveOrders(obj);
    }
    
    @PostMapping("/v1/order/order_update")  
    public ApiResponse updateOrder(Orderdemo obj) throws Exception {   
    return orderCont.updateOrder(obj);
    }
    @PostMapping("/v1/order/order_delete")
    public ApiResponse deleteOrder(Orderdemo obj) throws Exception {
        return orderCont.deleteOrder(obj);
    }
    

    @PostMapping("/v1/reporting/reporting_update")
    public ApiResponse UpdateReport_v1(Reporting obj) throws Exception {
        return reportCont.UpdateReport(obj);
    }

    @PostMapping("/v1/reporting/get_reports_by_parent_id")
    public ApiResponse getReportsForParent_v1(Users obj) throws Exception {
        return reportCont.getReportsForParent(obj);
    }

    @PostMapping("/v1/reporting/get_reports_by_school_id")
    public ApiResponse getReportsForTeachers_v1(Users obj) throws Exception {
        return reportCont.getReportsForTeachers(obj);
    }

    @PostMapping("/v1/reporting/get_reports_by_driver_id")
    public ApiResponse getReportsForDrivers_v1(Users obj) throws Exception {
        return reportCont.getReportsForDrivers(obj);
    }

    @PostMapping("/v1/org/get_all_org")
    public ApiResponse getSchools_v1() throws Exception {
        return schoolCont.getOrgs();
    }

    @PostMapping("/v1/org/get_org_by_state_id")
    public ApiResponse getSchoolByStateId_v1(Organization obj) throws Exception {
        return schoolCont.getSch(obj);
    }

    @PostMapping("/v1/org/get_org_autofill_by_state_id")
    public ApiResponse getSchoolsByState_v1(Organization obj) throws Exception {
        return schoolCont.getOrgsByState(obj);
    }

    @PostMapping("/v1/org/get_org_autofill")
    public ApiResponse getSchoolsAutoFill_v1(Organization obj) throws Exception {
        return schoolCont.getOrgsAutoFill(obj);
    }

    @PostMapping("/v1/org/get_org_for_autofill")
    public ApiResponse getSchoolsAuto_v1(Organization obj) throws Exception {
        return schoolCont.getOrgsAuto(obj);
    }

    @PostMapping("/v1/states/get_all_states")
    public ApiResponse getStates_v1() throws Exception {
        return stateCont.getStates();
    }

    @PostMapping("/v1/support/list")
    public ApiResponse getList_v1() throws Exception {
        return supportCont.getList();
    }

    @Transactional
    @PostMapping("/v1/user/login")
    public ApiResponse login_v1(Users obj) throws Exception {
        return userCont.loginUser(obj);
    }

    @Transactional
    @PostMapping("/v1/user/sign_up")
    public ApiResponse signUp_v1(Users obj) throws Exception {
        return userCont.saveUser(obj);
    }

    @Transactional
    @PostMapping("/v1/user/parent_sign_up")
    public ApiResponse parentSignUp_v1(Users obj) throws Exception {
        return userCont.saveParent(obj);
    }

    @Transactional
    @PostMapping("/v1/user/add_child")
    public ApiResponse add_child_v1(Users obj) throws Exception {
        return userCont.saveChild(obj);
    }

    @Transactional
    @PostMapping("/v1/user/verify_user")
    public ApiResponse verify_user_v1(Users obj) throws Exception {
        return userCont.verifyUser(obj);
    }

    @Transactional
    @PostMapping("/v1/user/verify_otp")
    public ApiResponse verify_otp_v1(Users obj) throws Exception {
        return userCont.VerifyOTP(obj);
    }

    @Transactional
    @PostMapping("/v1/user/check_email_exists")
    public ApiResponse check_email_exists_v1(Users obj) throws Exception {
        return userCont.checkEmail(obj);
    }

    @Transactional
    @PostMapping("/v1/user/check_social_user")
    public ApiResponse check_social_user_v1(Users obj) throws Exception {
        return userCont.verifySocialUser(obj);
    }

    @Transactional
    @PostMapping("/v1/user/verify_forgot_password")
    public ApiResponse verify_forgot_password_v1(Users obj) throws Exception {
        return userCont.getVerificationCode(obj);
    }

    @Transactional
    @PostMapping("/v1/user/change_password")
    public ApiResponse changePassword_v1(Users obj) throws Exception {
        return userCont.changePwd(obj);
    }

    @Transactional
    @PostMapping("/v1/user/forgot_password")
    public ApiResponse forgroPwd_v1(Users obj) throws Exception {
        return userCont.forgotPasword(obj);
    }

    @Transactional
    @PostMapping("/v1/user/forgot_username")
    public ApiResponse forgot_username_v1(Users obj) throws Exception {
        return userCont.forgotUserName(obj);
    }

    @Transactional
    @PostMapping("/v1/user/update_profile")
    public ApiResponse update_profile_v1(Users obj) throws Exception {
        return userCont.updateUser(obj);
    }

    @Transactional
    @PostMapping("/v1/user/get_child_list")
    public ApiResponse get_child_list_v1(Users obj) throws Exception {
        return userCont.getChilds(obj);
    }

    @Transactional
    @PostMapping("/v1/user/delete_child_by_id")
    public ApiResponse delete_child_by_id_v1(Users obj) throws Exception {
        return userCont.deleteChild(obj);
    }

    @Transactional
    @PostMapping("/v1/user/get_user_by_id")
    public ApiResponse get_user_by_id_v1(Users obj) throws Exception {
        return userCont.getUser(obj);
    }

    @Transactional
    @PostMapping("/v1/user/get_chat_group_ids")
    public ApiResponse get_chat_group_ids_v1(Users obj) throws Exception {
        return userCont.getIdForChat(obj);
    }

    @Transactional
    @PostMapping("/v1/user/logout")
    public ApiResponse logout_v1(Users obj) throws Exception {
        return userCont.logout(obj);
    }

    @Transactional
    @PostMapping("/v1/user/enable_ads")
    public ApiResponse checkAds_v1(Users obj) throws Exception {
        return userCont.checkAds(obj);
    }

    @Transactional
    @PostMapping("/v1/user/update_language")
    public ApiResponse updateLang_v1(Users obj) throws Exception {
        return userCont.updateLang(obj);
    }

    @Transactional
    @PostMapping("/v1/user/screenshot_count")
    public ApiResponse screenShotCount_v1(ScreenshotCount obj) throws Exception {
        return userCont.screenShotCount(obj);
    }

    @Transactional
    @PostMapping("/v1/fireBase/delete_chat")
    public ApiResponse deleteCollection_v1(ChatRequest obj) throws Exception {
        return fbCont.deleteCollection(obj.getDocumentId());
    }

    @Transactional
    @PostMapping("/v1/user/add_contact")
    public ApiResponse AddContact(UserContacts obj) throws Exception {
        return userCont.AddUserContact(obj);
    }

    @Transactional
    @PostMapping("/v1/user/delete_contact")
    public ApiResponse DeleteContact(UserContacts obj) throws Exception {
        return userCont.DeleteUserContact(obj);
    }

    @Transactional
    @PostMapping("/v1/fireBase/add_chat")
    public ApiResponse addCollection_v1(ChatRequest obj) throws Exception {
        return fbCont.addCollection(obj.getDocumentId(), obj.getDocumentList());
    }

    @Transactional
    @PostMapping("/v1/user/get_friends_list")
    public ApiResponse1 get_friend_list_v1(Users obj) throws Exception {
        return userCont.getFriends(obj);
    }
    
   @Transactional
    @PostMapping("/v1/user/send_invite")
    public ApiResponse sendInvite(UserContacts obj) throws Exception {
        return userCont.sendInvite(obj);
    }

    @Transactional
    @PostMapping("/v1/user/update_spouse_to_user")
    public ApiResponse update_Spouse_to_User(Users obj) throws Exception {
        return userCont.updateSpouseToUser(obj);
    }

    @Transactional
    @PostMapping("/v1/location_and_type/get_incident_loc_and_type")
    public ApiResponse getLocationTypes() throws Exception {
        return locAndTypeCont.getLocationAndTypes();
    }

    @Transactional
    @PostMapping("/v1/test")
    public String test() throws Exception {
        return "Success";
    }

    @Transactional
    @PostMapping("/v1/send_otp")
    public ApiResponse sms(TwilioRequest obj) throws Exception {
        return smsCont.sendSMsMessages(obj);
    }

}
