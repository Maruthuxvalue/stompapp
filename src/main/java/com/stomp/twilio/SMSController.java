/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stomp.twilio;

import com.response.ApiResponse;
import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Welcome
 */
//@RestController
//@RequestMapping(value = "/bb/mail")
@Service
public class SMSController {

    public static class MessageDetails {

        public List<String> numbers;
        public String message;
    }

//    private final String myTwilioPhoneNumber = "+12243343624";
    private final String myTwilioPhoneNumber = "+13042395183";
//    private final String twilioAccountSid = "ACa2cc1f9d1f883d75be77c5d238f7c9e5";
//    private final String twilioAuthToken = "9c34d2ab08670a27bdbebc46b92ad92f";

    @Autowired
    public SMSController(
            @Value("${twilioAccountSid}") String twilioAccountSid,
            @Value("${twilioAuthToken}") String twilioAuthToken) {
        Twilio.init(twilioAccountSid, twilioAuthToken);
    }

    public ApiResponse sendMessages(List<String> nos, String message) {
        try {
            nos.stream().forEach(number -> {
                Message msg = Message.creator(
                        new PhoneNumber(number),
                        new PhoneNumber(myTwilioPhoneNumber),
                        message).create();
            });
            return new ApiResponse(HttpStatus.OK, "Success", "Success");
        } catch (Exception e) {

            return new ApiResponse(HttpStatus.BAD_REQUEST, "Exception", e.getMessage());

        }
    }

//    @PostMapping("/sms")
    public ApiResponse sendSMsMessages( TwilioRequest obj) {
        try {
//            nos.stream().forEach(number -> {
            Message msg = Message.creator(
                    new PhoneNumber(obj.getNo()),
                    new PhoneNumber(myTwilioPhoneNumber),
                    obj.getMessage()+" is your one time password for STOMP app.").create();
//            });
            return new ApiResponse(HttpStatus.OK, "Success", "Success");
        } catch (Exception e) {

            return new ApiResponse(HttpStatus.BAD_REQUEST, "Exception", e);

        }
    }
}
