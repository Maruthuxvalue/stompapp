/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stomp.pushConfiguration;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author Welcome
 */
public interface PushConfigurationRepository extends JpaRepository<PushConfiguration, Long> {

    PushConfiguration findByUserId(long userId);

    List<PushConfiguration> findAllByDeviceToken(String token);

    PushConfiguration findByDeviceToken(String token);

}
