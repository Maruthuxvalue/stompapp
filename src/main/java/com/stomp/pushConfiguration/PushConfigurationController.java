/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stomp.pushConfiguration;

import com.stomp.user.Users;
import com.stomp.user.UserRepository;
import com.response.ApiResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Welcome
 */
//@RestController
//@RequestMapping(value = "/bb/push_config")
@Service
public class PushConfigurationController {

    @Autowired
    PushConfigurationRepository pushConfigRepository;

    @Autowired
    UserRepository userRepository;

    @PostMapping("/get_device_token")
    public ApiResponse getUsersToken(PushConfiguration obj) {
        String langNotFound = "Record not Found";
        String langRetrieved = "Record retrieved successfully";
        PushConfiguration deviceToken = new PushConfiguration();
        String token = "";
        String message = "";
        HttpStatus status = HttpStatus.NOT_FOUND;
        try {
            Users userDetails = userRepository.findById(obj.getUserId()).get();
            if (userDetails.getUserLang().contains("Spanish")) {
                langRetrieved = "Registro recuperado con éxito";
                langNotFound = "Registro no encontrado";
            }
            deviceToken = pushConfigRepository.findByUserId(obj.getUserId());
            if (deviceToken != null) {
                token = deviceToken.getDeviceToken();
                message = langRetrieved;
                status = HttpStatus.OK;
            } else {
                token = "";
                message = langNotFound;
            }
        } catch (Exception e) {
            return new ApiResponse(HttpStatus.BAD_REQUEST, "Exception", e.getMessage());
        }
        return new ApiResponse(status, message, token);
    }
}
