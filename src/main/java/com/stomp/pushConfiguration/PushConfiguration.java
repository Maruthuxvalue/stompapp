/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stomp.pushConfiguration;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

/**
 *
 * @author Welcome
 */
@Entity
@Table(name = "push_configuration_android")
public class PushConfiguration implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Push_config_id", nullable = false, updatable = false)
    private long id;

    @Column(name = "User_id", nullable = true)
    private long userId;

    @Column(name = "Name", nullable = true)
    private String name;

    @Column(name = "Device_token", nullable = true)
    private String deviceToken;

    @Column(name = "Imei_no", nullable = true)
    private String imeiNo;

    @Column(name = "Platform", nullable = true)
    private String platform;

    @Column(name = "Push_config_created_date", updatable = false)
    @CreationTimestamp
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date pushConfigCreatedDate;

    @Column(name = "Push_config_modified_date")
    @UpdateTimestamp
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date pushConfigModifiedDate;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDeviceToken() {
        return deviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }

    public String getImeiNo() {
        return imeiNo;
    }

    public void setImeiNo(String imeiNo) {
        this.imeiNo = imeiNo;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public Date getPushConfigCreatedDate() {
        return pushConfigCreatedDate;
    }

    public void setPushConfigCreatedDate(Date pushConfigCreatedDate) {
        this.pushConfigCreatedDate = pushConfigCreatedDate;
    }

    public Date getPushConfigModifiedDate() {
        return pushConfigModifiedDate;
    }

    public void setPushConfigModifiedDate(Date pushConfigModifiedDate) {
        this.pushConfigModifiedDate = pushConfigModifiedDate;
    }

}
