/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stomp.firebase;

import java.util.List;

/**
 *
 * @author Welcome
 */
public class ChatTable {

    private String grade;

    private String id;

    private String name;

    private List<String> name_Search;

    private String schoolName;

    private String user_Type_Id;

    private String grp_chat_Id;

    private List<Integer> grp_per_ID;

    private String unreadCount;

    private String updatedDateTime;

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getName_Search() {
        return name_Search;
    }

    public void setName_Search(List<String> name_Search) {
        this.name_Search = name_Search;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public String getUser_Type_Id() {
        return user_Type_Id;
    }

    public void setUser_Type_Id(String user_Type_Id) {
        this.user_Type_Id = user_Type_Id;
    }

    public String getGrp_chat_Id() {
        return grp_chat_Id;
    }

    public void setGrp_chat_Id(String grp_chat_Id) {
        this.grp_chat_Id = grp_chat_Id;
    }

    public List<Integer> getGrp_per_ID() {
        return grp_per_ID;
    }

    public void setGrp_per_ID(List<Integer> grp_per_ID) {
        this.grp_per_ID = grp_per_ID;
    }

    public String getUnreadCount() {
        return unreadCount;
    }

    public void setUnreadCount(String unreadCount) {
        this.unreadCount = unreadCount;
    }

    public String getUpdatedDateTime() {
        return updatedDateTime;
    }

    public void setUpdatedDateTime(String updatedDateTime) {
        this.updatedDateTime = updatedDateTime;
    }

}
