/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stomp.firebase;

/**
 *
 * @author Welcome
 */
import com.google.api.core.ApiFuture;
import com.google.cloud.firestore.DocumentReference;
import com.google.cloud.firestore.Firestore;
import com.google.cloud.firestore.QueryDocumentSnapshot;
import com.google.cloud.firestore.QuerySnapshot;
import com.google.cloud.firestore.WriteBatch;
import com.google.cloud.firestore.WriteResult;
import com.google.firebase.cloud.FirestoreClient;
import com.response.ApiResponse;
import java.util.ArrayList;
import java.util.List;

import java.util.concurrent.ExecutionException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

//CRUD operations
@Service
//@RestController
//@RequestMapping(value = "/bb/fireBase")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class FBService {

    public static final String COL_NAME = "users";

    public String savePatientDetails(ChatList patient) throws InterruptedException, ExecutionException {
        Firestore dbFirestore = FirestoreClient.getFirestore();
        ApiFuture<WriteResult> collectionsApiFuture = dbFirestore.collection(COL_NAME).document(patient.getName()).set(patient);
        return collectionsApiFuture.get().getUpdateTime().toString();
    }

//    public Patient getPatientDetails(String name) throws InterruptedException, ExecutionException {
//        Firestore dbFirestore = FirestoreClient.getFirestore();
//        DocumentReference documentReference = dbFirestore.collection(COL_NAME).document(name);
//        ApiFuture<DocumentSnapshot> future = documentReference.get();
//
//        DocumentSnapshot document = future.get();
//
//        Patient patient = null;
//
//        if (document.exists()) {
//            patient = document.toObject(Patient.class);
//            return patient;
//        } else {
//            return null;
//        }
//    }
//
//    public String updatePatientDetails(Patient person) throws InterruptedException, ExecutionException {
//        Firestore dbFirestore = FirestoreClient.getFirestore();
//        ApiFuture<WriteResult> collectionsApiFuture = dbFirestore.collection(COL_NAME).document(person.getName()).set(person);
//        return collectionsApiFuture.get().getUpdateTime().toString();
//    }
//
//    public String deletePatient(String name) {
//        Firestore dbFirestore = FirestoreClient.getFirestore();
//        ApiFuture<WriteResult> writeResult = dbFirestore.collection(COL_NAME).document(name).delete();
//        return "Document with Patient ID " + name + " has been deleted";
//    }
    @PostMapping("/delete_chat")
//    public ApiResponse deleteCollection(@RequestParam("documentId") String id) {
    public ApiResponse deleteCollection(String id) {
        try {
            // retrieve a small batch of documents to avoid out-of-memory errors
            Firestore dbFirestore = FirestoreClient.getFirestore();
            ApiFuture<QuerySnapshot> collectionsApiFuture = dbFirestore.collection("chat_list_table").document(id).collection("chat_list").limit(300).get();
//            ApiFuture<QuerySnapshot> future = collection.limit(batchSize).get();
            int deleted = 0;
            //future.get() blocks on document retrieval
            List<QueryDocumentSnapshot> documents = collectionsApiFuture.get().getDocuments();
            for (QueryDocumentSnapshot document : documents) {
                document.getReference().delete();
                ++deleted;
            }
            if (deleted >= 200) {
                // retrieve and delete another batch
                deleteCollection(id);
            }
            return new ApiResponse(HttpStatus.OK, "Chat Deleted Successfully", "Success");
        } catch (Exception e) {
            return new ApiResponse(HttpStatus.OK, "Error", e.getMessage());
        }
    }

    @PostMapping("/add_chat")
//    public ApiResponse addCollection(@RequestParam("documentId") String id, @RequestParam("documentList") List<ChatTable> list) {
    public ApiResponse addCollection(String id,List<ChatTable> list) {
        try {
            // retrieve a small batch of documents to avoid out-of-memory errors
            Firestore dbFirestore = FirestoreClient.getFirestore();
//            ApiFuture<QuerySnapshot> collectionsApiFuture = dbFirestore.collection("chat_list_table").document(id).collection("chat_list").limit(300).get();
//            ApiFuture<QuerySnapshot> future = collection.limit(batchSize).get();

/// Batch Thing //
            List<List<ChatTable>> parts = chopped(list, 400);
            System.out.println(parts.size()); // prints "[[5, 3, 1], [2, 9, 5], [0, 7]]"
            for (int i = 0; i < parts.size(); i++) {
                WriteBatch batch = dbFirestore.batch();
                for (ChatTable li : parts.get(i)) {
                    DocumentReference ref = dbFirestore.collection("chat_list_table").document(id).collection("chat_list").document("" + li.getId());
                    batch.set(ref, li);
                }
                ApiFuture<List<WriteResult>> future = batch.commit();
            }

            return new ApiResponse(HttpStatus.OK, "Chat Added Successfully", "Success");
        } catch (Exception e) {
            return new ApiResponse(HttpStatus.BAD_REQUEST, "Error", e.getMessage());
        }
    }

    // chops a list into non-view sublists of length L
    static <T> List<List<ChatTable>> chopped(List<ChatTable> list, final int L) {
        List<List<ChatTable>> parts = new ArrayList<List<ChatTable>>();
        final int N = list.size();
        for (int i = 0; i < N; i += L) {
            parts.add(new ArrayList<ChatTable>(
                    list.subList(i, Math.min(N, i + L)))
            );
        }
        return parts;
    }

    @PostMapping("/add_chat1")
    public void split(@RequestBody List<ChatTable> list) {

        List<List<ChatTable>> parts = chopped(list, 3);
        System.out.println(parts.size()); // prints "[[5, 3, 1], [2, 9, 5], [0, 7]]"
        for (int i = 0; i < parts.size(); i++) {
            for (ChatTable li : parts.get(i)) {
                System.out.println("element" + li); // prints "[[5, 3, 1, -1], [2, 9, 5], [0, 7]]"
            }
        }
        System.out.println(parts); // prints "[[5, 3, 1, -1], [2, 9, 5], [0, 7]]"
    }
}
