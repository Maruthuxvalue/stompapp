/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stomp.firebase;

import java.util.List;

/**
 *
 * @author Admin
 */
public class ChatRequest {

    private String documentId;
    private List<ChatTable> documentList;

    public String getDocumentId() {
        return documentId;
    }

    public void setDocumentId(String documentId) {
        this.documentId = documentId;
    }

    public List<ChatTable> getDocumentList() {
        return documentList;
    }

    public void setDocumentList(List<ChatTable> documentList) {
        this.documentList = documentList;
    }

}
