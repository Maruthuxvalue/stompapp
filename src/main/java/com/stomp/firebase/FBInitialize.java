/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stomp.firebase;

/**
 *
 * @author Welcome
 */
import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.InputStream;
import org.hibernate.internal.util.ConfigHelper;

@Service
public class FBInitialize {

    @PostConstruct
    public void initialize() {
        try {
//            InputStream serviceAccount = ConfigHelper.getResourceAsStream("json/bullingbuddy.json");
            InputStream serviceAccount = ConfigHelper.getResourceAsStream("json/stomp.json");
//            FileInputStream serviceAccount
//                    = new FileInputStream("json/bullingbuddy.json");

//            FirebaseOptions options = new FirebaseOptions.Builder()
//                    .setCredentials(GoogleCredentials.fromStream(serviceAccount))
//                    //                    .setDatabaseUrl("https://bullingbuddy.firebaseio.com")
//                    .setDatabaseUrl("https://bullying-buddy-5cc77-default-rtdb.firebaseio.com")
//                    .build();
            FirebaseOptions options = new FirebaseOptions.Builder()
                    .setCredentials(GoogleCredentials.fromStream(serviceAccount))
                    .setDatabaseUrl("https://stomp-c5094-default-rtdb.firebaseio.com")
                    .build();

            FirebaseApp.initializeApp(options);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
