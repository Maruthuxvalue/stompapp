/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stomp.mail;

/**
 *
 * @author Welcome
 */
import com.response.ApiResponse;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import org.springframework.http.HttpStatus;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.mail.MailSender;
import org.springframework.stereotype.Service;

@Service
public class AmazonSESMail {

//    @Autowired
//    private MailSender mailSender;
    // Replace sender@example.com with your "From" address.
    // This address must be verified.
    static final String FROM = "thebullyingbuddy@gmail.com";
    static final String FROMNAME = "Bullying";

    // Replace smtp_username with your Amazon SES SMTP user name.
//    static final String SMTP_USERNAME = "AKIAWTBAXLKXSX7KDIFR";
    static final String SMTP_USERNAME = "AKIAWTBAXLKX5MJD5ZV4";

    // Replace smtp_password with your Amazon SES SMTP password.
//    static final String SMTP_PASSWORD = "BPwjQWuYoejdC7dQXqhVA4G8mj7+qHu4vkga1A9ju9gJ";
    static final String SMTP_PASSWORD = "BHcI6RFn1dTpVwh1IxvPbW2tEeuXk0YwA3XlLlVuDleP";

    // The name of the Configuration Set to use for this message.
    // If you comment out or remove this variable, you will also need to
    // comment out or remove the header below.
//    static final String CONFIGSET = "ConfigSet";
    // Amazon SES SMTP host name. This example uses the US West (Oregon) region.
    // See https://docs.aws.amazon.com/ses/latest/DeveloperGuide/regions.html#region-endpoints
    // for more information.
    static final String HOST = "email-smtp.us-east-2.amazonaws.com";

    // The port you will connect to on the Amazon SES SMTP endpoint. 
    static final int PORT = 587;

    static final String SUBJECT = "Mail from S.T.O.M.P Application";

    public ApiResponse sendMail(String name, String userName, String emailId) throws Exception {
        String BODY = String.join(
                System.getProperty("line.separator"),
                "<h1>Hi " + name + ", </h1>",
                "<p>You recently requested your username for S.T.O.M.P Application.",
                "<p>And your username for above account is " + userName + ".",
                "<br><br>Thanks,<br><a href=''>S.T.O.M.P Application</a>"
        );

        // Create a Properties object to contain connection configuration information.
        Properties props = System.getProperties();

        props.put(
                "mail.transport.protocol", "smtp");
        props.put(
                "mail.smtp.port", PORT);
        props.put(
                "mail.smtp.starttls.enable", "true");
        props.put(
                "mail.smtp.auth", "true");

        // Create a Session object to represent a mail session with the specified properties. 
        Session session = Session.getDefaultInstance(props);

        // Create a message with the specified information. 
        MimeMessage msg = new MimeMessage(session);

        msg.setFrom(
                new InternetAddress(FROM, FROMNAME));
        msg.setRecipient(Message.RecipientType.TO,
                new InternetAddress(emailId));
        msg.setSubject(SUBJECT);

        msg.setContent(BODY,
                "text/html");

        // Add a configuration set header. Comment or delete the 
        // next line if you are not using a configuration set
//        msg.setHeader("X-SES-CONFIGURATION-SET", CONFIGSET);
        // Create a transport.
        Transport transport = session.getTransport();

        // Send the message.
        try {

            // Connect to Amazon SES using the SMTP username and password you specified above.
            transport.connect(HOST, SMTP_USERNAME, SMTP_PASSWORD);

            // Send the email.
            transport.sendMessage(msg, msg.getAllRecipients());
            return new ApiResponse(HttpStatus.OK, "Mail Sent Successfully", "Success");
        } catch (Exception ex) {
            return new ApiResponse(HttpStatus.BAD_REQUEST, "Exception", ex.getMessage());
        } finally {
            // Close and terminate the connection.
            transport.close();
        }
    }

    public ApiResponse sendVerifyCode(String code, String emailId, String lang) throws Exception {
        String BODY = BODY = String.join(
                System.getProperty("line.separator"),
                "<p>The verification code to reset your password for S.T.O.M.P Application account " + code + ".",
                "<br><br><b>Note:</b><br><p> This verification code will expires in 10 minutes.",
                "<br><br>Thanks,<br><a href=''>Stomp Application</a>"
        );
        if (lang.contains("Spanish")) {
            BODY = String.join(
                    System.getProperty("line.separator"),
                    "<p>El código de verificación para restablecer su contraseña para la cuenta S.T.O.M.P aplicación " + code + ".",
                    "<br><br><b>Nota:</b><br><p> Este código de verificación caducará en 10 minutos.",
                    "<br><br>Gracias,<br><a href=''>S.T.O.M.P aplicación</a>"
            );
        }

        // Create a Properties object to contain connection configuration information.
        Properties props = System.getProperties();

        props.put(
                "mail.transport.protocol", "smtp");
        props.put(
                "mail.smtp.port", PORT);
        props.put(
                "mail.smtp.starttls.enable", "true");
        props.put(
                "mail.smtp.auth", "true");

        // Create a Session object to represent a mail session with the specified properties. 
        Session session = Session.getDefaultInstance(props);

        // Create a message with the specified information. 
        MimeMessage msg = new MimeMessage(session);

        msg.setFrom(
                new InternetAddress(FROM, FROMNAME));
        msg.setRecipient(Message.RecipientType.TO,
                new InternetAddress(emailId));
        msg.setSubject(SUBJECT);

        msg.setContent(BODY,
                "text/html");

        // Add a configuration set header. Comment or delete the 
        // next line if you are not using a configuration set
//        msg.setHeader("X-SES-CONFIGURATION-SET", CONFIGSET);
        // Create a transport.
        Transport transport = session.getTransport();

        // Send the message.
        try {

            // Connect to Amazon SES using the SMTP username and password you specified above.
            transport.connect(HOST, SMTP_USERNAME, SMTP_PASSWORD);

            // Send the email.
            transport.sendMessage(msg, msg.getAllRecipients());
            return new ApiResponse(HttpStatus.OK, "Mail Sent Successfully", "Success");
        } catch (Exception ex) {
            return new ApiResponse(HttpStatus.BAD_REQUEST, "Exception", ex.getMessage());
        } finally {
            // Close and terminate the connection.
            transport.close();
        }
    }

    public ApiResponse sendCyberMail(String user, String cyberType, String emailId, String url) throws Exception {
        String BODY = String.join(
                System.getProperty("line.separator"),
                "<p>Hai  " + cyberType + ",",
                "<br><br><p>'" + user + "' has been harassed via social media.",
                "<br><br> Attachments " + url + " .",
                "<br><br>Thanks,<br><a href=''>S.T.O.M.P Application</a>"
        );

        // Create a Properties object to contain connection configuration information.
        Properties props = System.getProperties();

        props.put(
                "mail.transport.protocol", "smtp");
        props.put(
                "mail.smtp.port", PORT);
        props.put(
                "mail.smtp.starttls.enable", "true");
        props.put(
                "mail.smtp.auth", "true");

        // Create a Session object to represent a mail session with the specified properties. 
        Session session = Session.getDefaultInstance(props);

        // Create a message with the specified information. 
        MimeMessage msg = new MimeMessage(session);

        msg.setFrom(
                new InternetAddress(FROM, FROMNAME));
        msg.setRecipient(Message.RecipientType.TO,
                new InternetAddress(emailId));
        msg.setSubject(SUBJECT);

        msg.setContent(BODY,
                "text/html");

        // Add a configuration set header. Comment or delete the 
        // next line if you are not using a configuration set
//        msg.setHeader("X-SES-CONFIGURATION-SET", CONFIGSET);
        // Create a transport.
        Transport transport = session.getTransport();

        // Send the message.
        try {

            // Connect to Amazon SES using the SMTP username and password you specified above.
            transport.connect(HOST, SMTP_USERNAME, SMTP_PASSWORD);

            // Send the email.
            transport.sendMessage(msg, msg.getAllRecipients());
            return new ApiResponse(HttpStatus.OK, "Mail Sent Successfully", "Success");
        } catch (Exception ex) {
            return new ApiResponse(HttpStatus.BAD_REQUEST, "Exception", ex.getMessage());
        } finally {
            // Close and terminate the connection.
            transport.close();
        }
    }
}
