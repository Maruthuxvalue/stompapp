/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stomp.mail;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import org.springframework.context.annotation.Configuration;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailService;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailServiceClientBuilder;
import org.springframework.cloud.aws.mail.simplemail.SimpleEmailServiceMailSender;
import org.springframework.context.annotation.Bean;
import org.springframework.mail.MailSender;

/**
 *
 * @author Welcome
 */
@Configuration
public class AppConfig {

//    @Bean
//    public AmazonSimpleEmailService amazonSimpleEmailService(AmazonSimpleEmailService amazonSimpleEmailService) {
//        // if use ses region change, write below.
//        amazonSimpleEmailService.setRegion(Region.getRegion(Regions.US_EAST_2));
//        return amazonSimpleEmailService;
//    }
//    @Bean
//    public AmazonSimpleEmailService amazonSimpleEmailService(AWSCredentialsProvider credentialsProvider) {
//         return AmazonSimpleEmailServiceClientBuilder.standard()
//            .withCredentials(credentialsProvider)
//            .withRegion(Regions.US_EAST_2).build();
//    }
    @Bean
    public AmazonSimpleEmailService amazonSimpleEmailService() {

        BasicAWSCredentials basicAWSCredentials
                = new BasicAWSCredentials("AKIAWTBAXLKXSX7KDIFR", "BPwjQWuYoejdC7dQXqhVA4G8mj7+qHu4vkga1A9ju9gJ");

        return AmazonSimpleEmailServiceClientBuilder.standard()
                .withCredentials(new AWSStaticCredentialsProvider(basicAWSCredentials))
                .withRegion(Regions.US_EAST_2)
                .build();
    }

    @Bean
    public MailSender mailSender(AmazonSimpleEmailService ses) {
        return new SimpleEmailServiceMailSender(ses);
    }

}
