/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.stomp.order;

import com.amazonaws.services.simpleemail.model.TlsPolicy;
import com.response.ApiResponse;
import com.stomp.states.States;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PostMapping;

/**
 *
 * @author Admin
 */
@Service
@PropertySource(value =("application.properties"))
public class OrderController {
    
@Autowired
OrderRepository orderRepos;

//@Transactional()
@PostMapping("/saveOrder")
public ApiResponse saveOrders(Orderdemo obj){
    String langSave = "Record Saved Successfully";
    String message = "";
    try {
    message = langSave;
    //obj.setProduct_name("pdt-1");
    //obj.setPrice(priceVal);
    //System.out.println(obj);
    Orderdemo orderSave = orderRepos.save(obj);
    return new ApiResponse(HttpStatus.OK, message, orderSave);
    } catch (Exception e) {
            return new ApiResponse(HttpStatus.BAD_REQUEST, "Error", e.getMessage());
        }
}

@PostMapping("/order_list")
    public ApiResponse getAllOrderList() {

        List<Orderdemo> orderList = new ArrayList();
        try {
            orderList = orderRepos.findAll();
              return new ApiResponse(HttpStatus.OK, "Record retrieved successfully", orderList);
        } catch (Exception e) {
            return new ApiResponse(HttpStatus.BAD_REQUEST, "Exception", e.getMessage());

        }
      
    }
    @PostMapping("/order_update")
public ApiResponse updateOrder(Orderdemo obj)
{
    String orderMsg = "Order Updated Successfully";
    String OrderNotFound ="No Order Found";
    HttpStatus status = HttpStatus.OK;
    try{
        Orderdemo orderUpdate = new Orderdemo();
        Optional<Orderdemo> OrderDetails = orderRepos.findById(obj.getId());
        if(OrderDetails.isPresent())
        {
        orderUpdate = OrderDetails.get();
        orderUpdate.setProduct_name(obj.getProduct_name());
        orderUpdate.setPrice(obj.getPrice());
        orderRepos.save(orderUpdate);
        }else{                
        orderMsg = OrderNotFound;
        status = HttpStatus.NOT_FOUND;                
        }
        //Orderdemo orderUpdate = orderRepos.save(obj);
        return new ApiResponse(status, orderMsg, orderUpdate);
    }catch(Exception e){
        return new ApiResponse(HttpStatus.BAD_REQUEST,"Error",e.getMessage());
    }
}
@PostMapping("/delete_order")
public ApiResponse deleteOrder(Orderdemo obj)
{
    String OrderMsg = "Order Deleted Successfully";
    HttpStatus status = HttpStatus.OK;
    String OrderSuccess = "success";
    try{
    Orderdemo OrderDetails = orderRepos.findById(obj.getId()).get();
    if(OrderDetails != null)
    {
        orderRepos.delete(OrderDetails);
        return new ApiResponse(status, OrderMsg, OrderSuccess);
    }
    else
    {
        status = HttpStatus.BAD_REQUEST;
        OrderSuccess = "Error";
        return new ApiResponse(status, OrderMsg, OrderSuccess);
    }
    }catch (Exception e){
        return new ApiResponse(status, OrderMsg, e.getMessage());
    }
}

    private ApiResponse ApiResponse(HttpStatus httpStatus, String error, String message) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
