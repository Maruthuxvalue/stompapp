/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stomp.states;

import com.response.ApiResponse;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Welcome
 */
//@RestController
//@RequestMapping(value = "/bb/states")
@Service
public class StatesController {

    @Autowired
    StatesRepository statesRepository;

    @PostMapping("/get_all_states")
    public ApiResponse getStates() {

        List<States> statesList = new ArrayList();
        try {
            statesList = statesRepository.findAll();
        } catch (Exception e) {
            return new ApiResponse(HttpStatus.BAD_REQUEST, "Exception", e.getMessage());

        }
        return new ApiResponse(HttpStatus.OK, "Record retrieved successfully", statesList);
    }
}
