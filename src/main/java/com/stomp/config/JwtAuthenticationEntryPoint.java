package com.stomp.config;

import java.io.IOException;
import java.io.Serializable;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.context.annotation.Configuration;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.CrossOrigin;

@Configuration
@CrossOrigin(origins = "*", allowedHeaders = "*")

public class JwtAuthenticationEntryPoint implements AuthenticationEntryPoint, Serializable {

    private static final long serialVersionUID = -7858869558953243875L;

    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response,
            AuthenticationException authException) throws IOException {
        if (response.getStatus() == 504) {
            response.sendError(HttpServletResponse.SC_GATEWAY_TIMEOUT, "Token has expired");
        } else if (response.getStatus() == 204) {
            response.sendError(HttpServletResponse.SC_NO_CONTENT, "Unable to get Token");
        } else if (response.getStatus() == 406) {
            response.sendError(HttpServletResponse.SC_NOT_ACCEPTABLE, "Token does not begin with Bearer String");
        } else if (response.getStatus() == 203) {
            response.sendError(HttpServletResponse.SC_NON_AUTHORITATIVE_INFORMATION, "Invalid Token");
        } else {
            response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Unauthorized");
        }

    }
}
