/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stomp.reporting;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Welcome
 */
@Entity
@Table(name = "cyberSupport")
public class CyberSupport implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Id", nullable = false, updatable = false)
    private int id;

    @Column(name = "FbCyber", nullable = false)
    private String fbCyber;

    @Column(name = "InstaCyber", nullable = false)
    private String instaCyber;

    @Column(name = "SnapShotCyber", nullable = false)
    private String snapShotCyber;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFbCyber() {
        return fbCyber;
    }

    public void setFbCyber(String fbCyber) {
        this.fbCyber = fbCyber;
    }

    public String getInstaCyber() {
        return instaCyber;
    }

    public void setInstaCyber(String instaCyber) {
        this.instaCyber = instaCyber;
    }

    public String getSnapShotCyber() {
        return snapShotCyber;
    }

    public void setSnapShotCyber(String snapShotCyber) {
        this.snapShotCyber = snapShotCyber;
    }

}
