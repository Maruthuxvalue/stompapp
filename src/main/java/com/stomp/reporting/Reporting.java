/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stomp.reporting;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.Transient;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

/**
 *
 * @author Welcome
 */
@Entity
@Table(name = "reporting")
@NamedQuery(name = "Reporting.fetchForParentId", query = "SELECT report,user.name as childName \n"
        + "FROM Users AS user \n"
        + "INNER JOIN Reporting AS report ON report.userId=user.id \n"
        + "WHERE report.userId=:userId ORDER BY report.id DESC")
public class Reporting implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Id", nullable = false, updatable = false)
    private long id;

    @Column(name = "UserId", nullable = false)
    private long userId;

    @Column(name = "Name", nullable = false)
    private String name;

    @Column(name = "Grade", nullable = true)
    private String grade;

    @Column(name = "OrgId", nullable = true)
    private Integer orgId;

    @Column(name = "OrgTypeId", nullable = true)
    private Integer orgTypeId;

    @Column(name = "Age", nullable = false)
    private int age;

    @Column(name = "Gender", nullable = false)
    private char gender;

    @Column(name = "BullyName", nullable = false)
    private String bullyName;

    @Column(name = "IncidentPlace", nullable = false)
    private String incidentPlace;

    @Column(name = "IsBullyClassmate", nullable = false)
    private char isBullyClassmate;

    @Column(name = "BullyGender", nullable = false)
    private char bullyGender;

    @Column(name = "NoOfDaysBullied", nullable = false)
    private int noOfDaysBullied;

    @Column(name = "CalenderType", nullable = false)
    private String calenderType;

    @Column(name = "BullyType", nullable = false)
    private String bullyType;

    @Column(name = "CyberType", nullable = true)
    private String cyberType;

    @Column(name = "BullyDescription", nullable = false)
    private String bullyDescription;

    @Column(name = "VideoURL", nullable = false)
    private String videoURL;

    @Column(name = "BullyStatus", nullable = false)
    private char bullyStatus;

    @Column(name = "FriendId", nullable = true)
    private Long friendId;

    @Column(name = "BusRoute", nullable = true)
    private String busRoute;
    
    @Column(name = "OtherIncidentType", nullable = true)
    private String otherIncidentType;
    
    @Column(name = "OtherIncidentLocation", nullable = true)
    private String otherIncidentLocation;

    @Column(name = "CreatedDateTime", updatable = false)
    @CreationTimestamp
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date createdDateTime;

    @Column(name = "ModifiedDateTime")
    @UpdateTimestamp
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date modifiedDateTime;

    @Transient
    private String schoolName;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public Integer getOrgId() {
        return orgId;
    }

    public void setOrgId(Integer orgId) {
        this.orgId = orgId;
    }

    public Integer getOrgTypeId() {
        return orgTypeId;
    }

    public void setOrgTypeId(Integer orgTypeId) {
        this.orgTypeId = orgTypeId;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public char getGender() {
        return gender;
    }

    public void setGender(char gender) {
        this.gender = gender;
    }

    public String getBullyName() {
        return bullyName;
    }

    public void setBullyName(String bullyName) {
        this.bullyName = bullyName;
    }

    public String getIncidentPlace() {
        return incidentPlace;
    }

    public void setIncidentPlace(String incidentPlace) {
        this.incidentPlace = incidentPlace;
    }

    public char getIsBullyClassmate() {
        return isBullyClassmate;
    }

    public void setIsBullyClassmate(char isBullyClassmate) {
        this.isBullyClassmate = isBullyClassmate;
    }

    public char getBullyGender() {
        return bullyGender;
    }

    public void setBullyGender(char bullyGender) {
        this.bullyGender = bullyGender;
    }

    public int getNoOfDaysBullied() {
        return noOfDaysBullied;
    }

    public void setNoOfDaysBullied(int noOfDaysBullied) {
        this.noOfDaysBullied = noOfDaysBullied;
    }

    public String getCalenderType() {
        return calenderType;
    }

    public void setCalenderType(String calenderType) {
        this.calenderType = calenderType;
    }

    public String getBullyType() {
        return bullyType;
    }

    public void setBullyType(String bullyType) {
        this.bullyType = bullyType;
    }

    public String getCyberType() {
        return cyberType;
    }

    public void setCyberType(String cyberType) {
        this.cyberType = cyberType;
    }

    public String getBullyDescription() {
        return bullyDescription;
    }

    public void setBullyDescription(String bullyDescription) {
        this.bullyDescription = bullyDescription;
    }

    public String getVideoURL() {
        return videoURL;
    }

    public void setVideoURL(String videoURL) {
        this.videoURL = videoURL;
    }

    public char getBullyStatus() {
        return bullyStatus;
    }

    public void setBullyStatus(char bullyStatus) {
        this.bullyStatus = bullyStatus;
    }

    public Date getCreatedDateTime() {
        return createdDateTime;
    }

    public void setCreatedDateTime(Date createdDateTime) {
        this.createdDateTime = createdDateTime;
    }

    public Date getModifiedDateTime() {
        return modifiedDateTime;
    }

    public void setModifiedDateTime(Date modifiedDateTime) {
        this.modifiedDateTime = modifiedDateTime;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public Long getFriendId() {
        return friendId;
    }

    public void setFriendId(Long friendId) {
        this.friendId = friendId;
    }

    public String getBusRoute() {
        return busRoute;
    }

    public void setBusRoute(String busRoute) {
        this.busRoute = busRoute;
    }

    public String getOtherIncidentType() {
        return otherIncidentType;
    }
    
    public void setOtherIncidentType(String otherIncidentType) {
        this.otherIncidentType = otherIncidentType;
    }

    public String getOtherIncidentLocation() {
        return otherIncidentLocation;
    }

    public void setOtherIncidentLocation(String otherIncidentLocation) {
        this.otherIncidentLocation = otherIncidentLocation;
    }

}
