/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stomp.reporting;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author Welcome
 */
public interface ReportingRepository extends JpaRepository<Reporting, Long> {

    List<Reporting> findAllByUserIdOrderByIdDesc(long userId);

    List<Reporting> findAllByOrgIdOrderByIdDesc(int orgId);

    List<Reporting> findAllByOrgIdAndBusRouteOrderByIdDesc(int orgId, String route);

    List<Object> fetchForParentId(@Param("userId") long id);

}
