/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stomp.reporting;

import com.stomp.busRoutes.BusRoutes;
import com.stomp.busRoutes.BusRoutesRepository;
import com.stomp.mail.AmazonSESMail;
import com.stomp.notifications.NotificationsRepository;
import com.stomp.notifications.PushNotifications;
import com.stomp.org.Organization;
import com.stomp.org.OrgRepository;
import com.stomp.twilio.SMSController;
import com.stomp.user.ParentChild;
import com.stomp.user.ParentChildRepository;
import com.stomp.user.Users;
import com.stomp.user.UserRepository;
import com.response.ApiResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author Welcome
 */
//@RestController
//@RequestMapping(value = "/bb/reporting")
@Service
@PropertySource(value = {"application.properties"})
public class ReportingController {

    @Autowired
    ReportingRepository reportRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    BusRoutesRepository busRepository;

    @Autowired
    NotificationsRepository notifyRepository;

    @Autowired
    SMSController smsService;

    @Autowired
    OrgRepository schoolsRepository;

    @Autowired
    AmazonSESMail mailService;

    @Autowired
    ParentChildRepository parChildRepository;

    @Autowired
    CyberSupportRepository cyberSupportRepository;

    @Value("${phpip:}")
    private String ip;

    @PostMapping("/get_user_reports")
    public ApiResponse getUsersReports(Reporting obj) {
        String langRetrieved = "Record retrieved successfully";
        List<Reporting> list = new ArrayList();
        try {
            Users lang = userRepository.findById(obj.getUserId()).get();
            if (lang.getUserLang().contains("Spanish")) {
                langRetrieved = "Registro recuperado con éxito";
            }
            list = reportRepository.findAllByUserIdOrderByIdDesc(obj.getUserId());
            list.stream()
                    .forEach((ele) -> {
                        if (ele.getOrgId() != null && ele.getOrgId() != 0) {
                            Organization sch = schoolsRepository.findById(ele.getOrgId()).get();
                            ele.setSchoolName(sch.getOrgName());
                        }
                    });
        } catch (Exception e) {
            return new ApiResponse(HttpStatus.BAD_REQUEST, "Exception", e.getMessage());
        }
        return new ApiResponse(HttpStatus.OK, langRetrieved, list);
    }

    @PostMapping("/reporting_save")
    @Transactional()
    public ApiResponse saveReports(Reporting obj) {
        String pushLangMsg = " has reported a Abuse/Harassment Incident.";
        String langGrade = " (Grade: "; 
        String langChild = "Your Friend/Spouse/Parent ";
        String langTracing = " Please contact the admin using the chat on this app or call them immediately.";
        String langPhoneCall = " You can chat him through STOMP application Chat or call him by his phone.";
        String langSave = "Record Saved Successfully";
        String langBullied = " has been abused by the following person ";
        String message = "";
        Users childBus = new Users();
        try {
            Users userDetails = userRepository.findById(obj.getUserId()).get();
            Organization sch = new Organization();
            if (userDetails.getOrgId() != 0 && userDetails.getUserTypeId() == 1) {
                sch = schoolsRepository.findById(userDetails.getOrgId()).get();
            }
            if (userDetails.getUserLang().contains("Spanish")) {
                pushLangMsg = " ha reportado un incidente de acoso.";
                langSave = "Registro guardado con éxito";
                langGrade = "Grado";
                langTracing = "  Por favor comuníquese con el administrador mediante el chat de esta aplicación STOMP o llámelos de inmediato.";
                langPhoneCall = " Usted puede charlar con él a través de la aplicación STOMP Chat o llamarlo por su teléfono.";
                langBullied = " ha sido acosado por el siguiente persona ";
            }
            if (obj.getVideoURL() == null) {
                obj.setVideoURL("images/Group+10178.png");
            }
            if (obj.getIncidentPlace().contains("Bus") && userDetails.getUserTypeId() == 3 && obj.getFriendId() > 0 && obj.getFriendId() != null) {
                childBus = userRepository.findById(obj.getFriendId()).get();
                if (childBus != null && childBus.getBusRoute() != null) {
                    obj.setBusRoute(childBus.getBusRoute());
                }
            } else {
                obj.setBusRoute(null);
            }
            Reporting reportSave = reportRepository.save(obj);
            message = langSave;
            PushNotifications notifySave = new PushNotifications();
            notifySave.setUserId(obj.getUserId());
            notifySave.setReportId(reportSave.getId());
            String pushMessage = "";
            if (userDetails.getUserTypeId() == 1 && obj.getBullyStatus() == 1) { //reporting self
                //pushMessage = obj.getName() + langGrade + obj.getGrade() + ") " + pushLangMsg;
                pushMessage = obj.getName() + pushLangMsg;
            } else {
                //pushMessage = obj.getName() + langGrade + obj.getGrade() + ") " + langBullied + obj.getBullyName() + ".";
                pushMessage = obj.getName()  + langBullied + obj.getBullyName() + ".";
            }
            notifySave.setMessage(pushMessage);
            notifySave.setIsFriend('N');
            notifySave.setIsManager('N');
            if (obj.getIncidentPlace().contains("Bus") || obj.getIncidentPlace().contains("bus")) {
                notifySave.setIsDriver('N');
            } else {
                notifySave.setIsDriver('Y'); // don't want to show the notifications to driver
            }
            notifySave.setIsAdmin('N');
            notifySave.setIsSuperAdmin('N');
            notifyRepository.save(notifySave);
            if (userDetails.getUserTypeId() == 1) {
                List<ParentChild> parents = parChildRepository.findAllByUserId(userDetails.getId());

                if (parents != null && ((sch.getIsBoarding() == null) || sch.getIsBoarding().contains("NO"))) {
                    for (ParentChild item : parents) {
                        PushNotification(item.getFriendId(), langChild + obj.getName() + pushLangMsg + langTracing, "incident");
                    }
                }
//                if (userDetails.getFriendId() != null && userDetails.getFriendId() > 0) {
//                    PushNotification(userDetails.getFriendId(), "Your Child " + obj.getName() + " has reported a Abuse/Harassment Incident. Please contact the school admin using the chat on this app or call them immediately.");
//                }
                if (userDetails.getOrgId() != null && userDetails.getOrgId() != 0) {
                    if (obj.getOrgId() == userDetails.getOrgId()) { // if reported incident by self
                        if (obj.getIncidentPlace().contains("Bus") || obj.getIncidentPlace().contains("bus")) {
                            if (userDetails.getBusRoute() != null && userDetails.getBusRoute().length() > 0) {
                                BusRoutes bus = busRepository.findTop1ByOrgIdAndBusRouteIgnoreCase(userDetails.getOrgId(), userDetails.getBusRoute());
                                if (bus != null) {
                                    PushNotification(bus.getDriverId(), pushMessage, "incident");
                                }
                            }
                        }

                        List<Users> pushAdmin = userRepository.findAllByUserTypeIdAndOrgId(5, userDetails.getOrgId());
                        if (pushAdmin != null) {
                            for (Users list : pushAdmin) {
                                PushNotification(list.getId(), obj.getName()  + pushLangMsg + langPhoneCall, "incident");
                            }
                        }
                        List<Users> pushTeacher = userRepository.findAllByUserTypeIdAndOrgId(2, userDetails.getOrgId());
                        if (pushTeacher != null) {
                            for (Users list : pushTeacher) {
                                PushNotification(list.getId(), obj.getName()  + pushLangMsg, "incident");
                            }
                        }
                    } else {          /// reported incident as watcher
                        if (obj.getOrgId() != null) {

                            List<Users> pushAdmin = userRepository.findAllByUserTypeIdAndOrgId(5, obj.getOrgId());
                            if (pushAdmin != null) {
                                for (Users list : pushAdmin) {
                                    PushNotification(list.getId(), obj.getName()  + langBullied + obj.getBullyName() + ".", "incident");
                                }
                            }
                        }
                    }
                }

                if (obj.getCyberType() != null) {

//                    CyberSupport cyber = cyberSupportRepository.findById(1).get();
//                    switch (obj.getCyberType()) {
//                        case "F":
//                            mailService.sendCyberMail(userDetails.getFbCyber(), "Facebook", cyber.getFbCyber(), "https://bullybucket.s3-us-west-2.amazonaws.com/" + obj.getVideoURL());
//                            break;
//                        case "I":
//                            mailService.sendCyberMail(userDetails.getInstaCyber(), "Instagram", cyber.getInstaCyber(), "https://bullybucket.s3-us-west-2.amazonaws.com/" + obj.getVideoURL());
//                            break;
//                        case "S":
//                            mailService.sendCyberMail(userDetails.getSnapShotCyber(), "Snapshot", cyber.getSnapShotCyber(), "https://bullybucket.s3-us-west-2.amazonaws.com/" + obj.getVideoURL());
//                            break;
//                    }
                }
            } else if (userDetails.getUserTypeId() == 3 && obj.getOrgId() > 0) {
                if (userDetails.getOrgId() != null && userDetails.getOrgId() != 0) {

                    List<Users> pushAdmin = userRepository.findAllByUserTypeIdAndOrgId(5, obj.getOrgId());
                    if (pushAdmin != null) {
                        for (Users list : pushAdmin) {
                            PushNotification(list.getId(), obj.getName() + langBullied + obj.getBullyName() + ".", "incident");
                        }
                    }
                    if (obj.getGrade() != null) {
                        List<Users> pushTeacher = userRepository.findAllByUserTypeIdAndOrgIdAndGrade(2, obj.getOrgId(), obj.getGrade());
                        if (pushTeacher != null) {
                            for (Users list : pushTeacher) {
                                PushNotification(list.getId(), obj.getName() + langBullied + obj.getBullyName() + ".", "incident");
                            }
                        }
                    }
                    if (obj.getIncidentPlace().contains("Bus") && obj.getFriendId() > 0 && obj.getFriendId() != null) {
                        if (childBus.getBusRoute() != null && childBus.getBusRoute().length() > 0) {
                            BusRoutes bus = busRepository.findTop1ByOrgIdAndBusRouteIgnoreCase(childBus.getOrgId(), childBus.getBusRoute());
                            if (bus != null) {
                                PushNotification(bus.getDriverId(), pushMessage, "incident");
                            }
                        }
                    }
                }
            } else if (userDetails.getUserTypeId() == 2 || userDetails.getUserTypeId() == 4) {
                if (userDetails.getOrgId() != null && userDetails.getOrgId() != 0) {

                    if (obj.getOrgId() == userDetails.getOrgId()) { ///  if reported for same school
                        List<Users> pushAdmin = userRepository.findAllByUserTypeIdAndOrgId(5, userDetails.getOrgId());
                        if (pushAdmin != null) {
                            for (Users list : pushAdmin) {
                                PushNotification(list.getId(), obj.getName() + langBullied + obj.getBullyName() + ".", "incident");
                            }
                        }
                    } else {   /// reported for other school
                        List<Users> pushAdmin = userRepository.findAllByUserTypeIdAndOrgId(5, obj.getOrgId());
                        if (pushAdmin != null) {
                            for (Users list : pushAdmin) {
                                PushNotification(list.getId(), obj.getName() + langBullied + obj.getBullyName(), "incident");
                            }
                        }
                    }
                }
            }
            return new ApiResponse(HttpStatus.OK, message, reportSave);
//        } catch (IOException e) {
//            return new ApiResponse(HttpStatus.BAD_REQUEST, "Error", "Error");
        } catch (Exception e) {
            return new ApiResponse(HttpStatus.BAD_REQUEST, "Error", e.getMessage());
        }
    }

    public ResponseEntity<String> PushNotification(long id, String message, String page) {
        RestTemplate restTemplate = new RestTemplate();
        String url = "http://" + ip + "/bb_php/index.php/push/send_pushnotify_android";
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        MultiValueMap<String, Object> map = new LinkedMultiValueMap<String, Object>();
        map.add("user_id", id);
        map.add("message", message);
        map.add("page_name", page);
        HttpEntity<MultiValueMap<String, Object>> request = new HttpEntity<MultiValueMap<String, Object>>(map, headers);
        ResponseEntity<String> postForObject = restTemplate.postForEntity(url, request, String.class);
        return postForObject;
    }

    @PostMapping("/reporting_update")
    @Transactional(rollbackFor = Exception.class)
    public ApiResponse UpdateReport(Reporting obj) {
        String langUpdate = "Record Updated Successfully";
        String langNotFound = "Record not Found";
        String message = "";
        Reporting reportUpdate = new Reporting();
        Optional<Reporting> reportDetail = reportRepository.findById(obj.getId());
        HttpStatus status = HttpStatus.OK;
        try {
            if (reportDetail.isPresent()) {
                reportUpdate = reportDetail.get();
                Users userDetails = userRepository.findById(reportUpdate.getUserId()).get();
                if (userDetails.getUserLang().contains("Spanish")) {
                    langUpdate = "Registro actualizado con éxito";
                    langNotFound = "Registro no encontrado";
                }
                reportUpdate.setVideoURL(obj.getVideoURL());
                reportRepository.save(reportUpdate);
                message = langUpdate;
            } else {
                message = langNotFound;
                status = HttpStatus.NOT_FOUND;
            }
            return new ApiResponse(status, message, reportUpdate);
        } catch (Exception e) {
            return new ApiResponse(HttpStatus.BAD_REQUEST, "Exception", e.getMessage());
        }
    }

    @PostMapping("/get_reports_by_parent_id")
    public ApiResponse getReportsForParent(Users params) {
        String langRetrieved = "Record retrieved successfully";
        List<ParentChild> childList = parChildRepository.findAllByFriendId(params.getId());
        List<Users> userList = new ArrayList();
        userList.clear();
        Users userlang = userRepository.findById(params.getId()).get();
        if (userlang.getUserLang().contains("Spanish")) {
            langRetrieved = "Registro recuperado con éxito";
        }
        for (ParentChild item : childList) {
            Optional<Users> childReports = userRepository.findById(item.getUserId());
            Users child = new Users();
            if (childReports.isPresent()) {
                child = childReports.get();
                userList.add(child);
            }
        }
        List<Reporting> itemList = new ArrayList();
        List<Reporting> list1 = new ArrayList();
        List<Reporting> list2;
        try {
            if (!userList.isEmpty()) {
                for (Users userDetails : userList) {
                    list1 = reportRepository.findAllByUserIdOrderByIdDesc(userDetails.getId());
                    itemList.addAll(list1);
                }
            }
            list2 = reportRepository.findAllByUserIdOrderByIdDesc(params.getId());
            if (!list2.isEmpty()) {
                itemList.addAll(list2);
            }
            if (!itemList.isEmpty()) {

                Collections.sort(itemList,
                        Comparator.comparingLong(Reporting::getId).reversed());
                itemList.stream()
                        .forEach((ele) -> {
                            if (ele.getOrgId() != null && ele.getOrgId() != 0) {

                                Organization sch = schoolsRepository.findById(ele.getOrgId()).get();
                                ele.setSchoolName(sch.getOrgName());
                            }
                        });
            }
        } catch (Exception e) {
            return new ApiResponse(HttpStatus.BAD_REQUEST, "Exception", e.getMessage());
        }
        return new ApiResponse(HttpStatus.OK, langRetrieved, itemList);
    }

    @PostMapping("/get_reports_by_school_id")
    public ApiResponse getReportsForTeachers(Users params) {
        String langRetrieved = "Record retrieved successfully";
        Set<Reporting> itemList = new HashSet();
        List<Reporting> itemList1 = new ArrayList();
        List<Reporting> list1;
        List<Reporting> list2;
        Users userlang = userRepository.findById(params.getId()).get();
        if (userlang.getUserLang().contains("Spanish")) {
            langRetrieved = "Registro recuperado con éxito";
        }
        try {
            list1 = reportRepository.findAllByOrgIdOrderByIdDesc(params.getOrgId());
            list2 = reportRepository.findAllByUserIdOrderByIdDesc(params.getId());
            itemList.addAll(list1);
            if (!list2.isEmpty()) {
                itemList.addAll(list2);
            }
            itemList1.addAll(itemList);
            Collections.sort(itemList1,
                    Comparator.comparingLong(Reporting::getId).reversed());
            itemList1.stream()
                    .forEach((ele) -> {
                        if (ele.getOrgId() != null && ele.getOrgId() != 0) {

                            Organization sch = schoolsRepository.findById(ele.getOrgId()).get();
                            ele.setSchoolName(sch.getOrgName());
                        }
                    });
        } catch (Exception e) {
            return new ApiResponse(HttpStatus.BAD_REQUEST, "Exception", e.getMessage());
        }
        return new ApiResponse(HttpStatus.OK, langRetrieved, itemList1);
    }

    @PostMapping("/get_reports_by_driver_id")
    public ApiResponse getReportsForDrivers(Users params) {
        String langRetrieved = "Record retrieved successfully";
        List<Reporting> itemList = new ArrayList();
        List<Reporting> list1;
        List<Reporting> list2;
        List<Reporting> list3;
        try {
            BusRoutes userDetails = busRepository.findByDriverId(params.getId());
            Users userlang = userRepository.findById(params.getId()).get();
            if (userlang.getUserLang().contains("Spanish")) {
                langRetrieved = "Registro recuperado con éxito";
            }
            if (userDetails != null) {
                list1 = busRepository.fetchByDriverId(params.getId(), userDetails.getOrgId(), "%Bus%"); ///reports by student
                list2 = reportRepository.findAllByUserIdOrderByIdDesc(params.getId()); //reports by driver
                list3 = reportRepository.findAllByOrgIdAndBusRouteOrderByIdDesc(userDetails.getOrgId(), userDetails.getBusRoute());///reports by parent
                itemList.addAll(list1);
                if (!list2.isEmpty()) {
                    itemList.addAll(list2);
                }
                if (!list3.isEmpty()) {
                    itemList.addAll(list3);
                }
                Collections.sort(itemList,
                        Comparator.comparingLong(Reporting::getId).reversed());
                itemList.stream()
                        .forEach((ele) -> {
                            if (ele.getOrgId() != null && ele.getOrgId() != 0) {

                                Organization sch = schoolsRepository.findById(ele.getOrgId()).get();
                                ele.setSchoolName(sch.getOrgName());
                            }
                        });
            } else {
                itemList = null;
            }
        } catch (Exception e) {
            return new ApiResponse(HttpStatus.BAD_REQUEST, "Exception", e.getMessage());
        }
        return new ApiResponse(HttpStatus.OK, langRetrieved, itemList);
    }
}
