/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stomp.language;

import com.response.ApiResponse;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Welcome
 */
//@RestController
//@RequestMapping(value = "/bb/language")
@Service
public class LanguageController {

    @Autowired
    LanguageRepository langRepository;

    @PostMapping("/list")
    public ApiResponse getChatIds() {
        try {
            List<Language> list = langRepository.findAll();
            return new ApiResponse(HttpStatus.OK, "Record retrieved successfully", list);
        } catch (Exception e) {
            return new ApiResponse(HttpStatus.BAD_REQUEST, "Error", e.getMessage());
        }
    }
}
