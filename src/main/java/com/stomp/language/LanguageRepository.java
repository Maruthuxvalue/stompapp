/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.stomp.language;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author Welcome
 */
public interface LanguageRepository extends JpaRepository<Language, Integer> {
    
}
