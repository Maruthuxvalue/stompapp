/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stomp.groupChat;

import com.stomp.pushConfiguration.PushConfiguration;
import com.stomp.pushConfiguration.PushConfigurationRepository;
import com.stomp.org.Organization;
import com.stomp.org.OrgRepository;
import com.stomp.user.ParentChild;
import com.stomp.user.ParentChildRepository;
import com.stomp.user.Users;
import com.stomp.user.UserRepository;
import com.response.ApiResponse;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Welcome
 */
//@RestController
//@RequestMapping(value = "/bb/chat")
@Service
public class GroupChatController {

    @Autowired
    GroupChatRepository chatRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    PushConfigurationRepository pushConfigRepository;

    @Autowired
    OrgRepository schRepository;

    @Autowired
    ParentChildRepository parChildRepository;

    @PostMapping("/get_group_chatId")
//    public ApiResponse getChatIds(@RequestParam("userTypeId") Integer userTypeId, @RequestParam("id") long id) {
    public ApiResponse getChatIds(Integer userTypeId, long id) {
        String langRetrieved = "Record retrieved successfully";
//    public ApiResponse getChatIds(Users obj) {
        List<GroupChat> list = new ArrayList();
        List<Long> allParents = new ArrayList();
        try {
            switch (userTypeId) {
                case 1:
                    List<Long> adminIds = new ArrayList();
                    allParents.clear();
                    adminIds.clear();
                    GroupChat grpChat = chatRepository.findTop1ByUserId(id);
                    Users sch = userRepository.findById(id).get();
                    if (sch.getUserLang().contains("Spanish")) {
                        langRetrieved = "Registro recuperado con éxito";
                    }
                    Organization schData = schRepository.findById(sch.getOrgId()).get();
                    List<ParentChild> parChild = parChildRepository.findAllByUserId(id);
                    List<Users> allAdmins = userRepository.findAllByUserTypeIdAndOrgId(5, sch.getOrgId());
                    if (!parChild.isEmpty() && !allAdmins.isEmpty()) {
                        parChild.stream().forEach((ele) -> {
                            allParents.add(ele.getFriendId());
                        });
                        if (!allAdmins.isEmpty()) {
                            allAdmins.stream().forEach((ele) -> {
                                adminIds.add(ele.getId());
                            });
                        }
                        grpChat.setFriendId(allParents);
                        grpChat.setAdminId(adminIds);
                        grpChat.setOrgName(schData.getOrgName());
                        grpChat.setGrade(sch.getGrade());
                        list.add(grpChat);
                    } else {
                        list = null;
                    }
                    break;
                case 3:
                    List<ParentChild> list1 = parChildRepository.findAllByFriendId(id);
                    Users lang = userRepository.findById(id).get();
                    if (lang.getUserLang().contains("Spanish")) {
                        langRetrieved = "Registro recuperado con éxito";
                    }
                    for (ParentChild item : list1) {
                        allParents.clear();
                        List<Long> adminIds1 = new ArrayList();
                        adminIds1.clear();
                        Users sch2 = userRepository.findById(item.getUserId()).get();
                        Organization schData2 = schRepository.findById(sch2.getOrgId()).get();
                        List<Users> allAdminsForParent = userRepository.findAllByUserTypeIdAndOrgId(5, sch2.getOrgId());
                        List<ParentChild> childs = parChildRepository.findAllByUserId(item.getUserId());
                        if (!allAdminsForParent.isEmpty() && !childs.isEmpty()) {
                            allAdminsForParent.stream().forEach((ele) -> {
                                if (!adminIds1.contains(ele.getId())) {
                                    adminIds1.add(ele.getId());
                                }
                            });
                            childs.stream().forEach((ele) -> {
                                if (!allParents.contains(ele.getFriendId())) {
                                    allParents.add(ele.getFriendId());
                                }
                            });
                            GroupChat grpChat1 = chatRepository.findTop1ByUserId(item.getUserId());
                            grpChat1.setFriendId(allParents);
                            grpChat1.setAdminId(adminIds1);
                            grpChat1.setOrgName(schData2.getOrgName());
                            grpChat1.setGrade(sch2.getGrade());
                            list.add(grpChat1);
                        }
                    }
                    if (list.isEmpty()) {
                        list = null;
                    }
                    break;
                case 5:
                    List<Long> adminIds2 = new ArrayList();
                    Users adminUser = userRepository.findById(id).get();
                    if (adminUser.getUserLang().contains("Spanish")) {
                        langRetrieved = "Registro recuperado con éxito";
                    }
                    list = chatRepository.fetchAllByOrgId(adminUser.getOrgId());
                    Organization schData3 = schRepository.findById(adminUser.getOrgId()).get();
                    List<Users> allAdminsForGroup = userRepository.findAllByUserTypeIdAndOrgId(5, adminUser.getOrgId());
                    allAdminsForGroup.stream().forEach((ele) -> {
                        adminIds2.add(ele.getId());
                    });
                    for (GroupChat item : list) {
                        List<Long> allParents1 = new ArrayList();
                        allParents1.clear();
                        item.setAdminId(adminIds2);
                        List<ParentChild> parents = parChildRepository.findAllByUserId(item.getUserId());
                        if (!parents.isEmpty()) {
                            parents.stream().forEach((ele) -> {
                                allParents1.add(ele.getFriendId());
                            });
                        }
                        Users student = userRepository.findById(item.getUserId()).get();
                        item.setOrgName(schData3.getOrgName());
                        item.setGrade(student.getGrade());
                        item.setFriendId(allParents1);
                    }
                    break;
            }
            if (list.isEmpty()) {
                list = null;
            }

        } catch (Exception e) {
            return new ApiResponse(HttpStatus.BAD_REQUEST, "Exception", e.getMessage());
        }
        return new ApiResponse(HttpStatus.OK, langRetrieved, list);
    }

    @PostMapping("/get_device_tokens_by_chatId")
    public ApiResponse getDeviceTokens(GroupChat obj) {
        List list = new ArrayList();
        List<PushConfiguration> pushParentList = new ArrayList();
        try {
            GroupChat chat = chatRepository.findByChatId(obj.getChatId());
            PushConfiguration pushStudent = pushConfigRepository.findByUserId(chat.getUserId());
            List<ParentChild> parentList = parChildRepository.findAllByUserId(chat.getUserId());
            parentList.stream().forEach((ele) -> {
                PushConfiguration pushParent = pushConfigRepository.findByUserId(ele.getFriendId());
                if (pushParent != null) {
                    pushParentList.add(pushParent);
                }
            });
            List<Users> allAdmin = userRepository.findAllByUserTypeIdAndOrgId(5, chat.getOrgId());

            if (pushStudent != null) {
                HashMap hm = new HashMap();
                hm.put("deviceToken", pushStudent.getDeviceToken());
                hm.put("id", pushStudent.getUserId());
                hm.put("userTypeId", 1);
                list.add(hm);
            }
            if (!pushParentList.isEmpty()) {
                pushParentList.stream().forEach((ele) -> {
                    HashMap hm = new HashMap();
                    hm.put("deviceToken", ele.getDeviceToken());
                    hm.put("id", ele.getUserId());
                    hm.put("userTypeId", 3);
                    list.add(hm);
                });
            }
            if (!allAdmin.isEmpty()) {
                allAdmin.stream().forEach((ele) -> {
                    PushConfiguration pushAdmin = pushConfigRepository.findByUserId(ele.getId());
                    if (pushAdmin != null) {
                        HashMap hm = new HashMap();
                        hm.put("deviceToken", pushAdmin.getDeviceToken());
                        hm.put("id", pushAdmin.getUserId());
                        hm.put("userTypeId", 5);
                        list.add(hm);
                    }
                });
            }
        } catch (Exception e) {
            return new ApiResponse(HttpStatus.BAD_REQUEST, "Exception", e.getMessage());
        }
        return new ApiResponse(HttpStatus.OK, "Record retrieved successfully", list);
    }
}
