/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stomp.groupChat;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 *
 * @author Welcome
 */
@Entity
@Table(name = "groupChat")
@NamedQuery(name = "GroupChat.fetchAllByOrgId", query = "SELECT DISTINCT grpChat \n"
        + "FROM GroupChat AS grpChat \n"
        + "INNER JOIN ParentChild AS parChild ON parChild.userId=grpChat.userId \n"
        + "WHERE grpChat.orgId=:orgId ORDER BY grpChat.id DESC")
public class GroupChat implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ChatId", nullable = false, updatable = false)
    private long chatId;

    @Column(name = "UserId", nullable = false)
    private long userId;

    @Transient
    private List friendId;

    @Column(name = "OrgId", nullable = true)
    private Integer orgId;

    @Transient
    private List adminId;

    @Transient
    private String orgName;

    @Transient
    private String Grade;

    public long getChatId() {
        return chatId;
    }

    public void setChatId(long chatId) {
        this.chatId = chatId;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public List getFriendId() {
        return friendId;
    }

    public void setFriendId(List friendId) {
        this.friendId = friendId;
    }

    public Integer getOrgId() {
        return orgId;
    }

    public void setOrgId(Integer orgId) {
        this.orgId = orgId;
    }

    public List getAdminId() {
        return adminId;
    }

    public void setAdminId(List adminId) {
        this.adminId = adminId;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public String getGrade() {
        return Grade;
    }

    public void setGrade(String Grade) {
        this.Grade = Grade;
    }

}
