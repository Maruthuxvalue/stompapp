/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stomp.groupChat;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author Welcome
 */
public interface GroupChatRepository extends JpaRepository<GroupChat, Long> {

//    List<GroupChat> findAllByParentId(long id);
    List<GroupChat> findAllByOrgId(int id);

    List<GroupChat> findAllByUserId(long id);

    GroupChat findByChatId(long chatId);

    GroupChat findTop1ByUserId(long id);

    GroupChat findByUserIdAndOrgId(long chatId, int schId);

    List<GroupChat> fetchAllByOrgId(@Param("orgId") int id);

//    GroupChat findByUserIdAndParentId(long chatId, long parentId);
}
