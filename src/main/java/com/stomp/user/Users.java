/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stomp.user;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.Transient;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

/**
 *
 * @author Welcome
 */
@Entity
@Table(name = "user")
@NamedQueries({
    @NamedQuery(name = "Users.fetchByManagerIdForNotification", query = "SELECT noti \n"
            + "FROM Users AS user \n"
            + "INNER JOIN PushNotifications AS noti ON noti.userId=user.id \n"
            + "WHERE user.orgId=:orgId AND user.grade=:grade AND noti.userId <>:userId ORDER BY noti.id DESC"),
    @NamedQuery(name = "Users.fetchByOrgIdForNotification", query = "SELECT noti \n"
            + "FROM Users AS user \n"
            + "INNER JOIN PushNotifications AS noti ON noti.userId=user.id \n"
            + "WHERE user.orgId=:orgId AND noti.userId <>:userId ORDER BY noti.id DESC"),
    @NamedQuery(name = "Users.fetchByUserTypeAndOrgIdAndGrade", query = "SELECT DISTINCT parent \n"
            + "FROM Users AS user \n"
            + "INNER JOIN ParentChild AS parChild ON parChild.userId=user.id \n"
            + "INNER JOIN Users AS parent ON parent.id=parChild.friendId \n"
            + "WHERE user.orgId=:orgId AND user.userTypeId=:userTypeId AND user.grade=:grade ORDER BY parent.id DESC"),
    @NamedQuery(name = "Users.fetchAllParentsByOrgId", query = "SELECT DISTINCT parent \n"
            + "FROM Users AS user \n"
            + "INNER JOIN ParentChild AS parChild ON parChild.userId=user.id \n"
            + "INNER JOIN Users AS parent ON parent.id=parChild.friendId \n"
            + "WHERE user.orgId=:orgId ORDER BY parent.id DESC")
})
public class Users implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Id", nullable = false, updatable = false)
    private long id;

    @Column(name = "Email", nullable = false)
    private String email;

    @Column(name = "Password", nullable = true)
    private String password;

    @Column(name = "OrgId", nullable = true)
    private Integer orgId;

    @Column(name = "Age", nullable = true)
    private Integer age;

    @Column(name = "UserTypeId", nullable = false)
    private int userTypeId;

    @Column(name = "OrgTypeId", nullable = false)
    private Integer orgTypeId;

    @Column(name = "Name", nullable = false)
    private String name;

    @Transient
    private Long friendId;

    @Column(name = "CcUser", nullable = true)
    private String ccUser;

    @Column(name = "UserPhone", nullable = true)
    private String userPhone;

    @Column(name = "CcParent1", nullable = true)
    private String ccParent1;

    @Column(name = "ParentPhone1", nullable = true)
    private String parentPhone1;

    @Column(name = "CcParent2", nullable = true)
    private String ccParent2;

    @Column(name = "ParentPhone2", nullable = true)
    private String parentPhone2;

    @Column(name = "CcDriver", nullable = true)
    private String ccDriver;

    @Column(name = "DriverPhone", nullable = true)
    private String driverPhone;

    @Column(name = "CcOrg", nullable = true)
    private String ccOrg;

    @Column(name = "OrgPhone", nullable = true)
    private String orgPhone;

    @Column(name = "Gender", nullable = true)
    private String gender;

    @Column(name = "Grade", nullable = true)
    private String grade;

    @Column(name = "Address", nullable = true)
    private String address;

    @Column(name = "ZipCode", nullable = true)
    private String zipCode;

    @Column(name = "City", nullable = true)
    private String city;

    @Column(name = "State", nullable = true)
    private String state;

    @Column(name = "BusRoute", nullable = true)
    private String busRoute;

    @Column(name = "ProfileImage", nullable = true)
    private String profileImage;

    @Column(name = "IsSocialUser", nullable = true)
    private Integer isSocialUser;

    @Column(name = "SocialMediaId", nullable = true)
    private String socialMediaId;

    @Column(name = "FbCyber", nullable = true)
    private String fbCyber;

    @Column(name = "InstaCyber", nullable = true)
    private String instaCyber;

    @Column(name = "SnapShotCyber", nullable = true)
    private String snapShotCyber;

    @Column(name = "OrgName", nullable = true)
    private String orgName;
    
    @Column(name = "friendSpouseName", nullable = true)
    private String friendSpouseName;

    @Column(name = "OrgStateId", nullable = true)
    private Integer orgStateId;

    @Column(name = "VerifyOTP", nullable = false)
    private int verifyOTP;

    @Column(name = "IsSignUp", nullable = false)
    private boolean isSignUp;

    @Column(name = "UserLang", nullable = true)
    private String userLang;

    @Column(name = "IsAdsOn", nullable = true)
    private boolean isAdsOn;

    @Column(name = "CreatedDateTime", updatable = false)
    @CreationTimestamp
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date createdDateTime;

    @Column(name = "ModifiedDateTime")
    @UpdateTimestamp
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date modifiedDateTime;

    @Transient
    private String verifyCode;

    @Transient
    private String deviceToken;

    @Transient
    private Integer screenshotCount;

    //@Transient
    //private String friendSpouseName;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getOrgId() {
        return orgId;
    }

    public void setOrgId(Integer orgId) {
        this.orgId = orgId;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public int getUserTypeId() {
        return userTypeId;
    }

    public void setUserTypeId(int userTypeId) {
        this.userTypeId = userTypeId;
    }

    public Integer getOrgTypeId() {
        return orgTypeId;
    }

    public void setOrgTypeId(Integer orgTypeId) {
        this.orgTypeId = orgTypeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getFriendId() {
        return friendId;
    }

    public void setFriendId(Long friendId) {
        this.friendId = friendId;
    }

    public String getCcUser() {
        return ccUser;
    }

    public void setCcUser(String ccUser) {
        this.ccUser = ccUser;
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    public String getCcParent1() {
        return ccParent1;
    }

    public void setCcParent1(String ccParent1) {
        this.ccParent1 = ccParent1;
    }

    public String getParentPhone1() {
        return parentPhone1;
    }

    public void setParentPhone1(String parentPhone1) {
        this.parentPhone1 = parentPhone1;
    }

    public String getCcParent2() {
        return ccParent2;
    }

    public void setCcParent2(String ccParent2) {
        this.ccParent2 = ccParent2;
    }

    public String getParentPhone2() {
        return parentPhone2;
    }

    public void setParentPhone2(String parentPhone2) {
        this.parentPhone2 = parentPhone2;
    }

    public String getCcDriver() {
        return ccDriver;
    }

    public void setCcDriver(String ccDriver) {
        this.ccDriver = ccDriver;
    }

    public String getDriverPhone() {
        return driverPhone;
    }

    public void setDriverPhone(String driverPhone) {
        this.driverPhone = driverPhone;
    }

    public String getCcOrg() {
        return ccOrg;
    }

    public void setCcOrg(String ccOrg) {
        this.ccOrg = ccOrg;
    }

    public String getOrgPhone() {
        return orgPhone;
    }

    public void setOrgPhone(String orgPhone) {
        this.orgPhone = orgPhone;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getBusRoute() {
        return busRoute;
    }

    public void setBusRoute(String busRoute) {
        this.busRoute = busRoute;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    public Integer getIsSocialUser() {
        return isSocialUser;
    }

    public void setIsSocialUser(Integer isSocialUser) {
        this.isSocialUser = isSocialUser;
    }

    public String getSocialMediaId() {
        return socialMediaId;
    }

    public void setSocialMediaId(String socialMediaId) {
        this.socialMediaId = socialMediaId;
    }

    public String getFbCyber() {
        return fbCyber;
    }

    public void setFbCyber(String fbCyber) {
        this.fbCyber = fbCyber;
    }

    public String getInstaCyber() {
        return instaCyber;
    }

    public void setInstaCyber(String instaCyber) {
        this.instaCyber = instaCyber;
    }

    public String getSnapShotCyber() {
        return snapShotCyber;
    }

    public void setSnapShotCyber(String snapShotCyber) {
        this.snapShotCyber = snapShotCyber;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public Integer getOrgStateId() {
        return orgStateId;
    }

    public void setOrgStateId(Integer orgStateId) {
        this.orgStateId = orgStateId;
    }

    public int getVerifyOTP() {
        return verifyOTP;
    }

    public void setVerifyOTP(int verifyOTP) {
        this.verifyOTP = verifyOTP;
    }

    public boolean isIsSignUp() {
        return isSignUp;
    }

    public void setIsSignUp(boolean isSignUp) {
        this.isSignUp = isSignUp;
    }

    public String getUserLang() {
        return userLang;
    }

    public void setUserLang(String userLang) {
        this.userLang = userLang;
    }

    public boolean isIsAdsOn() {
        return isAdsOn;
    }

    public void setIsAdsOn(boolean isAdsOn) {
        this.isAdsOn = isAdsOn;
    }

    public Date getCreatedDateTime() {
        return createdDateTime;
    }

    public void setCreatedDateTime(Date createdDateTime) {
        this.createdDateTime = createdDateTime;
    }

    public Date getModifiedDateTime() {
        return modifiedDateTime;
    }

    public void setModifiedDateTime(Date modifiedDateTime) {
        this.modifiedDateTime = modifiedDateTime;
    }

    public String getVerifyCode() {
        return verifyCode;
    }

    public void setVerifyCode(String verifyCode) {
        this.verifyCode = verifyCode;
    }

    public String getDeviceToken() {
        return deviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }

    public Integer getScreenshotCount() {
        return screenshotCount;
    }

    public void setScreenshotCount(Integer screenshotCount) {
        this.screenshotCount = screenshotCount;
    }

    public String getFriendSpouseName() {
        return friendSpouseName;
    }

    public void setFriendSpouseName(String friendSpouseName) {
        this.friendSpouseName = friendSpouseName;
    }

}
