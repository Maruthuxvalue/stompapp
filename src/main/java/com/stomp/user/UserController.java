/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stomp.user;

import com.stomp.approveSchools.ApproveSchools;
import com.stomp.approveSchools.ApproveSchoolsRepository;
import com.stomp.busRoutes.BusRoutesRepository;
import com.stomp.controller.JwtAuthenticationController;
import com.stomp.groupChat.GroupChat;
import com.stomp.groupChat.GroupChatController;
import com.stomp.groupChat.GroupChatRepository;
import com.stomp.mail.AmazonSESMail;
import com.stomp.model.JwtRequest;
import com.stomp.notifications.NotificationsRepository;
import com.stomp.notifications.PushNotifications;
import com.stomp.pushConfiguration.PushConfiguration;
import com.stomp.pushConfiguration.PushConfigurationRepository;
import com.stomp.org.Organization;
import com.stomp.org.OrgRepository;
import com.stomp.twilio.SMSController;
import com.response.ApiResponse;
import com.response.ApiResponse1;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.http.util.TextUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpServerErrorException;

/**
 *
 * @author Welcome
 */
//@RestController
//@RequestMapping(value = "/bb/user")
@Service
public class UserController {

    @Autowired
    UserRepository userRepository;

    @Autowired
    BusRoutesRepository busRepository;

    @Autowired
    ApproveSchoolsRepository approveSchoolsRepository;

    @Autowired
    OrgRepository orgRepository;

    @Autowired
    GroupChatRepository chatRepository;

    @Autowired
    AmazonSESMail mailService;

    @Autowired
    SMSController smsService;

    @Autowired
    PushConfigurationRepository pushConfigRepository;

    @Autowired
    ParentChildRepository parChildRepository;

    @Autowired
    NotificationsRepository notifyRepository;

    @Autowired
    GroupChatController chatService;

    @Autowired
    ScreenshotCountRepository screenshotRepo;

    @Autowired
    JwtAuthenticationController authenticateCont;

    @Autowired
    UserContactRepository userContactRepository;

    @Autowired
    EnableAdsRepository adsRepository;

    @GetMapping("/list")
    public ApiResponse getUsers() {
        List<Users> list = new ArrayList();
        try {
            list = userRepository.findAll();

        } catch (Exception e) {
            return new ApiResponse(HttpStatus.BAD_REQUEST, "Exception", e.getMessage());
        }
        return new ApiResponse(HttpStatus.OK, "Record retrieved successfully", list);
    }

    @Transactional
//    @PostMapping("/login")
    public ApiResponse loginUser(Users obj) {
        String langAlreadyLoggedIn = "You have already logged in as admin in this device. Please use another device to login.";
        String langAlreadyMsg = "Login not allowed in this device.";
        String langSuccess = "Success";
        String langUserNotFound = "User Not Found";
        String langUserInvalid = "Either Username or Password is incorrect";
        String accesstoken = "";
        PushConfiguration checkExists = pushConfigRepository.findByDeviceToken(obj.getDeviceToken());
        Users getData = new Users();
        getData.setUserTypeId(0);
        if (checkExists != null) {
            Optional<Users> getTypeId = userRepository.findById(checkExists.getUserId());
            if (getTypeId.isPresent()) {
                getData = getTypeId.get();
                if (getData.getUserLang().contains("Spanish")) {
                    langAlreadyLoggedIn = "Ya ha iniciado sesión como administrador en este dispositivo. Por favor utilice otro dispositivo para iniciar sesión.";
                    langAlreadyMsg = " No se permite iniciar sesión en este dispositivo.";
                }
            }
        }
        if (checkExists != null && obj.getUserTypeId() != 5 && getData.getUserTypeId() == 5) {
            return new ApiResponse(HttpStatus.FOUND, langAlreadyLoggedIn, langAlreadyMsg);
        } else {
            try {
                String message = "";
                String token = "";
                HttpStatus status = HttpStatus.NOT_FOUND;
                Users name = new Users();

                if (obj.getIsSocialUser() == 1) {
                    name = userRepository.findByUserTypeIdAndSocialMediaId(obj.getUserTypeId(), obj.getSocialMediaId());
                    if (name == null && (obj.getUserTypeId() == 3 || obj.getUserTypeId() == 1)) {
                        Users newLogin = new Users();
                        name = userRepository.findBySocialMediaId(obj.getSocialMediaId());
                        if (name != null) {
                            newLogin.setUserTypeId(obj.getUserTypeId());
                            newLogin.setAge(name.getAge());
                            newLogin.setEmail(name.getEmail());
                            newLogin.setPassword(name.getPassword());
                            newLogin.setName(name.getName());
                            newLogin.setUserPhone(name.getUserPhone());
                            newLogin.setCcUser(name.getCcUser());
                            newLogin.setGender(name.getGender());
                            newLogin.setProfileImage(name.getProfileImage());
                            newLogin.setIsSocialUser(name.getIsSocialUser());
                            newLogin.setSocialMediaId(name.getSocialMediaId());
                            newLogin.setFbCyber(name.getFbCyber());
                            newLogin.setInstaCyber(name.getInstaCyber());
                            newLogin.setSnapShotCyber(name.getSnapShotCyber());
                            newLogin.setOrgName(name.getOrgName());
                            newLogin.setVerifyOTP(name.getVerifyOTP());
                            newLogin.setIsSignUp(name.isIsSignUp());
                            newLogin.setUserLang(name.getUserLang());
                            newLogin.setIsAdsOn(name.isIsAdsOn());
                            name = userRepository.save(newLogin);
                            name.setOrgId(0);
                            name.setOrgTypeId(0);
                        }
                    }
                    if (name != null) {
                        if (name.getUserLang().contains("Spanish")) {
                            langSuccess = "Éxito";
                        }
                        if (name.getUserTypeId() != 3 && name.getOrgTypeId() == 5) {
                            Organization sch = orgRepository.findById(name.getOrgId()).get();
                            name.setOrgName(sch.getOrgName());
                            name.setOrgStateId(sch.getStateId());
                        } else {
                            ParentChild parent = parChildRepository.findTop1ByFriendId(name.getId());
                            if (parent != null) {
                                Users child = userRepository.findById(parent.getUserId()).get();
                                if (child != null) {
                                    Organization sch = orgRepository.findById(child.getOrgId()).get();
                                    name.setOrgName(sch.getOrgName());
                                    name.setOrgStateId(sch.getStateId());
                                }
                            }
                        }
                        if (obj.getDeviceToken() != null) {
                            if (checkExists != null) {
                                checkExists.setDeviceToken("");
                                pushConfigRepository.save(checkExists);
                            }
                            PushConfiguration pushConfig1 = pushConfigRepository.findByUserId(name.getId());
                            if (pushConfig1 != null) {
                                pushConfig1.setDeviceToken(obj.getDeviceToken());
                                pushConfigRepository.save(pushConfig1);
                            } else {
                                PushConfiguration pushConfig2 = new PushConfiguration();
                                pushConfig2.setUserId(name.getId());
                                pushConfig2.setDeviceToken(obj.getDeviceToken());
                                pushConfig2.setPlatform("0");
                                pushConfigRepository.save(pushConfig2);
                            }
                        }
                        List<ScreenshotCount> count = screenshotRepo.findAllByUserId(obj.getId());
                        name.setScreenshotCount(count.size());
                        message = langSuccess;
                        status = HttpStatus.OK;
                        JwtRequest getToken = new JwtRequest();
                        getToken.setUserName(obj.getEmail());
                        getToken.setPassword(obj.getPassword());
                        accesstoken = authenticateCont.generateToken(getToken);
                    } else {
//                        if (name.getUserLang().contains("Spanish")) {
//                            langUserNotFound = "Usuario no encontrado";
//                        }
                        message = langUserNotFound;
                        status = HttpStatus.NOT_FOUND;
                        name = null;
                    }

                } else {
                    name = userRepository.findByUserTypeIdAndEmail(obj.getUserTypeId(), obj.getEmail());
                    /*
                    if (name == null && (obj.getUserTypeId() == 3 || obj.getUserTypeId() == 1)) {
                        Users newLogin = new Users();
                        name = userRepository.findByEmail(obj.getEmail());
                        if (name != null) {
                            newLogin.setUserTypeId(obj.getUserTypeId());
                            newLogin.setAge(name.getAge());
                            newLogin.setEmail(name.getEmail());
                            newLogin.setPassword(name.getPassword());
                            newLogin.setName(name.getName());
                            newLogin.setUserPhone(name.getUserPhone());
                            newLogin.setCcUser(name.getCcUser());
                            newLogin.setGender(name.getGender());
                            newLogin.setProfileImage(name.getProfileImage());
                            newLogin.setIsSocialUser(name.getIsSocialUser());
                            newLogin.setSocialMediaId(name.getSocialMediaId());
                            newLogin.setFbCyber(name.getFbCyber());
                            newLogin.setInstaCyber(name.getInstaCyber());
                            newLogin.setSnapShotCyber(name.getSnapShotCyber());
                            newLogin.setOrgName(name.getOrgName());
                            newLogin.setVerifyOTP(name.getVerifyOTP());
                            newLogin.setIsSignUp(name.isIsSignUp());
                            newLogin.setUserLang(name.getUserLang());
                            newLogin.setIsAdsOn(name.isIsAdsOn());
                            name = userRepository.save(newLogin);
                            name.setOrgId(0);
                            name.setOrgTypeId(0);
                        }
                    }*/
                    if (name != null) {
                        if (name.getPassword().equals(getMD5(obj.getPassword()))) {
                            if (name.getUserLang().contains("Spanish")) {
                                langSuccess = "Éxito";
                            }
                            if (name.getUserTypeId() != 3 && name.getOrgTypeId() == 5) {
                                Organization sch = orgRepository.findById(name.getOrgId()).get();
                                name.setOrgName(sch.getOrgName());
                                name.setOrgStateId(sch.getStateId());
                            } else {
                                ParentChild parent = parChildRepository.findTop1ByFriendId(name.getId());
                                if (parent != null) {
                                    Users child = userRepository.findById(parent.getUserId()).get();
                                    if (child != null) {
                                        Organization sch = orgRepository.findById(child.getOrgId()).get();
                                        name.setOrgName(sch.getOrgName());
                                        name.setOrgStateId(sch.getStateId());
                                    }
                                }
                            }
                            if (obj.getDeviceToken() != null) {
                                if (checkExists != null) {
                                    checkExists.setDeviceToken("");
                                    pushConfigRepository.save(checkExists);
                                }
                                PushConfiguration pushConfig1 = pushConfigRepository.findByUserId(name.getId());
                                if (pushConfig1 != null) {
                                    pushConfig1.setDeviceToken(obj.getDeviceToken());
                                    pushConfigRepository.save(pushConfig1);
                                } else {
                                    PushConfiguration pushConfig2 = new PushConfiguration();
                                    pushConfig2.setUserId(name.getId());
                                    pushConfig2.setDeviceToken(obj.getDeviceToken());
                                    pushConfig2.setPlatform("0");
                                    pushConfigRepository.save(pushConfig2);
                                }
                            }
                            List<ScreenshotCount> count = screenshotRepo.findAllByUserId(obj.getId());
                            name.setScreenshotCount(count.size());
                            message = langSuccess;
                            status = HttpStatus.OK;
                            JwtRequest getToken = new JwtRequest();
                            getToken.setUserName(obj.getEmail());
                            getToken.setPassword(obj.getPassword());
                            accesstoken = authenticateCont.generateToken(getToken);
                        } else {
//                            if (name.getUserLang().contains("Spanish")) {
//                                langUserInvalid = "El nombre del usuario o la contraseña es incorrecta";
//                            }
                            message = langUserInvalid;
                            status = HttpStatus.UNAUTHORIZED;
                            name = null;
                        }

                    } else {
//                        if (name.getUserLang().contains("Spanish")) {
//                            langUserNotFound = "Usuario no encontrado";
//                        }
                        message = langUserNotFound;
                        status = HttpStatus.NOT_FOUND;
                        name = null;
                    }
                }
                return new ApiResponse(status, message, name, accesstoken);
            } catch (HttpServerErrorException.InternalServerError e) {
                return new ApiResponse(HttpStatus.BAD_REQUEST, "Exception", e);
            } catch (InterruptedException e) {
                return new ApiResponse(HttpStatus.BAD_REQUEST, "Exception", e);
            } catch (Exception e) {
                return new ApiResponse(HttpStatus.BAD_REQUEST, "Exception", e);
            }
        }
    }

    @Transactional
//    @PostMapping("/sign_up")
    public ApiResponse saveUser(Users obj) {
        String langSave = "Record Saved Successfully";
        Integer inviteStatus = 0;
        HttpStatus status = HttpStatus.OK;
        if (obj.getUserLang().contains("Spanish")) {
            langSave = "Registro guardado con éxito";
        }
        String message = langSave;
        Users saveUser = new Users();
        obj.setUserPhone(getOnlyDigits(obj.getUserPhone()));
        obj.setParentPhone1(getOnlyDigits(obj.getParentPhone1()));
        obj.setParentPhone2(getOnlyDigits(obj.getParentPhone2()));
        obj.setOrgPhone(getOnlyDigits(obj.getOrgPhone()));
        obj.setDriverPhone(getOnlyDigits(obj.getDriverPhone()));
        obj.setFriendSpouseName(obj.getFriendSpouseName()); 
        obj.setIsSignUp(true);
        try {
            if (obj.getIsSocialUser() == 1) {
                saveUser = userRepository.save(obj);
            } else {
                obj.setPassword(getMD5(obj.getPassword()));
                saveUser = userRepository.save(obj);
            }
            if (obj.getCcParent1() != null && obj.getParentPhone1() != null) {
                Users userContact = userRepository.findTop1ByCcUserAndUserPhoneAndUserTypeId(obj.getCcParent1(), obj.getParentPhone1(), 3);
                if (userContact == null) {
                    UserContacts contact = new UserContacts();
                    contact.setUserId(saveUser.getId());
                    contact.setContactName(obj.getFriendSpouseName());
                    contact.setCcPhone(obj.getCcParent1());
                    contact.setContactPhone(obj.getParentPhone1());
                    contact.setInviteStatus(inviteStatus);
                    userContactRepository.save(contact);
                } else {
                    ParentChild parent = new ParentChild();
                    parent.setUserId(saveUser.getId());
                    parent.setFriendId(userContact.getId());
                    parChildRepository.save(parent);
                }
            }
            if (obj.getOrgId() != null && obj.getOrgId() == 0) {
                ApproveSchools approve = new ApproveSchools();
                approve.setUserId(saveUser.getId());
                approve.setOrgName(obj.getOrgName());
                approve.setIsApproved(0);
                approve.setStateId(obj.getOrgStateId());
                approve.setOrgTypeId(obj.getOrgTypeId());
                approveSchoolsRepository.save(approve);
            }

            if (saveUser.getOrgId() != null && saveUser.getOrgId() > 0) {
                Organization sch = orgRepository.findById(saveUser.getOrgId()).get();
                saveUser.setOrgName(sch.getOrgName());
                saveUser.setOrgStateId(sch.getStateId());
            }
            String accesstoken = "";
            if (saveUser.getId() > 0) {
                JwtRequest getToken = new JwtRequest();
                getToken.setUserName(obj.getEmail());
                getToken.setPassword(obj.getPassword());
                accesstoken = authenticateCont.generateToken(getToken);
            }
            return new ApiResponse(status, message, saveUser, accesstoken);
        } catch (Exception e) {
            return new ApiResponse(HttpStatus.BAD_REQUEST, "Exception", e.getMessage());

        }
    }

    public static String getMD5(String pwd) throws NoSuchAlgorithmException {
        MessageDigest messageDigest = MessageDigest.getInstance("MD5");
        messageDigest.update(pwd.getBytes());
        byte[] digest = messageDigest.digest();
        StringBuffer sb = new StringBuffer();
        for (byte b : digest) {
            sb.append(Integer.toHexString((int) (b & 0xff)));
        }
        return sb.toString();
//        return pwd;
    }

    @Transactional
//    @PostMapping("/parent_sign_up")
    public ApiResponse saveParent(Users obj) {
        String langSave = "Record Saved Successfully";
        String langSuccess = "Success";
        String langStudent = "User";
        String langTeacher = "Professor / Manager";
        String langParent = "Friend / Spouse";
        String langDriver = "Driver";
        String langUserNameReg = "Username is already registered as ";
        if (obj.getUserLang().contains("Spanish")) {
            langSave = "Registro guardado con éxito";
            langStudent = "Usuario";
            langParent = "Catedrático / Gerente";
            langTeacher = "Amigo / Cónyuge";
            langDriver = "Chofer";
            langUserNameReg = "El nombre del usuario ya está registrado como ";
        }
        String message = langSave;
        Users parentSave = new Users();
        HttpStatus status = HttpStatus.OK;
        obj.setUserPhone(getOnlyDigits(obj.getUserPhone()));
        obj.setParentPhone1(getOnlyDigits(obj.getParentPhone1()));
        obj.setParentPhone2(getOnlyDigits(obj.getParentPhone2()));
        obj.setOrgPhone(getOnlyDigits(obj.getOrgPhone()));
        obj.setDriverPhone(getOnlyDigits(obj.getDriverPhone()));
        obj.setIsSignUp(true);
//        Users childDetails = userRepository.findById(obj.getFriendId()).get();
//        ParentChild parChild = new ParentChild();
        try {
            if (obj.getIsSocialUser() == 1) {
                Users verify = userRepository.findByIsSocialUserAndSocialMediaId(1, obj.getSocialMediaId());
                if (verify != null) {
                    String userType = "";
                    switch (verify.getUserTypeId()) {
                        case 1:
                            userType = langStudent;
                            break;
                        case 2:
                            userType = langTeacher;
                            break;
                        case 3:
                            userType = langParent;
                            break;
                        case 4:
                            userType = langDriver;
                            break;
                    }
                    message = langUserNameReg + userType + ".";
                    status = HttpStatus.UNAUTHORIZED;
                    parentSave = null;
                } else {
                    obj.setFriendId(null);
                    obj.setPassword(getMD5(obj.getPassword()));
                    parentSave = userRepository.save(obj);
                    List<UserContacts> userContact = userContactRepository.findAllByContactPhone(obj.getUserPhone());
                    if (!userContact.isEmpty()) {
                        List<ParentChild> list = new ArrayList();
                        List<GroupChat> chatList = new ArrayList();
                        for (UserContacts item : userContact) {
                            ParentChild parent = new ParentChild();
                            parent.setUserId(item.getUserId());
                            parent.setFriendId(parentSave.getId());
                            list.add(parent);
                            Users chatUser = userRepository.findById(item.getUserId()).get();
                            if (chatUser.getOrgId() != null && chatUser.getOrgId() != 0) {
                                GroupChat chats = new GroupChat();
                                chats.setUserId(item.getUserId());
                                chats.setOrgId(chatUser.getOrgId());
                                chatList.add(chats);
                            }
                        }
                        if (!chatList.isEmpty()) {
                            chatRepository.saveAll(chatList);
                        }
                        parChildRepository.saveAll(list);
                        userContactRepository.deleteAll(userContact);
                    }
                }
            } else {
                obj.setPassword(getMD5(obj.getPassword()));
                obj.setFriendId(null);
                parentSave = userRepository.save(obj);
                List<UserContacts> userContact = userContactRepository.findAllByContactPhone(obj.getUserPhone());
                if (!userContact.isEmpty()) {
                    List<ParentChild> list = new ArrayList();
                    List<GroupChat> chatList = new ArrayList();
                    for (UserContacts item : userContact) {
                        ParentChild parent = new ParentChild();
                        parent.setUserId(item.getUserId());
                        parent.setFriendId(parentSave.getId());
                        list.add(parent);
                        Users chatUser = userRepository.findById(item.getUserId()).get();
                        if (chatUser.getOrgId() != null && chatUser.getOrgId() != 0) {
                            GroupChat chats = new GroupChat();
                            chats.setUserId(item.getUserId());
                            chats.setOrgId(chatUser.getOrgId());
                            chatList.add(chats);
                        }
                    }
                    chatRepository.saveAll(chatList);
                    parChildRepository.saveAll(list);
                    userContactRepository.deleteAll(userContact);
                }
            }
            String accesstoken = "";
            JwtRequest getToken = new JwtRequest();
            getToken.setUserName(obj.getEmail());
            getToken.setPassword(obj.getPassword());
            accesstoken = authenticateCont.generateToken(getToken);

            return new ApiResponse(status, message, parentSave, accesstoken);
        } catch (Exception e) {
            return new ApiResponse(HttpStatus.BAD_REQUEST, "Exception", e.getMessage());
        }
    }

    @Transactional
//    @PostMapping("/add_child")
    public ApiResponse saveChild(Users obj) {
        String langChild = "Friend / Spouse Added Successfully";
        String langChildNotLogged = "Friend / Spouse is not logged in any device.";
        String langChildAdded = "Friend / Spouse already added.";
        String langUserNotFound = "User Not Found";
        ParentChild parChild = new ParentChild();
        Map<String, Object> result = new HashMap();
        List individualList = new ArrayList();
        boolean isSameAdmin = false; //to check deleting child school is same for other childs
        HttpStatus status = HttpStatus.OK;
        Users parentData = userRepository.findById(obj.getFriendId()).get();
        if (parentData != null && parentData.getUserLang().contains("Spanish")) {
            langChild = "Amigo / Cónyuge se ha agregado con éxito";
            langChildAdded = "Amigo / Cónyuge ya se ha agregado.";
            langChildNotLogged = "Amigo / Cónyuge no está conectado en ningún dispositivo.";
            langUserNotFound = "Usuario no encontrado";
        }
        try {
            String message = langChild;
            Optional<Users> childsData = userRepository.findById(obj.getId());
            if (childsData.isPresent()) {
                Users childUser = childsData.get();

                PushConfiguration checkLoggedIn = pushConfigRepository.findByUserId(childUser.getId());

                if (checkLoggedIn != null && checkLoggedIn.getDeviceToken() != null && !checkLoggedIn.getDeviceToken().equals("")) {
                    ParentChild checkExists = parChildRepository.findByUserIdAndFriendId(obj.getId(), obj.getFriendId());
                    if (checkExists == null) {
//                childSave.setFriendId(obj.getFriendId());
//                userRepository.save(childSave);
                        GroupChat grpChat = new GroupChat();
                        if (childUser.getOrgId() > 0 && childUser.getOrgId() != null) {
//                    Users adminUser = userRepository.findTop1ByUserTypeIdAndOrgId(5, childUser.getOrgId());
                            GroupChat chatExist = chatRepository.findByUserIdAndOrgId(childUser.getId(), childUser.getOrgId());
                            if (chatExist == null) {
//                        grpChat.setFriendId(obj.getFriendId());
                                grpChat.setUserId(obj.getId());
                                grpChat.setOrgId(childUser.getOrgId());
                                chatRepository.save(grpChat);
                            }
                            parChild.setUserId(obj.getId());
                            parChild.setFriendId(obj.getFriendId());
                            parChildRepository.save(parChild);
                        }
                        ApiResponse childGroup = chatService.getChatIds(1, obj.getId()); //getting group chat
                        Users childLinkAdded = userRepository.findById(obj.getId()).get();
                        List<ParentChild> parChildList = parChildRepository.findAllByFriendId(obj.getFriendId());
                        if (!parChildList.isEmpty()) {
                            for (ParentChild item : parChildList) {
                                Users getChild = userRepository.findById(item.getUserId()).get();
                                if (!Objects.equals(childLinkAdded.getId(), item.getUserId()) && Objects.equals(getChild.getOrgId(), childLinkAdded.getOrgId())) {
                                    isSameAdmin = true;
                                }
                            }
                        }
                        Organization getSchool = orgRepository.findById(childLinkAdded.getOrgId()).get();
                        HashMap hm = new HashMap();
                        hm.put("Name", childLinkAdded.getName());
                        hm.put("Id", childLinkAdded.getId());
                        hm.put("User_Type_Id", 1);
                        hm.put("Grade", childLinkAdded.getGrade());
                        hm.put("School", getSchool.getOrgName());
                        individualList.add(hm);
                        if (isSameAdmin == false) {
                            List<Users> adminList = userRepository.findAllByUserTypeIdAndOrgId(5, childLinkAdded.getOrgId());
                            if (!adminList.isEmpty()) {
                                for (Users items : adminList) {
                                    HashMap hm1 = new HashMap();
                                    hm1.put("Name", items.getName());
                                    hm1.put("Id", items.getId());
                                    hm1.put("User_Type_Id", 5);
                                    hm1.put("Grade", null);
                                    hm1.put("School", getSchool.getOrgName());
                                    individualList.add(hm1);
                                }
                            }
                        }
                        result.put("individualList", individualList);
                        if (childGroup.getResult() != null) {
                            result.put("groupList", childGroup.getResult());
                        } else {
                            result.put("groupList", null);
                        }
                    } else if (checkExists.getUserId() == obj.getId()) {
                        message = langChildAdded;
                        status = HttpStatus.UNAUTHORIZED;
                        result = null;
                    }
                } else {
                    message = langChildNotLogged;
                    status = HttpStatus.UNAUTHORIZED;
                    result = null;
                }
            } else {
                message = langUserNotFound;
                status = HttpStatus.NOT_FOUND;
                result = null;
            }
            return new ApiResponse(status, message, result);

        } catch (Exception e) {
            return new ApiResponse(HttpStatus.BAD_REQUEST, "Exception", e.getMessage());
        }
    }
    
    @Transactional
    public ApiResponse sendInvite(UserContacts obj) {
        String langSuccess = "Success";
        String langMessage = "Invite send successfully";
        String noUserFound ="No user contact found";
        HttpStatus status = HttpStatus.OK;
        Integer inviteStatus = 1;
        try {
         //UserContacts userContactUpdate = userContactRepository.findById(obj.getId()).get();
         UserContacts userContact = userContactRepository.findById(obj.getId()).get();
        //Optional<UserContacts> userContactDetails = userContactRepository.findById(obj.getId());
        if( userContact == null)
        {
            status = HttpStatus.NOT_FOUND;
            langSuccess = noUserFound;
            
        } else {
            //userContactUpdate = userContactDetails.get();
            userContact.setInviteStatus(inviteStatus);
            userContactRepository.save(userContact);
        }
           return new ApiResponse(status, langSuccess,langMessage);
        } catch (Exception e) {
            return new ApiResponse(HttpStatus.BAD_REQUEST, "Error", e.getMessage());

        }
    }

//    @PostMapping("/verify_user")
    public ApiResponse verifyUser(Users obj
    ) {
        String langChildNotLogged = "Friend / Spouse is not logged in any device.";
        String langUserExists = "User Exists";
        String langUserNotExits = "User Not Exists";
        String message = "";
        HttpStatus status = HttpStatus.OK;
        Users verify = new Users();
        try {
            if (obj.getIsSocialUser() == 0) {
                verify = userRepository.findByEmail(obj.getEmail());
                if (verify.getUserLang().contains("Spanish")) {
                    langUserExists = "Amigo / Cónyuge existe";
                    langUserNotExits = "Amigo / Cónyuge no existe";
                    langChildNotLogged = "Amigo / Cónyuge no está conectado";
                }
                PushConfiguration checkLoggedIn = pushConfigRepository.findByUserId(verify.getId());
                if (checkLoggedIn != null && checkLoggedIn.getDeviceToken() != null && !checkLoggedIn.getDeviceToken().equals("")) {
                    if (verify != null && verify.getPassword().equals(getMD5(obj.getPassword()))) {
                        message = langUserExists;
                    } else {
                        message = langUserNotExits;
                        status = HttpStatus.UNAUTHORIZED;
                        verify = null;
                    }
                } else {
                    message = langChildNotLogged;
                    status = HttpStatus.UNAUTHORIZED;
                    verify = null;
                }
            } else {
                verify = userRepository.findByIsSocialUserAndSocialMediaId(1, obj.getSocialMediaId());
                if (verify.getUserLang().contains("Spanish")) {
                    langUserExists = "Amigo / Cónyuge existe";
                    langUserNotExits = "Amigo / Cónyuge no existe";
                    langChildNotLogged = "Amigo / Cónyuge niña no está conectado";
                }
                PushConfiguration checkLoggedIn = pushConfigRepository.findByUserId(verify.getId());
                if (checkLoggedIn != null && checkLoggedIn.getDeviceToken() != null && !checkLoggedIn.getDeviceToken().equals("")) {
                    if (verify != null) {
                        message = langUserExists;
                    } else {
                        message = langUserNotExits;
                        status = HttpStatus.UNAUTHORIZED;
                        verify = null;
                    }
                } else {
                    message = langChildNotLogged;
                    status = HttpStatus.UNAUTHORIZED;
                    verify = null;
                }
            }
            return new ApiResponse(status, message, verify);
        } catch (Exception e) {
            return new ApiResponse(HttpStatus.BAD_REQUEST, "Exception", e.getMessage());
        }
    }

    @Transactional
//    @PostMapping("/verify_otp")
    public ApiResponse VerifyOTP(Users obj
    ) {
        String langUserNotFound = "User Not Found";
        String langUserVerified = "User verified Successfully";
        String message = "";
        Users verify = userRepository.findById(obj.getId()).get();
        if (verify != null && verify.getUserLang().contains("Spanish")) {
            langUserNotFound = "Usuario no encontrado";
            langUserVerified = "Usuario verificado correctamente";
        }
        HttpStatus status = HttpStatus.OK;
        try {
            if (verify != null) {
                verify.setVerifyOTP(1);
                userRepository.save(verify);
                message = langUserVerified;
            } else {
                message = langUserNotFound;
                status = HttpStatus.NOT_FOUND;
            }
            return new ApiResponse(status, message, verify);
        } catch (Exception e) {
            return new ApiResponse(HttpStatus.BAD_REQUEST, "Exception", e.getMessage());
        }
    }

//    @PostMapping("/check_email_exists")
    public ApiResponse checkEmail(Users obj
    ) {
        String langStudent = "User";
        String langTeacher = "Professor / Manager";
        String langParent = "Friend / Spouse";
        String langDriver = "Driver";
        String langUserNameReg = "Username is already registered as ";
        String langUserNameNotExists = "Username not exists";
        String message = "";
        Users verifyEmail = userRepository.findTop1ByEmail(obj.getEmail());
        if (verifyEmail != null && verifyEmail.getUserLang().contains("Spanish")) {
            langStudent = "Usuario";
            langParent = "Catedrático / Gerente";
            langTeacher = "Amigo / Cónyuge";
            langDriver = "Chofer";
            langUserNameReg = "El nombre del usuario ya está registrado como ";
            langUserNameNotExists = "El nombre de usuario no existe";
        }
//        Users verifyMobile = userRepository.findTop1ByUserPhone(getOnlyDigits(obj.getUserPhone()));
        HttpStatus status = HttpStatus.UNAUTHORIZED;
        try {
            String userType = "";
            String mobileUsed = "";
            if (verifyEmail != null) {
                switch (verifyEmail.getUserTypeId()) {
                    case 1:
                        userType = langStudent;
                        break;
                    case 2:
                        userType = langTeacher;
                        break;
                    case 3:
                        userType = langParent;
                        break;
                    case 4:
                        userType = langDriver;
                        break;
                }
            }
            if (verifyEmail != null) {
                message = langUserNameReg + userType + ".";
            } else {
                message = langUserNameNotExists;
                status = HttpStatus.OK;
            }
            return new ApiResponse(status, message, null);
        } catch (Exception e) {
            return new ApiResponse(HttpStatus.BAD_REQUEST, "Exception", e.getMessage());
        }
    }

//    @PostMapping("/check_social_user")
    public ApiResponse verifySocialUser(Users obj
    ) {
        String langUserExists = "User Exists";
        String langUserNotExits = "User Not Exists";
        String message = "";
//        Users checkEmailExists = userRepository.findByEmail(obj.getEmail());
        Users checkUserExists = userRepository.findByIsSocialUserAndUserTypeIdAndSocialMediaId(obj.getIsSocialUser(), obj.getUserTypeId(), obj.getSocialMediaId());
        HttpStatus status = HttpStatus.OK;
        PushConfiguration isLoggedIn = pushConfigRepository.findByDeviceToken(obj.getDeviceToken());
        if (checkUserExists != null && checkUserExists.getUserLang().contains("Spanish")) {
            langUserExists = "El usuario existe";
            langUserNotExits = "El usuario no existe";
        }
        try {
            if (checkUserExists == null) {
                message = langUserNotExits;
                status = HttpStatus.NOT_FOUND;
                checkUserExists = null;
            } else {
                if (obj.getDeviceToken() != null) {
                    if (isLoggedIn != null) {
                        isLoggedIn.setDeviceToken("");
                        pushConfigRepository.save(isLoggedIn);
                    }
                    PushConfiguration pushConfig1 = pushConfigRepository.findByUserId(checkUserExists.getId());
                    if (pushConfig1 != null) {
                        pushConfig1.setDeviceToken(obj.getDeviceToken());
                        pushConfigRepository.save(pushConfig1);
                    } else {
                        PushConfiguration pushConfig2 = new PushConfiguration();
                        pushConfig2.setUserId(checkUserExists.getId());
                        pushConfig2.setDeviceToken(obj.getDeviceToken());
                        pushConfig2.setPlatform("0");
                        pushConfigRepository.save(pushConfig2);
                    }
                }
                message = langUserExists;
            }
            return new ApiResponse(status, message, checkUserExists);
        } catch (Exception e) {
            return new ApiResponse(HttpStatus.BAD_REQUEST, "Exception", e.getMessage());
        }
    }

    public static String getOnlyDigits(String s) {
        if (s != null) {
            Pattern pattern = Pattern.compile("[^0-9]");
            Matcher matcher = pattern.matcher(s);
            String number = matcher.replaceAll("");
            return number;
        } else {
            return null;
        }
    }

    @Transactional
//    @PostMapping("/verify_forgot_password")
    public ApiResponse getVerificationCode(Users obj) {
        String langPwdVerify = "The verification code to reset your password for STOMP account ";
        String langPwdVerifyTime = ". This verification code doesn't expire, you can use it any time.";
        String langPwdVerifyMsg = "Verification code sent to registered EmailId and Mobile Number";
        String langMailSent = "Mail not sent";
        try {
            Users userExists = userRepository.findByEmail(obj.getEmail());
            if (userExists != null && userExists.getUserLang().contains("Spanish")) {
                langPwdVerify = "El código de verificación para restablecer su contraseña para la cuenta STOMP";
                langPwdVerifyTime = ". Este código de verificación no caduca, puede usarlo en cualquier momento.";
                langPwdVerifyMsg = "Código de verificación enviado a la identificación de correo electrónico registrada y al número de teléfono móvil";
                langMailSent = "Correo no enviado";
            }
            HttpStatus status = HttpStatus.OK;
            String message = "";
            String result = "";
            ApiResponse n = new ApiResponse();
            if (userExists != null) {
                List<String> noList = new ArrayList();
                noList.add(userExists.getCcUser() + getOnlyDigits(userExists.getUserPhone()));
                smsService.sendMessages(noList, langPwdVerify + obj.getVerifyCode() + langPwdVerifyTime);
            }
            n = mailService.sendVerifyCode(obj.getVerifyCode(), obj.getEmail(), userExists.getUserLang());
            if (n.getStatus() == 200) {
                message = langPwdVerifyMsg;
                status = HttpStatus.OK;
                result = "Success";
            } else {
                message = langMailSent;
                status = HttpStatus.BAD_REQUEST;
                result = "Error";
            }

            return new ApiResponse(status, message, result);
        } catch (Exception e) {
            return new ApiResponse(HttpStatus.BAD_REQUEST, "Exception", e.getMessage());
        }
    }

    @Transactional
//    @PostMapping("/forgot_password")
    public ApiResponse forgotPasword(Users obj) {
        String langUserNameNotExists = "Username not exists";
        String langPwdUpdate = "Password Updated Successfully";
        String samePassword = "Please Enter different Password";
        String message = "";
        Users verify = userRepository.findByEmail(obj.getEmail());
        if (verify != null && verify.getUserLang().contains("Spanish")) {
            langPwdUpdate = "Contraseña actualizada exitosamente";
            langUserNameNotExists = "El nombre de usuario no existe";
        }
        HttpStatus status = HttpStatus.OK;
        try {
            if (verify != null) {
                if (verify.getPassword().equals(getMD5(obj.getPassword()))) 
                {
                    message = samePassword;
                    status = HttpStatus.BAD_REQUEST;
                }
                else
                {
                    verify.setPassword(getMD5(obj.getPassword()));
                    userRepository.save(verify);
                    message = langPwdUpdate;
                    
                }
            } else {
                message = langUserNameNotExists;
                status = HttpStatus.NOT_FOUND;
            }
            return new ApiResponse(status, message, "Success");
        } catch (Exception e) {
            return new ApiResponse(HttpStatus.BAD_REQUEST, "Exception", e.getMessage());
        }
    }

    @Transactional
//    @PostMapping("/forgot_username")
    public ApiResponse forgotUserName(Users obj) {
        String langSMSMsg = "Hi STOMP user, you recently requested your username for STOMP  account. Your username for the registered mobile number provided by you is/are ";
        String langSMSSent = "Username sent to your mobile number";
        String langMobNotFound = "Mobile No not Found";
        Users verify = userRepository.findTop1ByUserPhone(obj.getUserPhone());
        if (verify == null) {
            verify = userRepository.findTop1ByUserPhone(getOnlyDigits(obj.getUserPhone()));
        }
        if (verify != null && verify.getUserLang().contains("Spanish")) {
            langMobNotFound = "Número de móvil no encontrado";
            langSMSSent = "Nombre de usuario enviado a su número de telefono móvil";
            langSMSMsg = "Hola, usuario de STOMP, recientemente solicitó su nombre de usuario para la cuenta de STOMP. Su nombre de usuario para el número de teléfono móvil registrado proporcionado por usted es / son";
        }
        String result = "NULL";
        try {
            String message = "";
            HttpStatus status = HttpStatus.UNAUTHORIZED;
            List<String> list = new ArrayList();
            if (verify != null) {
                List<Users> allList = userRepository.findAllByUserPhone(verify.getUserPhone());
                for (Users item : allList) {
                    list.add(item.getEmail() + " - " + getUserType(item.getUserTypeId(), item.getUserLang()));
                }
                String allEmails = list.toString();
                if (obj.getUserPhone() != null && obj.getUserPhone().length() > 0) {
                    List<String> noList = new ArrayList();
                    noList.add(verify.getCcUser() + getOnlyDigits(obj.getUserPhone()));
                    smsService.sendMessages(noList, langSMSMsg + allEmails + ".");
                    message = langSMSSent;
                    status = HttpStatus.OK;
                    result = "Success";
                }
            } else {
                message = langMobNotFound;
                status = HttpStatus.NOT_FOUND;
            }
            return new ApiResponse(status, message, result);
        } catch (Exception e) {
            return new ApiResponse(HttpStatus.BAD_REQUEST, "Exception", e.getMessage());
        }
    }

//    @PostMapping("/change_password")
    public ApiResponse changePwd(Users userDetails) {
        String langPwdUpdate = "Password Updated Successfully";
        String langNotFound = "Record not Found";
        String message = "";
        Users updatePwd = userRepository.findById(userDetails.getId()).get();
        if (updatePwd != null && updatePwd.getUserLang().contains("Spanish")) {
            langPwdUpdate = "Contraseña actualizada exitosamente";
            langNotFound = "Registro no encontrado";
        }
        HttpStatus status = HttpStatus.OK;
        try {
            if (updatePwd != null) {
                updatePwd.setPassword(getMD5(userDetails.getPassword()));
                userRepository.save(updatePwd);
                message = langPwdUpdate;
            } else {
                message = langNotFound;
                status = HttpStatus.UNAUTHORIZED;
                updatePwd = null;
            }
            return new ApiResponse(HttpStatus.OK, message, "Success");
        } catch (Exception e) {
            return new ApiResponse(HttpStatus.BAD_REQUEST, "Exception", e.getMessage());
        }
    }

//    @PostMapping("/get_child_list")
    public ApiResponse getChilds(Users userDetails) {
        String langRetrieved = "Record retrieved successfully";
        String langNotFound = "Record not Found";
        String message = "";
        HttpStatus status = HttpStatus.OK;
        Users oth = new Users();
        oth.setId(0);
        oth.setName("Others");
        oth.setEmail("Others");
        List<ParentChild> parentList = parChildRepository.findAllByFriendId(userDetails.getId());
        List<Users> childList = new ArrayList();
        Users spouseData = userRepository.findById(userDetails.getId()).get();
        for (ParentChild ele : parentList) {
            Users child = userRepository.findById(ele.getUserId()).get();
            if (child.getOrgId() != null && child.getOrgId() != 0) {
                Organization sch = new Organization();
                sch = orgRepository.findById(child.getOrgId()).get();
                child.setOrgName(sch.getOrgName());
            }
            childList.add(child);
        }
        childList.add(oth);
        if (spouseData != null && spouseData.getUserLang().contains("Spanish")) {
            langRetrieved = "Registro recuperado con éxito";
            langNotFound = "Registro no encontrado";
        }
        try {
            if (!childList.isEmpty()) {
                message = langRetrieved;
            } else {
                message = langNotFound;
                status = HttpStatus.UNAUTHORIZED;
                childList = null;
            }
            return new ApiResponse(status, message, childList);
        } catch (Exception e) {
            return new ApiResponse(HttpStatus.BAD_REQUEST, "Exception", e.getMessage());
        }
    }

//    @PostMapping("/delete_child_by_id")
    public ApiResponse deleteChild(Users userDetails) {
        String langChildDelete = "Friend / Spouse deleted successfully";
        String langNotFound = "Record not Found";
        String message = "";
        Map<String, Object> result = new HashMap();
        List individualList = new ArrayList();
//        List result = new ArrayList();
//        boolean isSameAdmin = false; //to check deleting child school is same for other childs
        HttpStatus status = HttpStatus.OK;
//        Users childDetails = userRepository.findById(userDetails.getId()).get();
        ApiResponse childGroup = chatService.getChatIds(3, userDetails.getFriendId()); //getting group chat
        Users parentLinkDeleted = userRepository.findById(userDetails.getFriendId()).get();
        ParentChild parChildDelete = parChildRepository.findByUserIdAndFriendId(userDetails.getId(), userDetails.getFriendId());
//        Users parentData = userRepository.findById(userDetails.getFriendId()).get();
        if (parentLinkDeleted != null && parentLinkDeleted.getUserLang().contains("Spanish")) {
            langChildDelete = "Amigo / Cónyuge eliminado correctamente";
            langNotFound = "Registro no encontrado";
        }
        try {
            if (parChildDelete != null && parentLinkDeleted != null) {
//                childDetails.setFriendId(null);
//                userRepository.save(childDetails);
                message = langChildDelete;
//                chatRepository.delete(chat);
                parChildRepository.delete(parChildDelete);
//              Chat delete part starts
//                List<ParentChild> parChildList = parChildRepository.findAllByFriendId(userDetails.getFriendId());
//                if (!parChildList.isEmpty()) {
//                    for (ParentChild item : parChildList) {
//                        Users getChild = userRepository.findById(item.getUserId()).get();
//                        if (Objects.equals(getChild.getOrgId(), childLinkDeleted.getOrgId())) {
//                            isSameAdmin = true;
//                        }
//                    }
//                }
//                Organization getSchool = orgRepository.findById(childLinkDeleted.getOrgId()).get();
                HashMap hm = new HashMap();
                hm.put("Name", parentLinkDeleted.getName());
                hm.put("Id", parentLinkDeleted.getId());
                hm.put("User_Type_Id", 3);
                hm.put("Grade", parentLinkDeleted.getGrade());
                hm.put("School", "");
                individualList.add(hm);
//                if (isSameAdmin == false) {
//                    List<Users> adminList = userRepository.findAllByUserTypeIdAndOrgId(5, childLinkDeleted.getOrgId());
//                    if (!adminList.isEmpty()) {
//                        for (Users items : adminList) {
//                            HashMap hm1 = new HashMap();
//                            hm1.put("Name", items.getName());
//                            hm1.put("Id", items.getId());
//                            hm1.put("User_Type_Id", 5);
//                            hm1.put("Grade", null);
//                            hm1.put("School", getSchool.getOrgName());
//                            individualList.add(hm1);
//                        }
//                    }
//                }
                result.put("individualList", individualList);
                if (childGroup.getResult() != null) {
                    result.put("groupList", childGroup.getResult());
                } else {
                    result.put("groupList", null);
                }
//                Chat delete part ends
            } else {
                message = langNotFound;
                status = HttpStatus.NOT_FOUND;
                result = null;
//                childDetails = null;
            }
            return new ApiResponse(status, message, result);
        } catch (Exception e) {
            return new ApiResponse(HttpStatus.BAD_REQUEST, "Exception", e.getMessage());
        }
    }

//    @PostMapping("/update_profile")
    public ApiResponse updateUser(Users userDetails) {
        String langUpdate = "Record Updated Successfully";
        String langNotFound = "Record not Found";
        String langMobReg = "Mobile number already registered";
        String message = "";
        Users login = userRepository.findById(userDetails.getId()).get();
        if (userDetails.getUserLang().contains("Spanish")) {
            langUpdate = "Registro actualizado con éxito";
            langNotFound = "Registro no encontrado";
            langMobReg = "Número de teléfono móvil ya registrado";
        }
        int schId = 0;
        if (userDetails.getUserTypeId() == 1) {
            schId = login.getOrgId();
        }
        HttpStatus status = HttpStatus.OK;
        if (getOnlyDigits(login.getUserPhone()).equals(getOnlyDigits(userDetails.getUserPhone()))) {
            try {
                if (login != null) {
                    login.setEmail(userDetails.getEmail());
                    login.setOrgId(userDetails.getOrgId());
                    login.setOrgName(userDetails.getOrgName());
                    login.setFriendSpouseName(userDetails.getFriendSpouseName());                    
                    login.setOrgStateId(userDetails.getOrgStateId());
                    login.setUserTypeId(userDetails.getUserTypeId());
                    login.setAge(userDetails.getAge());
                    login.setName(userDetails.getName());
                    login.setCcUser(userDetails.getCcUser());
                    login.setUserPhone(getOnlyDigits(userDetails.getUserPhone()));
                    login.setCcParent1(userDetails.getCcParent1());
                    login.setParentPhone1(getOnlyDigits(userDetails.getParentPhone1()));
                    login.setCcParent2(userDetails.getCcParent2());
                    login.setParentPhone2(getOnlyDigits(userDetails.getParentPhone2()));
                    login.setCcDriver(userDetails.getCcDriver());
                    login.setDriverPhone(getOnlyDigits(userDetails.getDriverPhone()));
                    login.setCcOrg(userDetails.getCcOrg());
                    login.setOrgPhone(getOnlyDigits(userDetails.getOrgPhone()));
                    login.setGrade(userDetails.getGrade());
                    login.setGender(userDetails.getGender());
                    login.setAddress(userDetails.getAddress());
                    login.setZipCode(userDetails.getZipCode());
                    login.setCity(userDetails.getCity());
                    login.setState(userDetails.getState());
                    login.setBusRoute(userDetails.getBusRoute());
                    login.setProfileImage(userDetails.getProfileImage());
                    login.setFbCyber(userDetails.getFbCyber());
                    login.setInstaCyber(userDetails.getInstaCyber());
                    login.setSnapShotCyber(userDetails.getSnapShotCyber());
                    login.setUserLang(userDetails.getUserLang());
                    userRepository.save(login);
                    message = langUpdate;
                    status = HttpStatus.OK;
                    if (login.getUserTypeId() == 1) {
                        if (userDetails.getOrgId() != schId) {
                            if (userDetails.getOrgId() > 0) {
                                GroupChat oldChatDelete = chatRepository.findTop1ByUserId(login.getId());
                                if (oldChatDelete != null) {
                                    chatRepository.delete(oldChatDelete);
                                    GroupChat grpChat = new GroupChat();
                                    grpChat.setUserId(login.getId());
                                    grpChat.setOrgId(userDetails.getOrgId());
                                    chatRepository.save(grpChat);
                                }
                            }
                            List<PushNotifications> msgList = notifyRepository.findAllByUserIdOrderByIdDesc(userDetails.getId());
                            if (!msgList.isEmpty()) {
                                notifyRepository.deleteAll(msgList);
                            }
                        }
                    }
                } else {
                    message = langNotFound;
                    status = HttpStatus.UNAUTHORIZED;
                    login = null;
                }
                return new ApiResponse(status, message, login);
            } catch (Exception e) {
                return new ApiResponse(HttpStatus.BAD_REQUEST, "Exception", e.getMessage());
            }
        } else {
            Users userUpdate = userRepository.findByUserPhone(getOnlyDigits(userDetails.getUserPhone()));
            if (userUpdate != null) {
                return new ApiResponse(HttpStatus.UNAUTHORIZED, langMobReg, "Error");
            } else {
                try {
                    if (login != null) {
                        login.setEmail(userDetails.getEmail());
                        login.setOrgId(userDetails.getOrgId());
                        login.setOrgName(userDetails.getOrgName());
                        login.setFriendSpouseName(userDetails.getFriendSpouseName());
                        login.setOrgStateId(userDetails.getOrgStateId());
                        login.setUserTypeId(userDetails.getUserTypeId());
                        login.setName(userDetails.getName());
                        login.setAge(userDetails.getAge());
                        login.setCcUser(userDetails.getCcUser());
                        login.setUserPhone(getOnlyDigits(userDetails.getUserPhone()));
                        login.setCcParent1(userDetails.getCcParent1());
                        login.setParentPhone1(getOnlyDigits(userDetails.getParentPhone1()));
                        login.setCcParent2(userDetails.getCcParent2());
                        login.setParentPhone2(getOnlyDigits(userDetails.getParentPhone2()));
                        login.setCcDriver(userDetails.getCcDriver());
                        login.setDriverPhone(getOnlyDigits(userDetails.getDriverPhone()));
                        login.setCcOrg(userDetails.getCcOrg());
                        login.setOrgPhone(getOnlyDigits(userDetails.getOrgPhone()));
                        login.setGrade(userDetails.getGrade());
                        login.setGender(userDetails.getGender());
                        login.setAddress(userDetails.getAddress());
                        login.setZipCode(userDetails.getZipCode());
                        login.setCity(userDetails.getCity());
                        login.setState(userDetails.getState());
                        login.setBusRoute(userDetails.getBusRoute());
                        login.setProfileImage(userDetails.getProfileImage());
                        login.setFbCyber(userDetails.getFbCyber());
                        login.setInstaCyber(userDetails.getInstaCyber());
                        login.setSnapShotCyber(userDetails.getSnapShotCyber());
                        login.setUserLang(userDetails.getUserLang());
                        userRepository.save(login);
                        message = langUpdate;
                        status = HttpStatus.OK;
                        if (login.getUserTypeId() == 1) {
                            if (userDetails.getOrgId() != schId) {
                                if (userDetails.getOrgId() > 0) {
                                    GroupChat oldChatDelete = chatRepository.findTop1ByUserId(login.getId());
                                    if (oldChatDelete != null) {

                                        chatRepository.delete(oldChatDelete);
                                        GroupChat grpChat = new GroupChat();
                                        grpChat.setUserId(login.getId());
                                        grpChat.setOrgId(userDetails.getOrgId());
                                        chatRepository.save(grpChat);
                                    }
                                }
                                List<PushNotifications> msgList = notifyRepository.findAllByUserIdOrderByIdDesc(userDetails.getId());
                                if (!msgList.isEmpty()) {
                                    notifyRepository.deleteAll(msgList);
                                }
                            }
                        }
                    } else {
                        message = langNotFound;
                        status = HttpStatus.UNAUTHORIZED;
                        login = null;
                    }
                    return new ApiResponse(status, message, login);
                } catch (Exception e) {
                    return new ApiResponse(HttpStatus.BAD_REQUEST, "Exception", e.getMessage());
                }
            }
        }
    }

//    @PostMapping("/get_user_by_id")
    public ApiResponse getUser(Users userDetails) {
        String langRetrieved = "Record retrieved successfully";
        String langNotFound = "Record not Found";
        String message = "";
        Optional<Users> userDetail = userRepository.findById(userDetails.getId());
        Users login = new Users();
        try {
            if (userDetail.isPresent()) {
                login = userDetail.get();
                if (login.getUserLang().contains("Spanish")) {
                    langRetrieved = "Registro recuperado con éxito";
                    langNotFound = "Registro no encontrado";
                }
                if (login.getUserTypeId() == 3) {
                    ParentChild parChild = parChildRepository.findTop1ByFriendId(userDetails.getId());
                    if (parChild != null) {
                        Users child = userRepository.findById(parChild.getUserId()).get();
                        login.setOrgId(child.getOrgId());
                    }
                }
//                if (login.getOrgId() > 0) {
//                    Organization sch = orgRepository.findById(login.getOrgId()).get();
//                    login.setOrgName(sch.getOrgName());
//                    login.setOrgStateId(sch.getStateId());
//                }

                message = langRetrieved;
            } else {
                message = langNotFound;
            }
            return new ApiResponse(HttpStatus.OK, message, login);
        } catch (Exception e) {
            return new ApiResponse(HttpStatus.BAD_REQUEST, "Exception", e.getMessage());
        }
    }

//    @PostMapping("/get_chat_group_ids") 
    // Individual chat list 
    public ApiResponse getIdForChat(Users obj) {
        String langRetrieved = "Record retrieved successfully";
        String langNotFound = "Record not Found";
        String message = "";
        Users getChatIds = userRepository.findById(obj.getId()).get();
        if (getChatIds != null && getChatIds.getUserLang().contains("Spanish")) {
            langRetrieved = "Registro recuperado con éxito";
            langNotFound = "Registro no encontrado";
        }
        HttpStatus status = HttpStatus.OK;
        try {
            List list = new ArrayList();
            if (getChatIds != null) {
                message = langRetrieved;
                status = HttpStatus.OK;
                if (obj.getUserTypeId() == 1) {
                    List<ParentChild> parentList = parChildRepository.findAllByUserId(obj.getId());
                    if (parentList != null) {
                        for (ParentChild ele : parentList) {
                            HashMap hm1 = new HashMap();
                            Users getParent = userRepository.findById(ele.getFriendId()).get();
                            hm1.put("Name", getParent.getName());
                            hm1.put("Id", getParent.getId());
                            hm1.put("User_Type_Id", getParent.getUserTypeId());
                            hm1.put("Grade", getParent.getGrade());
                            hm1.put("School", "");
                            list.add(hm1);
                        }
                    }
                    if (getChatIds.getOrgId() != null && getChatIds.getOrgId() != 0) {
                        List<Users> schoolAdmin = userRepository.findAllByUserTypeIdAndOrgId(5, getChatIds.getOrgId());
                        if (schoolAdmin != null) {
                            for (Users list2 : schoolAdmin) {
                                HashMap hm4 = new HashMap();
                                hm4.put("Name", list2.getName());
                                hm4.put("Id", list2.getId());
                                hm4.put("User_Type_Id", list2.getUserTypeId());
                                hm4.put("Grade", list2.getGrade());
                                hm4.put("School", "");
                                list.add(hm4);
                            }
                        }
                    }
                } else if (obj.getUserTypeId() == 3) {
                    List<ParentChild> parentList = parChildRepository.findAllByFriendId(getChatIds.getId());
                    List<Users> getChild = new ArrayList();
                    for (ParentChild ele : parentList) {
                        Users child = userRepository.findById(ele.getUserId()).get();
                        getChild.add(child);
                    }
                    for (Users item : getChild) {
                        Organization sch = new Organization();
                        if (item.getOrgId() != null && item.getOrgId() != 0) {
                            sch = orgRepository.findById(item.getOrgId()).get();
                            List<Users> teach = userRepository.findAllByUserTypeIdAndOrgIdAndGrade(2, item.getOrgId(), item.getGrade());
                            if (teach != null) {
                                for (Users item1 : teach) {
                                    HashMap hm3 = new HashMap();
                                    hm3.put("Name", item1.getName());
                                    hm3.put("Id", item1.getId());
                                    hm3.put("User_Type_Id", 2);
                                    hm3.put("Grade", item1.getGrade());
                                    hm3.put("School", sch.getOrgName());
                                    if (!list.contains(hm3)) {
                                        list.add(hm3);
                                    }
                                }
                            }
                            List<Users> schoolAdmin = userRepository.findAllByUserTypeIdAndOrgId(5, item.getOrgId());
                            if (schoolAdmin != null) {
                                for (Users item1 : schoolAdmin) {
                                    HashMap hm4 = new HashMap();
                                    hm4.put("Name", item1.getName());
                                    hm4.put("Id", item1.getId());
                                    hm4.put("User_Type_Id", item1.getUserTypeId());
                                    hm4.put("Grade", item1.getGrade());
                                    hm4.put("School", sch.getOrgName());
                                    list.add(hm4);
                                }
                            }
                        }
                        HashMap hm1 = new HashMap();
                        hm1.put("Name", item.getName());
                        hm1.put("Id", item.getId());
                        hm1.put("User_Type_Id", item.getUserTypeId());
                        hm1.put("Grade", item.getGrade());
                        hm1.put("School", sch.getOrgName());
                        list.add(hm1);
                    }
                } else if (obj.getUserTypeId() == 2) {
                    List<Users> getAllAdmins = userRepository.findAllByUserTypeIdAndOrgId(5, getChatIds.getOrgId());
                    for (Users item : getAllAdmins) {
                        HashMap hmList1 = new HashMap();
                        hmList1.put("Name", item.getName());
                        hmList1.put("Id", item.getId());
                        hmList1.put("User_Type_Id", item.getUserTypeId());
                        hmList1.put("Grade", item.getGrade());
                        hmList1.put("School", "");
                        list.add(hmList1);
                    }
                    List<Users> getAllParents = userRepository.fetchByUserTypeAndOrgIdAndGrade(1, getChatIds.getOrgId(), getChatIds.getGrade());
                    for (Users item : getAllParents) {
                        HashMap hmList2 = new HashMap();
                        hmList2.put("Name", item.getName());
                        hmList2.put("Id", item.getId());
                        hmList2.put("User_Type_Id", item.getUserTypeId());
                        hmList2.put("Grade", item.getGrade());
                        hmList2.put("School", "");
                        list.add(hmList2);
                    }
                } else if (obj.getUserTypeId() == 4) {
                    List<Users> getAllAdmins = userRepository.findAllByUserTypeIdAndOrgId(5, getChatIds.getOrgId());
                    for (Users item : getAllAdmins) {
                        HashMap hmList1 = new HashMap();
                        hmList1.put("Name", item.getName());
                        hmList1.put("Id", item.getId());
                        hmList1.put("User_Type_Id", item.getUserTypeId());
                        hmList1.put("Grade", item.getGrade());
                        hmList1.put("School", "");
                        list.add(hmList1);
                    }
                } else if (obj.getUserTypeId() == 5) {
                    List<Integer> ids = new ArrayList();
                    ids.add(3);
                    ids.add(5);
                    List<Users> getAllUsers = userRepository.findAllByOrgIdAndUserTypeIdNotIn(getChatIds.getOrgId(), ids);
                    List<Users> getAllParents = userRepository.fetchAllParentsByOrgId(getChatIds.getOrgId());
                    for (Users item : getAllUsers) {
                        HashMap hmList1 = new HashMap();
                        hmList1.put("Name", item.getName());
                        hmList1.put("Id", item.getId());
                        hmList1.put("User_Type_Id", item.getUserTypeId());
                        hmList1.put("Grade", item.getGrade());
                        hmList1.put("School", "");
                        list.add(hmList1);
                    }
                    if (getAllParents != null) {
                        for (Users item : getAllParents) {
                            HashMap hmList2 = new HashMap();
                            hmList2.put("Name", item.getName());
                            hmList2.put("Id", item.getId());
                            hmList2.put("User_Type_Id", item.getUserTypeId());
                            hmList2.put("Grade", item.getGrade());
                            hmList2.put("School", "");
                            if (!list.contains(hmList2)) {
                                list.add(hmList2);
                            }
                        }
                    }
                }
            } else {
                message = langNotFound;
                status = HttpStatus.NOT_FOUND;
            }
            return new ApiResponse(status, message, list);
        } catch (Exception e) {
            return new ApiResponse(HttpStatus.BAD_REQUEST, "Exception", e.getMessage());

        }
    }

//    @PostMapping("/get_group_chat")
//    public ApiResponse getGroupChat(Users obj) {
//        String message = "";
//        HttpStatus status = HttpStatus.OK;
//        try {
//            Users getChatIds = userRepository.findById(obj.getId()).get();
//            List outerList = new ArrayList();
//            List<User> getChild = userRepository.findAllByFriendId(getChatIds.getId());
//            for (Users item : getChild) {
//                List list = new ArrayList();
//                HashMap hm1 = new HashMap();
//                hm1.put("Name", item.getName());
//                hm1.put("Id", item.getId());
//                hm1.put("User_Type_Id", item.getUserTypeId());
//                list.add(hm1);
//                List<User> schoolAdmin = userRepository.findAllByUserTypeIdAndOrgId(5, item.getOrgId());
//                if (schoolAdmin != null) {
//                    for (Users item1 : schoolAdmin) {
//                        HashMap hm4 = new HashMap();
//                        hm4.put("Name", item1.getName());
//                        hm4.put("Id", item1.getId());
//                        hm4.put("User_Type_Id", item1.getUserTypeId());
//                        list.add(hm4);
//                    }
//                }
//                outerList.add(list);
//            }
//            return new ApiResponse(status, message, outerList);
//        } catch (Exception e) {
//            return new ApiResponse(HttpStatus.BAD_REQUEST, "Exception", e);
//
//        }
//    }
    @Transactional
//    @PostMapping("/logout")
    public ApiResponse logout(Users obj
    ) {
        String message = null;
        String result = null;
        HttpStatus status = HttpStatus.OK;
        try {
            message = "User logged out successfully";
            status = HttpStatus.OK;
            result = "Success";
            return new ApiResponse(status, message, result);
        } catch (Exception e) {
            return new ApiResponse(HttpStatus.BAD_REQUEST, "Error", "Error");

        }
    }

    @Transactional
//    @PostMapping("/enable_ads")
    public ApiResponse checkAds(Users obj) {

        String langNotFound = "Record not Found";
        String langSuccess = "Success";
        String message = null;
        HttpStatus status = HttpStatus.OK;
        try {
            Users userAds = userRepository.findById(obj.getId()).get();
            EnableAds enableAds = adsRepository.findById(1).get();

            if (userAds != null && userAds.getUserLang().contains("Spanish")) {
                langSuccess = "Éxito";
                langNotFound = "Registro no encontrado";
            }
            HashMap hm = new HashMap();
            if (userAds != null) {
                hm.put("isAdsOn", userAds.isIsAdsOn());
                hm.put("enableAds", enableAds.isEnableAds());
                message = langSuccess;
            } else {
                message = langNotFound;
                status = HttpStatus.NOT_FOUND;
                hm = null;
            }
            return new ApiResponse(status, message, hm);
        } catch (Exception e) {
            return new ApiResponse(HttpStatus.BAD_REQUEST, "Error", "Error");

        }
    }

    @Transactional
//    @PostMapping("/update_language")
    public ApiResponse updateLang(Users obj) {

        String langNotFound = "Record not Found";
        String langSuccess = "Language Updated Successfully";
        String message = "";
        HttpStatus status = HttpStatus.OK;
        try {
            if (obj.getUserLang().contains("Spanish")) {
                langSuccess = "Idioma actualizado correctamente";
                langNotFound = "Registro no encontrado";
            }
            Users userAds = userRepository.findById(obj.getId()).get();
            if (userAds != null) {
                userAds.setUserLang(obj.getUserLang());
                userRepository.save(userAds);
                message = langSuccess;
            } else {
                status = HttpStatus.NOT_FOUND;
                message = langNotFound;
            }
            return new ApiResponse(status, message, userAds);
        } catch (Exception e) {
            return new ApiResponse(HttpStatus.BAD_REQUEST, "Error", "Error");

        }
    }

    public static String getUserType(int id, String lang) throws NoSuchAlgorithmException {
        String userType = "";
        if (lang.contains("Spanish")) {
            switch (id) {
                case 1:
                    userType = "Usuario";
                    break;
                case 2:
                    userType = "Amigo / Cónyuge";
                    break;
                case 3:
                    userType = "Catedrático / Gerente";
                    break;
                case 4:
                    userType = "Chofer";
                    break;
                case 5:
                    userType = "Administrador";
                    break;
            }
        } else {
            switch (id) {
                case 1:
                    userType = "User";
                    break;
                case 2:
                    userType = "Professor / Manager";
                    break;
                case 3:
                    userType = "Friend / Spouse";
                    break;
                case 4:
                    userType = "Driver";
                    break;
                case 5:
                    userType = "Admin";
                    break;
            }
        }
        return userType;
//        return pwd;
    }

    @Transactional
//    @PostMapping("/screenshot_count")
    public ApiResponse screenShotCount(ScreenshotCount obj) {
        String message = null;
        HttpStatus status = HttpStatus.OK;
        try {
            message = "Success";
            status = HttpStatus.OK;
            HashMap hm = new HashMap();
            if (obj.getUserId() > 0 && obj.getName() != null) {
                screenshotRepo.save(obj);
                List<ScreenshotCount> count = screenshotRepo.findAllByUserId(obj.getUserId());
                hm.put("screenshotCount", count.size());
            } else {
                message = "UserId and Name is not valid";
                status = HttpStatus.NOT_FOUND;
                hm.put("screenshotCount", 0);
            }
            return new ApiResponse(status, message, hm);
        } catch (Exception e) {
            return new ApiResponse(HttpStatus.BAD_REQUEST, "Error", e);

        }
    }

    @Transactional
    public ApiResponse AddUserContact(UserContacts obj) {
        String langSuccess = "Success";
        String langMessage = "Contact already added";
        Integer inviteStatus = 0;
        HttpStatus status = HttpStatus.OK;
        obj.setContactPhone(getOnlyDigits(obj.getContactPhone()));
        try {
            Users user = userRepository.findById(obj.getUserId()).get();
            if (user.getUserLang().contains("Spanish")) {
                langSuccess = "Éxito";
                langMessage = "Contacto ya agregado";
            }
            status = HttpStatus.OK;
            Users userExist = userRepository.findTop1ByCcUserAndUserPhoneAndUserTypeId(obj.getCcPhone(), obj.getContactPhone(), 3);
            if (userExist == null) {
                UserContacts contactExist = userContactRepository.findByUserIdAndContactPhone(obj.getUserId(), obj.getContactPhone());
                if (contactExist == null) {
                    obj.setInviteStatus(inviteStatus);
                    userContactRepository.save(obj);
                } else {
                    status = HttpStatus.FOUND;
                    langSuccess = langMessage;
                }
            } else {
                if (user.getOrgId() != null && user.getOrgId() != 0) {
                    GroupChat grpChat = new GroupChat();
                    GroupChat chatExist = chatRepository.findByUserIdAndOrgId(user.getId(), user.getOrgId());
                    if (chatExist == null) {
                        grpChat.setUserId(user.getId());
                        grpChat.setOrgId(user.getOrgId());
                        chatRepository.save(grpChat);
                    }
                }
                ParentChild parentExist = parChildRepository.findByUserIdAndFriendId(obj.getUserId(), userExist.getId());
                if (parentExist == null) {
                    ParentChild parent = new ParentChild();
                    parent.setUserId(obj.getUserId());
                    parent.setFriendId(userExist.getId());
                    parChildRepository.save(parent);
                } else {
                    status = HttpStatus.FOUND;
                    langSuccess = langMessage;
                }
            }
            return new ApiResponse(status, langSuccess, "Success");
        } catch (Exception e) {
            return new ApiResponse(HttpStatus.BAD_REQUEST, "Error", e);

        }
    }
    
   

    @Transactional
    public ApiResponse DeleteUserContact(UserContacts obj) {
        String langSuccess = "Success";
        HttpStatus status = HttpStatus.OK;
        try {
            UserContacts userContact = userContactRepository.findById(obj.getId()).get();
            if (userContact != null) {
                Users user = userRepository.findById(userContact.getUserId()).get();
                if (user.getUserLang().contains("Spanish")) {
                    langSuccess = "Éxito";
                }
                userContactRepository.delete(userContact);
                status = HttpStatus.OK;
            } else {
                langSuccess = "Error";
                status = HttpStatus.BAD_REQUEST;
            }
            return new ApiResponse(status, langSuccess, "Success");
        } catch (Exception e) {
            return new ApiResponse(HttpStatus.BAD_REQUEST, "Error", e.getMessage());

        }
    }

    public ApiResponse1 getFriends(Users userDetails) {
        String langRetrieved = "Record retrieved successfully";
        String langNotFound = "Record not Found";
        String message = "";
        HttpStatus status = HttpStatus.OK;
        Users oth = new Users();
        oth.setId(0);
        oth.setName("Others");
        oth.setEmail("Others");
        try {
            List<ParentChild> parentList = parChildRepository.findAllByUserId(userDetails.getId());
            List<Users> childList = new ArrayList();
            if (!parentList.isEmpty()) {

                Users child = userRepository.findById(userDetails.getId()).get();
                Organization sch = orgRepository.findById(child.getOrgId()).get();
                for (ParentChild ele : parentList) {
                    Users spouse = userRepository.findById(ele.getFriendId()).get();
                    if (sch.getId() > 0) {
                        spouse.setOrgName(sch.getOrgName());
                    }
                    childList.add(spouse);
                }
            }
            childList.add(oth);
            Users parentData = userRepository.findById(userDetails.getId()).get();
            if (parentData != null && parentData.getUserLang().contains("Spanish")) {
                langRetrieved = "Registro recuperado con éxito";
                langNotFound = "Registro no encontrado";
            }
            List<UserContacts> contactList = userContactRepository.findAllByUserId(userDetails.getId());
            if (!childList.isEmpty()) {
                message = langRetrieved;
            } else {
                message = langNotFound;
                status = HttpStatus.UNAUTHORIZED;
                childList = null;
            }
            return new ApiResponse1(status, message, contactList, childList);
        } catch (Exception e) {
            return new ApiResponse1(HttpStatus.BAD_REQUEST, "Exception", null, e.getMessage());
        }
    }

    public ApiResponse updateSpouseToUser(Users userDetails) {
        String langUpdate = "Record Updated Successfully";
        String recordNotFound = "Record not Found";
        Integer inviteStatus = 0;
        String message = "";
        Users login = userRepository.findById(userDetails.getId()).get();
        if (userDetails.getUserLang().contains("Spanish")) {
            langUpdate = "Registro actualizado con éxito";
            recordNotFound = "Registro no encontrado";
        }
        HttpStatus status = HttpStatus.OK;
        try {
            if (login != null) {
                login.setOrgId(userDetails.getOrgId());
                login.setOrgId(userDetails.getOrgTypeId());
                login.setOrgName(userDetails.getOrgName());
                login.setOrgStateId(userDetails.getOrgStateId());
                login.setUserTypeId(userDetails.getUserTypeId());
                login.setAge(userDetails.getAge());
                login.setCcParent1(userDetails.getCcParent1());
                login.setParentPhone1(getOnlyDigits(userDetails.getParentPhone1()));
                login.setCcParent2(userDetails.getCcParent2());
                login.setParentPhone2(getOnlyDigits(userDetails.getParentPhone2()));
                login.setCcDriver(userDetails.getCcDriver());
                login.setDriverPhone(getOnlyDigits(userDetails.getDriverPhone()));
                login.setCcOrg(userDetails.getCcOrg());
                login.setOrgPhone(getOnlyDigits(userDetails.getOrgPhone()));
                login.setGrade(userDetails.getGrade());
                login.setGender(userDetails.getGender());
                login.setAddress(userDetails.getAddress());
                login.setZipCode(userDetails.getZipCode());
                login.setCity(userDetails.getCity());
                login.setState(userDetails.getState());
                login.setBusRoute(userDetails.getBusRoute());
                login.setProfileImage(userDetails.getProfileImage());
                login.setFbCyber(userDetails.getFbCyber());
                login.setInstaCyber(userDetails.getInstaCyber());
                login.setSnapShotCyber(userDetails.getSnapShotCyber());
                userRepository.save(login);
                if (userDetails.getCcParent1() != null && userDetails.getParentPhone1() != null) {
                    Users userContact = userRepository.findTop1ByCcUserAndUserPhoneAndUserTypeId(userDetails.getCcParent1(), userDetails.getParentPhone1(), 3);
                    if (userContact == null) {
                        UserContacts contact = new UserContacts();
                        contact.setUserId(login.getId());
                        contact.setContactName(userDetails.getFriendSpouseName());
                        contact.setCcPhone(userDetails.getCcParent1());
                        contact.setContactPhone(userDetails.getParentPhone1());
                        contact.setInviteStatus(inviteStatus);
                        userContactRepository.save(contact);
                    } else {
                        ParentChild parent = new ParentChild();
                        parent.setUserId(login.getId());
                        parent.setFriendId(userContact.getId());
                        parChildRepository.save(parent);
                    }
                }
                message = langUpdate;
                status = HttpStatus.OK;

            } else {
                message = recordNotFound;
                status = HttpStatus.UNAUTHORIZED;
                login = null;
            }
            return new ApiResponse(status, message, login);
        } catch (Exception e) {
            return new ApiResponse(HttpStatus.BAD_REQUEST, "Exception", e.getMessage());
        }
    }
}
