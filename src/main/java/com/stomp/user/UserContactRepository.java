/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stomp.user;

import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Admin
 */
@Repository
public interface UserContactRepository extends JpaRepository<UserContacts, Long> {

    List<UserContacts> findAllByUserId(long id);

    UserContacts findByUserIdAndContactPhone(long id, String phone);
    
    List<UserContacts> findAllByContactPhone(String no);

    //public Optional<UserContacts> findAllByUserIdAndContactPhone();

    public Optional<UserContacts> findAllByUserIdAndContactPhone(long userId, String contactPhone);
}
