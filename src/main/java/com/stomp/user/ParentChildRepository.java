/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stomp.user;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author Welcome
 */
public interface ParentChildRepository extends JpaRepository<ParentChild, Long> {

    List<ParentChild> findAllByFriendId(long id);

    List<ParentChild> findAllByUserId(long id);

    ParentChild findByUserIdAndFriendId(long childId, long parentId);

    ParentChild findTop1ByFriendId(long parentId);

}
