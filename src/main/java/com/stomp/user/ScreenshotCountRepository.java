/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stomp.user;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author Admin
 */
public interface ScreenshotCountRepository extends JpaRepository<ScreenshotCount, Long> {

    List<ScreenshotCount> findAllByUserId(long userId);
}
