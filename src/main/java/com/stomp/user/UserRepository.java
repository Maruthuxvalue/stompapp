/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stomp.user;

import com.stomp.notifications.PushNotifications;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author Welcome
 */
public interface UserRepository extends JpaRepository<Users, Long> {

    Users findByEmail(String email);

    Users findTop1ByEmail(String email);

    Users findByUserPhone(String phone);

    Users findTop1ByUserPhone(String phone);

    Users findTop1ByUserPhoneAndUserTypeId(String phone, int id);

    Users findByUserTypeIdAndEmail(int userTypeId, String email);

    Users findByUserTypeIdAndOrgId(int userTypeId, int orgId);

    List<Users> findAllByUserTypeIdAndOrgId(int userTypeId, int orgId);

    List<Users> findAllByOrgIdAndUserTypeIdNot(int orgId, int userTypeId);

    List<Users> findAllByOrgIdAndUserTypeIdNotIn(int orgId, List<Integer> userTypeId);

    List<Users> findAllByUserTypeIdAndOrgIdAndGrade(int userTypeId, int orgId, String grade);

    Users findByOrgId(int id);

    List<Users> findAllByOrgId(int id);

    Users findByUserTypeId(int userId);

    Users findTop1ByUserTypeIdAndOrgId(int userTypeId, int orgId);

//    Users findByParentId(long parentId);
//    List<User> findAllByParentId(long parentId);
    Users findByUserTypeIdAndSocialMediaId(int id, String socialId);

    Users findBySocialMediaId(String socialId);

    Users findByIsSocialUserAndSocialMediaId(int id, String socialId);

    Users findByIsSocialUserAndUserTypeIdAndSocialMediaId(int id, int typeId, String socialId);

    List<PushNotifications> fetchByManagerIdForNotification(@Param("orgId") int id, @Param("grade") String grade, @Param("userId") long userId);

    List<Users> fetchAllParentsByOrgId(@Param("orgId") int id);

    List<Users> fetchByUserTypeAndOrgIdAndGrade(@Param("userTypeId") int userType, @Param("orgId") int id, @Param("grade") String grade);

    List<PushNotifications> fetchByOrgIdForNotification(@Param("orgId") int id, @Param("userId") long userId);

    List<Users> findAllByBusRoute(String id);

    List<Users> findAllByUserPhone(String ph);

    Users findTop1ByCcUserAndUserPhone(String cc, String phone);

    Users findTop1ByCcUserAndUserPhoneAndUserTypeId(String cc, String phone, int userType);
}
