/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stomp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.bind.annotation.CrossOrigin;

/**
 *
 * @author Welcome
 */
@SpringBootApplication
@EnableAutoConfiguration
@CrossOrigin(origins = "*", allowedHeaders = "*")

//@ComponentScan({"com.aivis.user", "com.aivis.upload", "com.aivis.projects", "com.aivis.dataset", "com.aivis.favourites", "com.aivis.admin"})
//@EnableJpaRepositories({"com.aivis.user", "com.aivis.projects", "com.aivis.dataset", "com.aivis.favourites", "com.aivis.admin"})
//@EntityScan({"com.aivis.user", "com.aivis.projects", "com.aivis.dataset", "com.aivis.favourites", "com.aivis.admin"})
public class STOMPMain {

    public static void main(String[] args) {
        SpringApplication.run(STOMPMain.class, args);
       
    }
}
