/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stomp.org;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author Welcome
 */
public interface OrgRepository extends JpaRepository<Organization, Integer> {

    List<Organization> findAllByStateId(int id);

    List<Organization> findAllByStateIdAndZipCodeAndOrgNameIgnoreCaseContaining(int id, String zipcode, String school);

    List<Organization> findTop50ByStateIdAndOrgNameStartsWithOrderByOrgNameAsc(int id, String school);

    List<Organization> findTop50ByOrgNameStartsWithOrderByOrgNameAsc(String school);

}
