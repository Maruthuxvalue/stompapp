/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stomp.org;

import com.response.ApiResponse;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Welcome
 */
//@RestController
//@RequestMapping(value = "/bb/schools")
@Service
public class OrgController {

    @Autowired
    OrgRepository schoolsRepository;

    @PostMapping("/get_all_schools")
    public ApiResponse getOrgs() {

        List<Organization> list1;
        List schoolsList = new ArrayList();
        try {
            list1 = schoolsRepository.findAll();
            list1.stream().forEach((ele) -> {
                HashMap hm = new HashMap();
                hm.put("id", ele.getId());
                hm.put("schoolName", ele.getOrgName());
                schoolsList.add(hm);
            });
        } catch (Exception e) {
            return new ApiResponse(HttpStatus.BAD_REQUEST, "Exception", e.getMessage());

        }
        return new ApiResponse(HttpStatus.OK, "Record retrieved successfully", schoolsList);

    }

    @PostMapping("/get_schools_by_state_id")
    public ApiResponse getSch(Organization obj) {

        List schoolsList = new ArrayList();
        List<Organization> list = new ArrayList();
        HashMap other = new HashMap();
        other.put("id", 0);
        other.put("schoolName", "Others");
        try {
            list = schoolsRepository.findAllByStateId(obj.getStateId());
            list.stream().forEach((ele) -> {
                HashMap hm = new HashMap();
                hm.put("id", ele.getId());
                hm.put("schoolName", ele.getOrgName());
                schoolsList.add(hm);
            });
            schoolsList.add(other);
        } catch (Exception e) {
            return new ApiResponse(HttpStatus.BAD_REQUEST, "Exception", e.getMessage());

        }
        return new ApiResponse(HttpStatus.OK, "Record retrieved successfully", schoolsList);

    }

    @PostMapping("/get_schools_autofill_by_state_id")
    public ApiResponse getOrgsByState(Organization obj) {

        List<Organization> schoolsList = new ArrayList();
        Organization other = new Organization();
        other.setOrgName("Others");
        other.setId(0);
        try {
            schoolsList = schoolsRepository.findTop50ByStateIdAndOrgNameStartsWithOrderByOrgNameAsc(obj.getStateId(), obj.getOrgName());
            schoolsList.add(other);
        } catch (Exception e) {
            return new ApiResponse(HttpStatus.BAD_REQUEST, "Exception", e.getMessage());

        }
        return new ApiResponse(HttpStatus.OK, "Record retrieved successfully", schoolsList);

    }

    @PostMapping("/get_schools_autofill")
    public ApiResponse getOrgsAutoFill(Organization obj) {

        List resultList = new ArrayList();
        try {
            List<Organization> schoolsList = schoolsRepository.findTop50ByStateIdAndOrgNameStartsWithOrderByOrgNameAsc(obj.getStateId(), obj.getOrgName());
            for (Organization list : schoolsList) {
                HashMap hm = new HashMap();
                hm.put("id", list.getId());
                hm.put("schoolName", list.getOrgName());
                hm.put("stateId", list.getStateId());
                resultList.add(hm);
            }
            HashMap hm1 = new HashMap();
            hm1.put("id", 0);
            hm1.put("schoolName", "Others");
            hm1.put("stateId", 0);
            resultList.add(hm1);
        } catch (Exception e) {
            return new ApiResponse(HttpStatus.BAD_REQUEST, "Exception", e.getMessage());

        }
        return new ApiResponse(HttpStatus.OK, "Record retrieved successfully", resultList);

    }

    @PostMapping("/get_schools_for_autofill")
    public ApiResponse getOrgsAuto(Organization obj) {

        List list = new ArrayList();
        try {
            List<Organization> schoolsList = schoolsRepository.findAllByStateIdAndZipCodeAndOrgNameIgnoreCaseContaining(obj.getStateId(), obj.getZipCode(), obj.getOrgName());
            for (Organization element : schoolsList) {
                HashMap hm = new HashMap();
                hm.put("Id", element.getId());
                hm.put("OrgName", element.getOrgName());
                list.add(hm);
            }
            HashMap hm1 = new HashMap();
            hm1.put("Id", 0);
            hm1.put("OrgName", "Others");
            list.add(hm1);
        } catch (Exception e) {
            return new ApiResponse(HttpStatus.BAD_REQUEST, "Exception", e.getMessage());

        }
        return new ApiResponse(HttpStatus.OK, "Record retrieved successfully", list);

    }

}
