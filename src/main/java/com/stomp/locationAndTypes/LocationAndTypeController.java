/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stomp.locationAndTypes;

import com.response.ApiResponse;
import com.stomp.EmergencyReporting.ESBReporting;
import com.stomp.org.Organization;
import com.stomp.user.Users;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PostMapping;

/**
 *
 * @author Admin
 */
@Service
public class LocationAndTypeController {

    @Autowired
    IncidentLocationRepository locationRepository;

    @Autowired
    IncidentTypeRepository typeRepository;

    public ApiResponse getLocationAndTypes() {
        try {
            HashMap hm = new HashMap();
            List<IncidentLocation> locList = locationRepository.findAll();
            List<IncidentType> typeList = typeRepository.findAll();
            hm.put("incidentLocation", locList);
            hm.put("incidentType", typeList);

            return new ApiResponse(HttpStatus.OK, "Success", hm);
        } catch (Exception e) {
            return new ApiResponse(HttpStatus.BAD_REQUEST, "Error", e.getMessage());
        }
    }
}
