/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stomp.locationAndTypes;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author Admin
 */
public interface IncidentLocationRepository extends JpaRepository<IncidentLocation, Integer> {

    List<IncidentLocation> findAllByOrgTypeId(int id);

}
