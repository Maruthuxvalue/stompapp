/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stomp.busRoutes;

import com.response.ApiResponse;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Welcome
 */
//@RestController
//@RequestMapping(value = "/bb/bus")
@Service
public class BusRoutesController {

    @Autowired
    BusRoutesRepository busRepository;

    @PostMapping("/get_all_busRoutes")
    public ApiResponse getSchoolBus(BusRoutes obj) {

        List list = new ArrayList();
        try {
            List<BusRoutes> busList = busRepository.findAllByOrgId(obj.getOrgId());
            for (BusRoutes element : busList) {
                HashMap hm = new HashMap();
                hm.put("Id", element.getId());
                hm.put("BusRoute", element.getBusRoute());
                list.add(hm);
            }
        } catch (Exception e) {
            return new ApiResponse(HttpStatus.BAD_REQUEST, "Exception", e.getMessage());

        }
        return new ApiResponse(HttpStatus.OK, "Record retrieved successfully", list);

    }
}
