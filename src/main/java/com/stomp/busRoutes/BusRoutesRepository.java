/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stomp.busRoutes;

import com.stomp.EmergencyReporting.ESBReporting;
import com.stomp.notifications.PushNotifications;
import com.stomp.reporting.Reporting;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author Welcome
 */
public interface BusRoutesRepository extends JpaRepository<BusRoutes, Integer> {

    List<Reporting> fetchByDriverId(@Param("driverId") long id, @Param("orgId") int schId, @Param("place") String place);

    List<PushNotifications> fetchByDriverIdForNotification(@Param("driverId") long id);

    List<ESBReporting> fetchByDriverIdForEBB(@Param("driverId") long id, @Param("orgId") int schId);

    BusRoutes findByDriverId(long id);

    BusRoutes findTop1ByOrgIdAndBusRouteIgnoreCase(int id, String busRoute);

    List<BusRoutes> findAllByOrgId(int id);

    BusRoutes findByDriverIdAndOrgId(long id, int orgId);

}
