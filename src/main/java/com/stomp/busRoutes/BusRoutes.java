/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stomp.busRoutes;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

/**
 *
 * @author Welcome
 */
@Entity
@Table(name = "busRoutes")
@NamedQueries({
    @NamedQuery(name = "BusRoutes.fetchByDriverId", query = "SELECT report \n"
            + "FROM BusRoutes AS bus \n"
            + "INNER JOIN Users AS user ON user.busRoute=bus.busRoute \n"
            + "INNER JOIN Reporting AS report ON user.id=report.userId AND report.incidentPlace LIKE :place \n"
            + "WHERE bus.driverId=:driverId AND report.orgId =:orgId ORDER BY report.id DESC"),
    @NamedQuery(name = "BusRoutes.fetchByDriverIdForNotification", query = "SELECT noti \n"
            + "FROM BusRoutes AS bus \n"
            + "INNER JOIN Users AS user ON user.busRoute=bus.busRoute \n"
            + "INNER JOIN PushNotifications AS noti ON noti.userId=user.id \n"
            + "WHERE bus.driverId=:driverId AND noti.userId <>:driverId ORDER BY noti.id DESC"),
    @NamedQuery(name = "BusRoutes.fetchByDriverIdForEBB", query = "SELECT report \n"
            + "FROM BusRoutes AS bus \n"
            + "INNER JOIN Users AS user ON user.busRoute=bus.busRoute \n"
            + "INNER JOIN ESBReporting AS report ON report.userId=user.id \n"
            + "WHERE bus.driverId=:driverId AND report.orgId =:orgId ORDER BY report.id DESC")})
public class BusRoutes implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Id", nullable = false, updatable = false)
    private int id;

    @Column(name = "BusRoute", nullable = false)
    private String busRoute;

    @Column(name = "DriverId", nullable = false)
    private long driverId;

    @Column(name = "OrgId", nullable = true)
    private Integer orgId;

    @Column(name = "CreatedDateTime", updatable = false)
    @CreationTimestamp
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date createdDateTime;

    @Column(name = "ModifiedDateTime")
    @UpdateTimestamp
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date modifiedDateTime;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getBusRoute() {
        return busRoute;
    }

    public void setBusRoute(String busRoute) {
        this.busRoute = busRoute;
    }

    public long getDriverId() {
        return driverId;
    }

    public void setDriverId(long driverId) {
        this.driverId = driverId;
    }

    public Integer getOrgId() {
        return orgId;
    }

    public void setOrgId(Integer orgId) {
        this.orgId = orgId;
    }

    public Date getCreatedDateTime() {
        return createdDateTime;
    }

    public void setCreatedDateTime(Date createdDateTime) {
        this.createdDateTime = createdDateTime;
    }

    public Date getModifiedDateTime() {
        return modifiedDateTime;
    }

    public void setModifiedDateTime(Date modifiedDateTime) {
        this.modifiedDateTime = modifiedDateTime;
    }

}
