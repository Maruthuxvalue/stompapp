/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stomp.EmergencyReporting;

import com.stomp.busRoutes.BusRoutes;
import com.stomp.busRoutes.BusRoutesRepository;
import com.stomp.notifications.NotificationsRepository;
import com.stomp.notifications.PushNotifications;
import com.stomp.org.Organization;
import com.stomp.org.OrgRepository;
import com.stomp.twilio.SMSController;
import com.stomp.user.ParentChild;
import com.stomp.user.ParentChildRepository;
import com.stomp.user.Users;
import com.stomp.user.UserRepository;
import com.response.ApiResponse;
import com.stomp.user.UserContactRepository;
import com.stomp.user.UserContacts;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author Welcome
 */
//@RestController
//@RequestMapping(value = "/bb/ebb_reporting")
@Service
@PropertySource(value = {"application.properties"})
public class EmergencyReportingController {

    @Autowired
    EmergencyReportingRepository emergencyRepository;

    @Autowired
    UserRepository userRepository;

    @Autowired
    NotificationsRepository notifyRepository;

    @Autowired
    BusRoutesRepository busRepository;

    @Autowired
    OrgRepository schoolsRepository;

    @Autowired
    SMSController smsService;

    @Autowired
    ParentChildRepository parChildRepository;

    @Autowired
    UserContactRepository userContactRepo;

    @Value("${phpip:}")
    private String ip;

    @PostMapping("/get_user_ebb_reports")
    public ApiResponse getUsersEsbReports(ESBReporting obj) {
        List<ESBReporting> list = new ArrayList();
        String errMessage = "Exception";
        String messsage = "Record retrieved successfully";
        try {
            Users language = userRepository.findById(obj.getUserId()).get();
            list = emergencyRepository.findAllByUserIdOrderByIdDesc(obj.getUserId());
            list.stream().forEach((ele) -> {
                if (ele.getOrgId() != null && ele.getOrgId() != 0) {
                    Organization sch = schoolsRepository.findById(ele.getOrgId()).get();
                    ele.setSchoolName(sch.getOrgName());
                }
            });
            if (language.getUserLang().contains("Spanish")) {
                errMessage = "Exception";
                messsage = "Registro recuperado con  éxito";
            }
        } catch (Exception e) {
            return new ApiResponse(HttpStatus.BAD_REQUEST, errMessage, e.getMessage());
        }
        return new ApiResponse(HttpStatus.OK, messsage, list);
    }

    @PostMapping("/ebb_save")
    @Transactional
//    public ApiResponse saveReports(ESBReporting obj, @RequestParam("userTypeId") int typeId) {
    public ApiResponse saveReports(ESBReporting obj, int typeId) {
        String pushLangMsg = " has reported an Emergency Abuse/Harassment.";
        String langGrade = " (Grade: ";
        String langChild = "Your Friend/Spouse/Parent ";
        String langTracing = " You can track your Friend's/Spouse's/Parent's latest location using the location tracker. Please contact the school admin using the chat on this app or call them immediately.";
        String langPhoneCall = " You can chat him through STOMP application Chat or call him by his phone.";
        String langSave = "Record Saved Successfully";
        try {
            Users userDetails = userRepository.findById(obj.getUserId()).get();
            Organization sch = new Organization();
            if (userDetails.getOrgId() != null && userDetails.getOrgId() != 0) {
                sch = schoolsRepository.findById(userDetails.getOrgId()).get();
            }
            if (userDetails.getUserLang().contains("Spanish")) {
                pushLangMsg = " ha reportado una emergencia de acoso.";
                langGrade = " (Grado: ";
                langChild = "Tu amigo / cÃ³nyuge ";
                langTracing = " Puede rastrear la Ãºltima ubicaciÃ³n de su amigo's / cÃ³nyuge's usando el rastreador de ubicaciÃ³n. Por favor comunÃ­quese con el administrador de la escuela usando la charla de la aplicaciÃ³n STOMP o llÃ¡melos de inmediato.";
                langPhoneCall = " Usted puede charlar con Ã©l a travÃ©s de la aplicaciÃ³n STOMP Chat o llamarlo por su telÃ©fono.";
                langSave = "Registro guardado con Ã©xito";
            }
            if (obj.getFileURL() == null) {
//                obj.setFileURL("https://bullingbuddy.s3-us-west-1.amazonaws.com/images/Group+10178.png");
                obj.setFileURL("images/Group+10178.png");
            }
            ESBReporting reportSave = emergencyRepository.save(obj);
            sendSMS(userDetails.getId(), userDetails.getName(), userDetails.getUserLang(), sch.getIsBoarding());
            PushNotifications notifySave = new PushNotifications();
            notifySave.setUserId(obj.getUserId());
            notifySave.setEsbReportId(reportSave.getId());

            if (typeId == 2) { // reporting by teacher
                String pushMessage = userDetails.getName() + pushLangMsg;
                notifySave.setMessage(pushMessage);
                notifySave.setIsFriend('Y');
                notifySave.setIsManager('N');
                notifySave.setIsDriver('Y');
                notifySave.setIsAdmin('N');
                notifySave.setIsSuperAdmin('N');
                notifyRepository.save(notifySave);

                if (userDetails.getOrgId() != null) {

                    List<Users> pushAdmin = userRepository.findAllByUserTypeIdAndOrgId(5, userDetails.getOrgId());
                    if (pushAdmin != null) {
                        for (Users list1 : pushAdmin) {
                            PushNotification(list1.getId(), pushMessage, "ebb");
                        }
                    }
                }
            } else {

                //String pushMessage = userDetails.getName() + langGrade + userDetails.getGrade() + " ) " + pushLangMsg;
                String pushMessage = userDetails.getName()  + pushLangMsg;
                notifySave.setMessage(pushMessage);
                notifySave.setIsFriend('N');
                notifySave.setIsManager('N');
                notifySave.setIsDriver('N');
                notifySave.setIsAdmin('N');
                notifySave.setIsSuperAdmin('N');
                notifyRepository.save(notifySave);
                List<ParentChild> parents = parChildRepository.findAllByUserId(userDetails.getId());

                if (parents != null && ((sch.getIsBoarding() == null) || sch.getIsBoarding().contains("NO"))) {
                    for (ParentChild item : parents) {
                        PushNotification(item.getFriendId(), langChild + userDetails.getName() + pushLangMsg + langTracing, "ebb");
                    }
                }
                if (userDetails.getOrgId() != null && userDetails.getOrgId() != 0) {

                    if (userDetails.getBusRoute() != null && userDetails.getBusRoute().length() > 0 && ((sch.getIsBoarding() == null) || sch.getIsBoarding().contains("NO"))) {
                        BusRoutes bus = busRepository.findTop1ByOrgIdAndBusRouteIgnoreCase(userDetails.getOrgId(), userDetails.getBusRoute());
                        if (bus != null) {
                            PushNotification(bus.getDriverId(), pushMessage, "ebb");
                        }
                    }
                    List<Users> pushTeacher = userRepository.findAllByUserTypeIdAndOrgId(2, userDetails.getOrgId());
                    if (pushTeacher != null) {
                        for (Users list1 : pushTeacher) {
                            PushNotification(list1.getId(), pushMessage, "ebb");
                        }
                    }
                    List<Users> pushAdmin = userRepository.findAllByUserTypeIdAndOrgId(5, userDetails.getOrgId());
                    if (pushAdmin != null) {
                        for (Users list1 : pushAdmin) {
                            PushNotification(list1.getId(), userDetails.getName() + pushLangMsg + langPhoneCall, "ebb");
                        }
                    }
                }
            }
            return new ApiResponse(HttpStatus.OK, langSave, reportSave);
        } catch (Exception e) {
            return new ApiResponse(HttpStatus.BAD_REQUEST, "Exception", e.getMessage());
        }
    }

    public ApiResponse sendSMS(long id, String name, String lang, String isBoarding) {
        String pushLangMsg = " has reported an Emergency Abuse/Harassment.";
        String langGrade = " (Grade: ";
        String langChild = "Your Friend/Spouse/Parent ";
        String langTracing = " You can track your Friend's/Spouse's/Parent's latest location using the location tracker. Please contact the admin using the chat on this app or call them immediately.";
        String langPhoneCall = " You can chat him through STOMP application Chat or call him by his phone.";
        String langSave = "Record Saved Successfully";
        List<String> noList = new ArrayList();
        try {
            if (lang.contains("Spanish")) {
                pushLangMsg = " ha reportado una emergencia de acoso.";
                langGrade = " (Grado: ";
                langChild = "Tu amigo / cÃ³nyuge ";
                langTracing = " Puede rastrear la Ãºltima ubicaciÃ³n de su amigo / cÃ³nyuge usando el rastreador de ubicaciÃ³n. Por favor comunÃ­quese con el administrador mediante el chat de esta aplicaciÃ³n STOMP o llÃ¡melos de inmediato.";
                langPhoneCall = " Usted puede charlar con Ã©l a travÃ©s de la aplicaciÃ³n STOMP Chat o llamarlo por su telÃ©fono.";
                langSave = "Registro guardado con Ã©xito";
            }
            List<ParentChild> parents = parChildRepository.findAllByUserId(id);

            if (parents != null && ((isBoarding == null) || isBoarding.contains("NO"))) {
                for (ParentChild item : parents) {
                    Users listP = userRepository.findById(item.getFriendId()).get();
                    if (listP.getUserPhone() != null && listP.getUserPhone().length() > 0) {
                        noList.add(listP.getCcUser() + getOnlyDigits(listP.getUserPhone()));
                    }
                }
                smsService.sendMessages(noList, langChild + name + pushLangMsg + langTracing);
            }
            Users listC = userRepository.findById(id).get();
//            if (listC.getFriendId() != null) {
//                Users listP = userRepository.findById(listC.getFriendId()).get();
//                if (listP.getUserPhone() != null && listP.getUserPhone().length() > 0) {
//                    noList.add(getOnlyDigits(listP.getUserPhone()));
//                    smsService.sendMessages(noList, "Your amigo / cÃ³nyuge " + name + " has reported an Emergency Abuse/Harassment. You can track your amigo's / cÃ³nyuge's latest location using the location tracker. Please contact the school admin using the chat on STOMP app or call them immediately.");
//                }
//            }
            if (listC.getOrgId() != null && listC.getOrgId() != 0) {
                ///List is cleared for SMS to send different message
                noList.clear();
                Users listA = userRepository.findTop1ByUserTypeIdAndOrgId(5, listC.getOrgId());//For Admins
                if (listA.getUserPhone() != null && listA.getUserPhone().length() > 0) {
                    noList.add(listA.getCcUser() + getOnlyDigits(listA.getUserPhone()));
                    if (listC.getUserTypeId() == 2) {
                        smsService.sendMessages(noList, name + pushLangMsg);
                    } else {
                        smsService.sendMessages(noList, name  + pushLangMsg + langPhoneCall);
                    }
                }
            }
            List<UserContacts> friendContacts = userContactRepo.findAllByUserId(id);
            if (!friendContacts.isEmpty()) {
                noList.clear();
                for (UserContacts item : friendContacts) {
                    noList.add(item.getCcPhone() + getOnlyDigits(item.getContactPhone()));
                }
                smsService.sendMessages(noList, name  + pushLangMsg + langPhoneCall);
            }

        } catch (Exception e) {
            return new ApiResponse(HttpStatus.BAD_REQUEST, "Exception", e.getMessage());

        }
        return new ApiResponse(HttpStatus.OK, langSave, noList);

    }

    public static String getOnlyDigits(String s) {
        Pattern pattern = Pattern.compile("[^0-9]");
        Matcher matcher = pattern.matcher(s);
        String number = matcher.replaceAll("");
        return number;
    }

    @PostMapping("/send")
    public ResponseEntity<String> PushNotification(long id, String message, String page) {
        RestTemplate restTemplate = new RestTemplate();
        String url = "http://" + ip + "/bb_php/index.php/push/send_pushnotify_android";
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        MultiValueMap<String, Object> map = new LinkedMultiValueMap<String, Object>();
        map.add("user_id", id);
        map.add("message", message);
        map.add("page_name", page);
        HttpEntity<MultiValueMap<String, Object>> request = new HttpEntity<MultiValueMap<String, Object>>(map, headers);
        ResponseEntity<String> postForObject = restTemplate.postForEntity(url, request, String.class);
        return postForObject;
    }

    @PostMapping("/ebb_update")
    @Transactional(rollbackFor = Exception.class)
    public ApiResponse UpdateReport(ESBReporting obj) {
        String langUpdate = "Record Updated Successfully";
        String langNotFound = "Record not Found";
        String message = "";
        ESBReporting reportDetail = emergencyRepository.findById(obj.getId()).get();
        HttpStatus status = HttpStatus.OK;
        Users lang = userRepository.findById(reportDetail.getUserId()).get();
        try {
            if (lang.getUserLang().contains("Spanish")) {
                langUpdate = "Registro guardado con Ã©xito";
                langNotFound = "Registro no encontrado";
            }
            if (reportDetail != null) {
                reportDetail.setFileURL(obj.getFileURL());
                emergencyRepository.save(reportDetail);
                message = langUpdate;
            } else {
                message = langNotFound;
                status = HttpStatus.NOT_FOUND;
            }
            return new ApiResponse(status, message, reportDetail);
        } catch (Exception e) {
            return new ApiResponse(HttpStatus.BAD_REQUEST, "Exception", e.getMessage());

        }
    }

    @PostMapping("/get_ebb_reports_by_parent_id")
    public ApiResponse getReportsForParent(Users params) {
        String langRetrieved = "Record retrieved successfully";
        Users userLang = userRepository.findById(params.getId()).get();
        List<ParentChild> childList = parChildRepository.findAllByFriendId(params.getId());
        List<Users> userList = new ArrayList();
        for (ParentChild item : childList) {
            Users childReports = userRepository.findById(item.getUserId()).get();
            userList.add(childReports);
        }
        List<ESBReporting> list = new ArrayList();
        List<ESBReporting> list1 = new ArrayList();
        if (userLang.getUserLang().contains("Spanish")) {
            langRetrieved = "Registro recuperado con Ã©xito";
        }
        try {
            for (Users userDetails : userList) {
                list = emergencyRepository.findAllByUserIdOrderByIdDesc(userDetails.getId());
                list.stream().forEach((ele) -> {
                    if (ele.getOrgId() != null && ele.getOrgId() != 0) {
                        Organization sch = schoolsRepository.findById(ele.getOrgId()).get();
                        ele.setSchoolName(sch.getOrgName());
                    }
                    Users userData = userRepository.findById(ele.getUserId()).get();
                    ele.setGrade(userData.getGrade());
                    ele.setName(userData.getName());
                });
                list1.addAll(list);
            }
            Collections.sort(list1,
                    Comparator.comparingLong(ESBReporting::getId).reversed());
        } catch (Exception e) {
            return new ApiResponse(HttpStatus.BAD_REQUEST, "Exception", e.getMessage());
        }
        return new ApiResponse(HttpStatus.OK, langRetrieved, list1);
    }

    @PostMapping("/get_ebb_reports_by_school_id")
    public ApiResponse getReportsForTeachers(Users params) {
        String langRetrieved = "Record retrieved successfully";
        List<ESBReporting> itemList = new ArrayList();
        try {
            itemList = emergencyRepository.findAllByOrgIdOrderByIdDesc(params.getOrgId());
            itemList.stream().forEach((ele) -> {
                if (ele.getOrgId() != null && ele.getOrgId() != 0) {
                    Organization sch = schoolsRepository.findById(ele.getOrgId()).get();
                    ele.setSchoolName(sch.getOrgName());
                }
                Optional<Users> userExist = userRepository.findById(ele.getUserId());
                if (userExist.isPresent()) {
                    Users userDetail = userExist.get();
                    ele.setGrade(userDetail.getGrade());
                    ele.setName(userDetail.getName());
                }
            });
        } catch (Exception e) {
            return new ApiResponse(HttpStatus.BAD_REQUEST, "Exception", e);
        }
        return new ApiResponse(HttpStatus.OK, langRetrieved, itemList);
    }

    @PostMapping("/get_ebb_reports_by_driver_id")
    public ApiResponse getReportsForDrivers(Users params) {
        String langRetrieved = "Record retrieved successfully";
        List<ESBReporting> itemList;
        try {
            Users userDetails = userRepository.findById(params.getId()).get();
            itemList = busRepository.fetchByDriverIdForEBB(params.getId(), userDetails.getOrgId());
            itemList.stream()
                    .forEach((ele) -> {
                        if (ele.getOrgId() != null && ele.getOrgId() != 0) {
                            Organization sch = schoolsRepository.findById(ele.getOrgId()).get();
                            ele.setSchoolName(sch.getOrgName());
                        }
                        Optional<Users> userExist = userRepository.findById(ele.getUserId());
                        if (userExist.isPresent()) {
                            Users userDetail = userExist.get();
                            ele.setGrade(userDetail.getGrade());
                            ele.setName(userDetail.getName());
                        }
                    });
            if (userDetails.getUserLang().contains("Spanish")) {
                langRetrieved = "Registro recuperado con Ã©xito";
            }
        } catch (Exception e) {
            return new ApiResponse(HttpStatus.BAD_REQUEST, "Exception", e.getMessage());
        }
        return new ApiResponse(HttpStatus.OK, langRetrieved, itemList);
    }
}
