/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stomp.EmergencyReporting;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author Welcome
 */
public interface EmergencyReportingRepository extends JpaRepository<ESBReporting, Long> {

    List<ESBReporting> findAllByUserIdOrderByIdDesc(long userId);

    List<ESBReporting> findAllByOrgIdOrderByIdDesc(int orgId);

}
